[RoadMap file](https://bitbucket.org/bugtsa/sporton/src/e16b00f4501d392c49b9901fcad2a051ea9b9b80/Auxiliary_files/RoadMap.md?at=develop&fileviewer=file-view-default)

[V1](https://bitbucket.org/bugtsa/sporton/src/V1?at=develop&fileviewer=file-view-default)

##v 0.0.32
* correct address at event for show events on the map

##v 0.0.30
* delete specification symbols in e-mail strings
* add icon to app

##v 0.0.27
* fix small bugs

##v 0.0.26
* create many event in the same Activity log
* edit(change) event without\with start upload on server

##v 0.0.25
* remove in zone output column UserId and replace her on column Note
* add proxy connection at create events

##v 0.0.24
* create one new window what show event specification
* all main windows is rubber
* delete button test connection
* create function up/down minutes in set Time of Start Event and Start Automaiton at press key left and key down

##v 0.0.23
* create many dublicate event from the same event with repeat weekByWeek
* randomize Days and Hours shift at weekByweek

##v 0.0.22
* dynamical change time in calendar
* remove group Id and  resize time case in dateTimeDialog

##v 0.0.21
* fix code for demo check email and userId in Table Users Profile

##v 0.0.20
* cosmetic fix showing message window
* real generate random index enable user at create event

##v 0.0.19
* add event to server at achievement time&date set startOfAutomation
* add function getUserIdByEmail and getEmailByUserId at change email cell in Table Process Page
* check load data to rows in table Process Page with function getUserIdByEmail and getEmailByUserId

##v 0.0.18
* add slot to click check box Enable User. Save enabled state users
* change playersSpots on enabled user profile

##v 0.0.17
* Add Show button for set Time in Calendar : 1,3,4,5,6,7,8,9,10,11,12 ; 00-15-30-45 ; AM/PM
* Check fill row (no empty) Table in Process Page and output errorMessage if rows not fill
* Add column start of automation and output strings StartOfAutomation to GUI
* Add log Message in LogFile at click buttons in Log Activity

##v 0.0.16
* Correct add partipation to event with empty user_id.
* Output get from SportOn server user_id to UI and autosave file, and xml file

##v 0.0.15
* Edit one selected task on server(update Event) even when add many process Page in Log Activity

##v 0.0.14
* Edit one selected task on server(update Event)
* Test Button work with SportOn API
* Add Participation At event

##v 0.0.13
* Uploading one task on server(create Event)

##v 0.0.12
* Log Activity tasks is run to background
* button test connection to server is run to background process
* correctly output text in calendar button after: change date and time, autoload previous session

##v 0.0.11
* work recovery previous session from auto save file
* button test connection to server

##v 0.0.10
* work auto save(saving in file untitled.xml.autosave)
* work key up and key down for set time in date&time widget

##v 0.0.9
* set place in gui

##v 0.0.8
* create button clean in Log Activity
* craate list Tasks

##v 0.0.7
* upload data to server
* output data in all column Log Activity

##v 0.0.6
* create button add row/s

##v 0.0.5
* action file->new
* action open xml file
* action save xml file
* action save as
* change icons

##v 0.0.4
* Add action in File menu
* Checked select, Select All, Unselect All
* Add column "Start of Event" in Activity Log