#ifndef EVENTWINDOW_H
#define EVENTWINDOW_H

#include <QMainWindow>

#include "data/eventspecification.h"

namespace Ui {
class EventWindow;
}

class EventWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit EventWindow(QWidget *parent = 0);
    ~EventWindow();

    void setEventSpecification(const EventSpecification eventSpecification);

private:
    Ui::EventWindow *ui;

private:

};

#endif // EVENTWINDOW_H
