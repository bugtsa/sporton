#include "datetimedialog.h"
#include "ui_DateTimeDialog.h"

DateTimeDialog::DateTimeDialog(QString windowTitle, QDate date, QTime time, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DateTimeDialog)
{
    ui->setupUi(this);

    connect(ui->btn_1_hour, SIGNAL(clicked()), this, SLOT(btn1HourClicked()));
    connect(ui->btn_2_hour, SIGNAL(clicked()), this, SLOT(btn2HourClicked()));
    connect(ui->btn_3_hour, SIGNAL(clicked()), this, SLOT(btn3HourClicked()));
    connect(ui->btn_4_hour, SIGNAL(clicked()), this, SLOT(btn4HourClicked()));
    connect(ui->btn_5_hour, SIGNAL(clicked()), this, SLOT(btn5HourClicked()));
    connect(ui->btn_6_hour, SIGNAL(clicked()), this, SLOT(btn6HourClicked()));
    connect(ui->btn_7_hour, SIGNAL(clicked()), this, SLOT(btn7HourClicked()));
    connect(ui->btn_8_hour, SIGNAL(clicked()), this, SLOT(btn8HourClicked()));
    connect(ui->btn_9_hour, SIGNAL(clicked()), this, SLOT(btn9HourClicked()));
    connect(ui->btn_10_hour, SIGNAL(clicked()), this, SLOT(btn10HourClicked()));
    connect(ui->btn_11_hour, SIGNAL(clicked()), this, SLOT(btn11HourClicked()));
    connect(ui->btn_12_hour, SIGNAL(clicked()), this, SLOT(btn12HourClicked()));

    connect(ui->btn_minute_00, SIGNAL(clicked()), this, SLOT(btnMinute00Clicked()));
    connect(ui->btn_minute_15, SIGNAL(clicked()), this, SLOT(btnMinute15Clicked()));
    connect(ui->btn_minute_30, SIGNAL(clicked()), this, SLOT(btnMinute30Clicked()));
    connect(ui->btn_minute_45, SIGNAL(clicked()), this, SLOT(btnMinute45Clicked()));

    connect(ui->checkbox_am_pm, SIGNAL(toggled(bool)), this, SLOT(changeCheckBoxMidday()));

    connect(ui->calendarWidget, SIGNAL(clicked(QDate)), this, SLOT(setDateAfterCalendarClicked(QDate)));

    setDateAndTime(date, time);
    changeCheckBoxMidday();
    processDateTime();
    ui->label->setFocus();
    this->setWindowTitle(windowTitle);
}

bool DateTimeDialog::checkHoursAfterMidday(QTime time) {
    if (time.hour() < value12HourPM && time.hour() >= value12HourAM) {
        return false;
    } else {
        return true;
    }
}

void DateTimeDialog::btn1HourClicked() {
    setHourInTime(1);
}

void DateTimeDialog::btn2HourClicked() {
    setHourInTime(2);
}

void DateTimeDialog::btn3HourClicked() {
    setHourInTime(3);
}

void DateTimeDialog::btn4HourClicked() {
    setHourInTime(4);
}

void DateTimeDialog::btn5HourClicked() {
    setHourInTime(5);
}

void DateTimeDialog::btn6HourClicked() {
    setHourInTime(6);
}

void DateTimeDialog::btn7HourClicked() {
    setHourInTime(7);
}

void DateTimeDialog::btn8HourClicked() {
    setHourInTime(8);
}

void DateTimeDialog::btn9HourClicked() {
    setHourInTime(9);
}

void DateTimeDialog::btn10HourClicked() {
    setHourInTime(10);
}

void DateTimeDialog::btn11HourClicked() {
    setHourInTime(11);
}

void DateTimeDialog::btn12HourClicked() {
    setHourInTime(12);
}

void DateTimeDialog::btnMinute00Clicked(){
    setMinuteInTime(0);
}

void DateTimeDialog::btnMinute15Clicked(){
    setMinuteInTime(15);
}

void DateTimeDialog::btnMinute30Clicked(){
    setMinuteInTime(30);
}

void DateTimeDialog::btnMinute45Clicked(){
    setMinuteInTime(45);
}

int DateTimeDialog::getHalfDayInHours(bool isAfterMidday) {
    int halfDayInHours = 0;
    if (isAfterMidday) {
        halfDayInHours = 12;
    } else {
        halfDayInHours = 0;
    }
    return halfDayInHours;
}

void DateTimeDialog::changeButtonsText(bool isAfterMidday) {
    int halfDayInHours = getHalfDayInHours(isAfterMidday);
    ui->btn_12_hour->setText(QString("%1").arg(0 + halfDayInHours));
    ui->btn_1_hour->setText(QString("%1").arg(1 + halfDayInHours));
    ui->btn_2_hour->setText(QString("%1").arg(2 + halfDayInHours));
    ui->btn_3_hour->setText(QString("%1").arg(3 + halfDayInHours));
    ui->btn_4_hour->setText(QString("%1").arg(4 + halfDayInHours));
    ui->btn_5_hour->setText(QString("%1").arg(5 + halfDayInHours));
    ui->btn_6_hour->setText(QString("%1").arg(6 + halfDayInHours));
    ui->btn_7_hour->setText(QString("%1").arg(7 + halfDayInHours));
    ui->btn_8_hour->setText(QString("%1").arg(8 + halfDayInHours));
    ui->btn_9_hour->setText(QString("%1").arg(9 + halfDayInHours));
    ui->btn_10_hour->setText(QString("%1").arg(10 + halfDayInHours));
    ui->btn_11_hour->setText(QString("%1").arg(11 + halfDayInHours));
}

void DateTimeDialog::changeCheckBoxMidday() {
    if (ui->checkbox_am_pm->isChecked()) {
        ui->checkbox_am_pm->setText(formatHourClockPostMeridiem);
        isAfterMidday = true;
    } else {
        ui->checkbox_am_pm->setText(formatHourClockAnteMeridiem);
        isAfterMidday = false;
    }
    changeButtonsText(isAfterMidday);
    setTimeAfterChangeBoxMidday(isAfterMidday);
}

void DateTimeDialog::setDateAfterCalendarClicked(QDate currentDate) {
    this->date = currentDate;
}

void DateTimeDialog::setTimeAfterChangeBoxMidday(bool isAfterMidday) {
    QTime time = getTime();
    int halfDayInHours = 12;
    int currentHour = time.hour();
    if(isAfterMidday) {
        if(currentHour < 12) {
            currentHour += halfDayInHours;
        } else if (currentHour == 0) {
            currentHour = 12;
        }
    } else if (!isAfterMidday){
        if ((currentHour > 12) && (currentHour != 0)) {
            currentHour -= halfDayInHours;
        } else if (currentHour == 12) {
            currentHour = 0;
        }
    }
    time.setHMS(currentHour, time.minute(), time.second());
    setTimeInTimeEdit(time);
}

DateTimeDialog::~DateTimeDialog()
{
    delete ui;
}

QString DateTimeDialog::getDateString(){
    return dateString;
}

QDate DateTimeDialog::getDate() {
    return date;
}

QDateTime DateTimeDialog::getDateTime() {
    return dateTime;
}

QString DateTimeDialog::getDateTimeString() {
    return dateTimeString;
}

void DateTimeDialog::setDateAndTime(QDateTime dateTime) {
    setDateAndTime(dateTime.date(), dateTime.time());
}

void DateTimeDialog::processDateTime() {
    date = ui->calendarWidget->selectedDate();
    time = ui->timeEdit->time();
    dateTime.setDate(date);
    dateTime.setTime(time);
    dateTimeString = date.toString(formatOfDate) + " " + time.toString(formatOfTime);
}

void DateTimeDialog::setDateAndTime(QDate date, QTime time) {
    ui->checkbox_am_pm->setChecked(checkHoursAfterMidday(time));
    ui->label->setFocus();
    ui->calendarWidget->setSelectedDate(date);
    ui->timeEdit->setTime(time);
    processDateTime();
}

QTime DateTimeDialog::getTime() {
    return ui->timeEdit->time();
}

void DateTimeDialog::setHourInTime(int quantityHour) {
    QTime currentTime = getTime();
    int setHour = quantityHour;
    if (isAfterMidday) {
        setHour += 12;
    }
    if (quantityHour == 12) {
        if (!isAfterMidday) {
            setHour = 0;
        }
    }
    currentTime.setHMS(setHour, currentTime.minute(), currentTime.second());
    setTimeInTimeEdit(currentTime);
    processDateTime();
}

void DateTimeDialog::setMinuteInTime(int quantityMinute) {
    QTime currentTime = getTime();
    int setMinute = quantityMinute;
    currentTime.setHMS(currentTime.hour(), setMinute, currentTime.second());
    setTimeInTimeEdit(currentTime);
    processDateTime();
}

void DateTimeDialog::setTimeInTimeEdit(QTime _time) {
    ui->timeEdit->setTime(_time);
}

void DateTimeDialog::upHour() {
    setDateAndTime(calendarUtils.processDateTimeWithShiftOnOneHour(getDateTime(), true));
}

void DateTimeDialog::downHour() {
    setDateAndTime(calendarUtils.processDateTimeWithShiftOnOneHour(getDateTime(), false));
}

void DateTimeDialog::upMinute() {
    setDateAndTime(calendarUtils.processDateTimeWithShiftOnOneMinute(getDateTime(), true));
}

void DateTimeDialog::downMinute() {
    setDateAndTime(calendarUtils.processDateTimeWithShiftOnOneMinute(getDateTime(), false));
}

void DateTimeDialog::keyPressEvent( QKeyEvent * event)
{
    if (event->count() == 1) {
        switch(event->key()) {
            case Qt::Key_Up:
                upHour();
                break;
            case Qt::Key_Down:
                downHour();
                break;
            case Qt::Key_Right:
                upMinute();
                break;
            case Qt::Key_Left:
                downMinute();
                break;
        }
    }
}

void DateTimeDialog::on_buttonBox_accepted()
{
    processDateTime();
    close();
}
