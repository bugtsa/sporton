#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "utils/itemdelegate.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    isFormProcessPageInitialize = false;
    ui->setupUi(this);

    logMessage = new LogMessage();
    logMessage->writeLine("Hi, gui. " + messageBodyAboutApplication + messageVersion + " and now create MainWindow::MainWindow()");

    connect(ui->act_About_Sporton, SIGNAL(triggered()), this, SLOT(about()));

    ui->btn_log->setIcon(QIcon(":/images/log.png"));
    ui->btn_add_process->setIcon(QIcon(":/images/start.png"));
    ui->btn_start_of_event->setIcon(QIcon(":/images/calendar.png"));
    ui->btn_start_of_automation->setIcon(QIcon(":/images/calendar.png"));

    networkAccessManager = new QNetworkAccessManager;
    QObject::connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyfinished(QNetworkReply*)));

    createActions();

    isSavedCurrentStateFile = isSaveFileInCurrentSession = isSetEventToView = false;
    initTitleFileName();
    checkCurrentStateSaving();
    autoSaveFileName = textNameFileAutoSave + textExtentionXml + textExtentionAutoSave;

    pTableWidget = ui->tableWidget;
    pTableWidget->setRowCount(firstQuantityRowsProcessPage);
    tableUtils.prepareTableWidget(pTableWidget);
    pTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    processGUI.initComboBoxCity(ui->combo_city);

    processPage = new ProcessPage();
    mainController = new MainController(logMessage, this);
    mainController->resetProcessPage(processPage, tableUtils.getRowCountTableWidget(pTableWidget));
    isHaveConnection = mainController->getConnectionStatus();
    logWindow = new LogWindow(logMessage, this);

    connect (logWindow, SIGNAL(readyReplyListTasks(QList<ProcessPage>)),
             this, SLOT(readyReplyListTasks(QList<ProcessPage>)));
    connect(logWindow, SIGNAL(enableEditSelectedEvent(QList<int>)), this, SLOT(enableEditCurrentEvent(QList<int>)));
    connect(logWindow, SIGNAL(requestOutputProcessPage(ProcessPage)),
            this, SLOT(getRequestOutputProcessPage(ProcessPage)));
    connect(logWindow, SIGNAL(replyGetUserIdByEmailForOutputUI(int)),
            this, SLOT(getUserIdByEmailForOutputUI(int)));

    connect(logWindow, SIGNAL(saveCurrentProcessPageToXmlFile(ProcessPage)),
            this, SLOT(getCurrentProcessPageToXmlFile(ProcessPage)));

    connect(this, SIGNAL(requestSetStateIsCloseApp()), logWindow, SLOT(getRequestSetStateIsCloseApp()));

    dialogStartOfEvent = new DateTimeDialog(windowTitleDialogStartOfEvent,
                                     processPage->eventHeader.dateStartOfEvent,
                                     processPage->eventHeader.timeStartOfEvent,
                                     this);
    requestForDateTimeButton(ui->btn_start_of_event, true);
    dialogStartOfAutomation = new DateTimeDialog(windowTitleDialogStartOfAutomation,
                                   processPage->eventHeader.dateOfAutomation,
                                   processPage->eventHeader.timeOfAutomation,
                                   this);
    requestForDateTimeButton(ui->btn_start_of_automation, false);

//    testConnecter = new TestConnecter(logMessage, this);
//    connect(testConnecter, SIGNAL(updateStateTestConnection()), this, SLOT(readyToUpdateStateTestConnect()));
//    startTestConnect();
//    ui->btn_test_connection->setVisible(false);

//    ItemDelegate *itDelegate = new  ItemDelegate;
//    pTableWidget->setItemDelegate(itDelegate);

    recoveryPreviousSession();
    isFormProcessPageInitialize = true;
    isEventWillBeSelectedForEdit = false;
    isEventChanged = false;

#ifdef QT_DEBUG
//    TestTwoTasks();
#endif
}

void MainWindow::TestTwoTasks() {
    open("./Xml_files/01-Hutler vs Stalin.xml");
    logWindow->sendToControllerProcessPage(*processPage, isEventWillBeSelectedForEdit);
    QThread::sleep(1);
    open("./Xml_files/02-Putin vs Arnold.xml");
    logWindow->sendToControllerProcessPage(*processPage, isEventWillBeSelectedForEdit);
}

MainWindow::~MainWindow()
{
//    testConnecter->requestInterruption();
//    testConnecter->wait(timeWaitToDestroyTestConnect);

    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if (logWindow->isVisible()) {
//        bool isCloseUploadTasksOnServer = true;
//        logWindow->closeEvent(isCloseUploadTasksOnServer, event);
        logWindow->close();
    }
    emit requestSetStateIsCloseApp();
}

bool MainWindow::maybeSave()
{
    if (!isSavedCurrentStateFile) {
        QMessageBox::StandardButton ret;
        ret = messageUtils.createWarningMessageWithStandartButtons(this, messageTitleApplication, messageBodyMayBeSave);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}

void MainWindow::recoveryPreviousSession() {
    if (QFile(autoSaveFileName).exists()) {
        openedFileName = mainController->openFile(autoSaveFileName, processPage);
        setProcessPageToView();
        checkCurrentStateSaving();
    }
}

void MainWindow::newPage() {
    mainController->resetProcessPage(processPage, tableUtils.getRowCountTableWidget(pTableWidget));
    setProcessPageToView();
    initTitleFileName();
    processCommandsAfterChangesInGUI(true);
}

void MainWindow::open(QString sendFileName) {
    QString fileName = "";

    if (sendFileName.isNull()) {
        fileName = QFileDialog::getOpenFileName(this,
            tr("Open Xml"), "./Xml_files", tr("Sporton Xml Files (*.xml)"));
    } else {
        fileName = sendFileName;
    }
    openedFileName = mainController->openFile(fileName, processPage);
    if (openedFileName.isEmpty()) {
        messageCantOpenFile();
    }
    else {
        isSavedCurrentStateFile = isSaveFileInCurrentSession = true;
    }
    setProcessPageToView();
    checkCurrentStateSaving();
    autoSave(true);
}

void MainWindow::autoSave(bool isOpenFile) {
    if (isOpenFile) {
        getProcessPageFromView(pTableWidget);
    }
    mainController->saveFile(autoSaveFileName, processPage, tableUtils.getRowCountTableWidget(pTableWidget));
}

bool MainWindow::save(){
    if (!isSaveFileInCurrentSession) {
        openedFileName = QFileDialog::getSaveFileName(this,
                                        tr("Save Xml"), getFileNameForSave(),
                                        tr("Xml files (*.xml)"));
    }
//    getProcessPageFromView(pTableWidget);
    bool isSaveFile = mainController->saveFile(openedFileName, processPage, tableUtils.getRowCountTableWidget(pTableWidget));
    isSavedCurrentStateFile = isSaveFileInCurrentSession = isSaveFile;
    checkCurrentStateSaving();
    return isSaveFile;
}

void MainWindow::saveAs(){
    getProcessPageFromView(pTableWidget);
    QString fileName = QFileDialog::getSaveFileName(this,
                                        tr("Save Xml"), getFileNameForSave(),
                                        tr("Xml files (*.xml)"));
    if (fileName != openedFileName) {
        openedFileName = fileName;
    }
    mainController->saveFile(openedFileName, processPage, tableUtils.getRowCountTableWidget(pTableWidget));
    isSavedCurrentStateFile = isSaveFileInCurrentSession = true;
    checkCurrentStateSaving();
}

void MainWindow::about() {
    messageUtils.createAboutMessage(this, messageTitleAboutApplication,
                                     messageBodyAboutApplication + messageVersion + messageSupport);
}

void MainWindow::enableEditCurrentEvent(QList<int> listSelectedRows) {
    logWindow->close();
    if (!listSelectedRows.isEmpty()) {
        isEventWillBeSelectedForEdit = true;
    } else {
        isEventWillBeSelectedForEdit = false;
    }
}

void MainWindow::swapProcessPage(ProcessPage _processPage) {
    mainController->swapProcessPage(processPage, _processPage);
    setProcessPageToView();
}

void MainWindow::getCurrentProcessPageToXmlFile(ProcessPage _processPage) {
    resetProcessPageToView();
    swapProcessPage(_processPage);
    mainController->saveFile(_processPage.eventHeader.filePathToOpenFile, &_processPage, _processPage.rowCount);
    processCommandsAfterChangesInGUI(false);
}

void MainWindow::getRequestOutputProcessPage(ProcessPage _processPage) {
    swapProcessPage(_processPage);
    isEventWillBeSelectedForEdit = true;
}

void MainWindow::updateUserDataByUserId(ProcessPage *prccessPage, int indexRow) {
    UserData UserData = processPage->listUserData.at(indexRow);
    if (UserData.id == 0) {
        int responseUserId = getUserIdByUserData(UserData);
        if (responseUserId > 0) {
            if (responseUserId != UserData.id) {
                UserData.id = responseUserId;
            }
        }
        processPage->refreshItemUserData(indexRow, UserData);
    }
}

int MainWindow::getUserIdByUserData(UserData userData) {
    if (userData.email != "") {
        int userId = logWindow->checkUserIdByEmail(userData.email);
        if(userId == 0) {
            QString getUserId = logWindow->registerUserData(userData);
            userId = getUserId.toInt(&okConvert, 0);
        }
        return userId;
    } else {
        return -1;
    }
}

void MainWindow::resetProcessPageToView() {
    ui->le_title->setText("");
    ui->le_desc->setText("");
    ui->combo_sports->setCurrentIndex(0);
    ui->combo_city->setCurrentIndex(0);
    ui->le_location->setText("");
    ui->btn_start_of_event->setText("");
    ui->btn_start_of_automation->setText("");
    ui->check_randomize_captain->setChecked(false);
    ui->check_repeat_week_by_week->setChecked(false);
    ui->check_days_repeat->setChecked(false);
    ui->combo_days_repeat->setCurrentIndex(0);
    ui->check_hours_repeat->setChecked(false);
    ui->combo_hours_repeat->setCurrentIndex(0);
    UserData UserData;
    for (int index = 0; index < tableUtils.getRowCountTableWidget(pTableWidget); index++) {
        setUserDataToView(pTableWidget, UserData, index);
    }
}

void MainWindow::setProcessPageToView() {
    int userId = 0;
    QString email = "";
    QString response = "";

    isSetEventToView = true;
    ui->le_title->setText(processPage->eventHeader.title);
    ui->le_desc->setText(processPage->eventHeader.description);
    ui->combo_sports->setCurrentIndex(processPage->eventHeader.kindOfSportForApp);
    ui->combo_city->setCurrentIndex(processPage->eventHeader.city);
    ui->le_location->setText(processPage->eventHeader.location);
    ui->btn_start_of_event->setText(processPage->eventHeader.stringDateTimeStartOfEvent);
    ui->btn_start_of_automation->setText(processPage->eventHeader.stringDateTimeOfAutomation);
    ui->check_randomize_captain->setChecked(processPage->eventHeader.randomizeCaptain);
    ui->check_repeat_week_by_week->setChecked(processPage->eventHeader.repeatWeekByWeek);
    ui->check_days_repeat->setChecked(processPage->eventHeader.daysRepeat);
    ui->combo_days_repeat->setCurrentIndex(processPage->eventHeader.valueDaysRepeat);
    ui->check_hours_repeat->setChecked(processPage->eventHeader.hoursRepeat);
    ui->combo_hours_repeat->setCurrentIndex(processPage->eventHeader.valueHoursRepeat);

    for (int row = 0; row < processPage->rowCount; row++) {
        userId = processPage->listUserData.at(row).id;
        email = processPage->listUserData.at(row).email;
        if (isHaveConnection && (email != "") && (userId > 0)) {
//            if  {
//                if  {
            response = isCorrectPairUserIdAndEmail(userId, email);
            if (response == "NO") {
//                setUserDataToView(pTableWidget, processPage->listUserData.at(row), row);
                tableUtils.setTextInTableWidgetCell(pTableWidget, row, 0, response + " correct value email");
            } else if ((response == "OK") || (response != "NO") || (response.toInt(&okConvert, 0) <= 0)) {
//              setUserDataToView(pTableWidget, processPage->listUserData.at(row), row);
            }
        } else if (userId == 0) {
            updateUserDataByUserId(processPage, row);
        }
//                    setUserDataToView(pTableWidget, processPage->listUserData.at(row), row);
        setUserDataToView(pTableWidget, processPage->listUserData.at(row), row);
//            }
//        }
    }
    pTableWidget->setCurrentCell(0, 0);
    pTableWidget->clearSelection();
    isSetEventToView = false;
}

void MainWindow::setUserDataToView(QTableWidget *pTableWidget, const UserData UserData, int indexRow) {
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexRow, indexColumnNote, UserData.note);
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexRow, 1, UserData.name);
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexRow, indexColumnEmail, UserData.email);
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexRow, 3, UserData.password);
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexRow, 4, UserData.proxy);
    processGUI.checkBoxUtils.setStateCheckBox(pTableWidget, indexRow, 5, UserData.enabled);
}

ProcessPage MainWindow::getProcessPageFromView(QTableWidget *pTableWidget) {

    EventHeader *pEventHeader = &processPage->eventHeader;

    pEventHeader->title = ui->le_title->text();
    pEventHeader->description = ui->le_desc->text();
    pEventHeader->kindOfSportForApp = ui->combo_sports->currentIndex();
    pEventHeader->city = ui->combo_city->currentIndex();
    pEventHeader->stCity = ui->combo_city->currentText();
    pEventHeader->location = ui->le_location->text();
    pEventHeader->stringDateTimeStartOfEvent = ui->btn_start_of_event->text();
    pEventHeader->ratingDateOfEvent = processPage->getShiftToStartEvent(lengthTimeForSetRatingEvent).toString(formatDateTimeFullWithSeconds);
    pEventHeader->stringDateTimeOfAutomation = ui->btn_start_of_automation->text();
    pEventHeader->randomizeCaptain = ui->check_randomize_captain->checkState();
    pEventHeader->repeatWeekByWeek = ui->check_repeat_week_by_week->checkState();
    pEventHeader->daysRepeat = ui->check_days_repeat->checkState();
    pEventHeader->valueDaysRepeat = ui->combo_days_repeat->currentIndex();
    pEventHeader->stDaysRepeat = ui->combo_days_repeat->currentText();
    pEventHeader->hoursRepeat = ui->check_hours_repeat->checkState();
    pEventHeader->valueHoursRepeat = ui->combo_hours_repeat->currentIndex();
    pEventHeader->stHoursRepeat = ui->combo_hours_repeat->currentText();

    processPage->rowCount = pTableWidget->rowCount();
    processPage->listUserData.clear();
    for (int row = 0; row < processPage->rowCount; row++) {
        UserData UserData = tableUtils.getUserDataFromTableWidget(pTableWidget, row);
        UserData.id = getUserIdByUserData(UserData);
        processPage->listUserData.push_back(UserData);
    }
    pEventHeader->quantityPlayersInEvent = tableUtils.getQuantityPlayersInEvent(pTableWidget, processPage);
    return *processPage;
}

QString MainWindow::getFileNameForSave() {
    QString defaultPath = xmlDefaultDirectory + "/";
    if (QDir(xmlDefaultDirectory).exists()) {
        defaultPath += textNoNameFile;
    } else {
        defaultPath = textNoNameFile;
    }
    return defaultPath;
}

void MainWindow::processCommandsAfterChangesInGUI(bool isOpenFile) {
    if (!isOpenFile) {
        isEventChanged = true;
    }
    isSavedCurrentStateFile = isOpenFile;
    checkCurrentStateSaving();
    autoSave(isOpenFile);
}

void MainWindow::checkCurrentStateSaving() {
    setTitleFileName();
    setFileNameWindowTitle();
    setActions();
}

void MainWindow::setFileNameWindowTitle() {
    this->setWindowTitle(titleFileName);
}

void MainWindow::initTitleFileName() {
    titleFileName = textNoNameFile + textExtentionXml;
}

void MainWindow::setTitleFileName() {
    titleFileName = getOnlyFileName();
    if (!isSavedCurrentStateFile) {
        titleFileName += textNoSaveFile;
    }
}

QString MainWindow::getOnlyFileName() {
    if (openedFileName.isEmpty()) {
        if (titleFileName.contains(textNoSaveFile)) {
            return titleFileName.remove(titleFileName.indexOf(textNoSaveFile), 1);
        } else {
            return titleFileName;
        }
    } else {
        if (openedFileName.lastIndexOf("/") > 0) {
             return openedFileName.right(openedFileName.size() - openedFileName.lastIndexOf("/") - 1);
        } else {
            return openedFileName;
        }
    }
}

void MainWindow::setActions() {
    QString onlyFileName = getOnlyFileName();
    ui->actSave->setText(textActionSave + onlyFileName);
    ui->actSaveAs->setText(textActionSave + onlyFileName + textActionSaveAs);
}

void MainWindow::createActions()
{
    ui->actNew->setIcon(QIcon(":/images/new.png"));
    ui->actNew->setShortcuts(QKeySequence::New);
    connect(ui->actNew, SIGNAL(triggered()), this, SLOT(newPage()));

    ui->actOpen->setIcon(QIcon(":/images/open.png"));
    ui->actOpen->setShortcuts(QKeySequence::Open);
    ui->actOpen->setStatusTip(tr("Open an existing file"));
    connect(ui->actOpen, SIGNAL(triggered()), this, SLOT(open()));

    ui->actSave->setIcon(QIcon(":/images/save.png"));
    ui->actSave->setShortcuts(QKeySequence::Save);
    ui->actSave->setStatusTip(tr("Save the document to disk"));
    connect(ui->actSave, SIGNAL(triggered()), this, SLOT(save()));

    ui->actSaveAs->setIcon(QIcon(":/images/save-as.png"));
    ui->actSaveAs->setShortcuts(QKeySequence::SaveAs);
    ui->actSaveAs->setStatusTip(tr("Save the document under a new name"));
    connect(ui->actSaveAs, SIGNAL(triggered()), this, SLOT(saveAs()));
}

void MainWindow::requestForDateTimeButton(QPushButton *pDateTimeButton, bool isStartDateTime) {
    pDateTimeButton->setText(mainController->getDateTimeString(processPage, isStartDateTime));
}


void MainWindow::setDateToButton(QPushButton *dateButton,
                                 DateTimeDialog *pDateTimeDialog,
                                 QDate *dateDialog,
                                 QTime *timeDialog,
                                 QString *pStringDateAndTime) {
    pDateTimeDialog->setDateAndTime(*dateDialog, *timeDialog);
    pDateTimeDialog->setModal(true);
    pDateTimeDialog->exec();

    checkChangeDate(pDateTimeDialog, dateDialog);
    checkChangeTime(pDateTimeDialog, timeDialog);

    QString dateTimeString = pDateTimeDialog->getDateTimeString();

    QString currentDateTimeInButton = *pStringDateAndTime;
    if ((currentDateTimeInButton != dateTimeString) && (!dateTimeString.isEmpty())) {
        *pStringDateAndTime = dateTimeString;
        dateButton->setText(dateTimeString);
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_btn_start_of_automation_clicked()
{
    setDateToButton(ui->btn_start_of_automation,
                    dialogStartOfAutomation,
                    &processPage->eventHeader.dateOfAutomation,
                    &processPage->eventHeader.timeOfAutomation,
                    &processPage->eventHeader.stringDateTimeOfAutomation);
}

void MainWindow::on_btn_start_of_event_clicked()
{
    setDateToButton(ui->btn_start_of_event,
                    dialogStartOfEvent,
                    &processPage->eventHeader.dateStartOfEvent,
                    &processPage->eventHeader.timeStartOfEvent,
                    &processPage->eventHeader.stringDateTimeStartOfEvent);
    processPage->setDateTimeEndOfEvent();
}

void MainWindow::checkChangeDate(DateTimeDialog *pDateTimeDialog, QDate *dateDialog) {
    if (pDateTimeDialog->getDate() != *dateDialog) {
        *dateDialog = pDateTimeDialog->getDate();
    }
}

void MainWindow::checkChangeTime(DateTimeDialog *pDateTimeDialog, QTime *timeDialog) {
    if (pDateTimeDialog->getTime() != *timeDialog) {
        *timeDialog = pDateTimeDialog->getTime();
    }
}

void MainWindow::on_btn_checked_select_clicked()
{
    QTableWidget *pTableWidget(ui->tableWidget);
    QItemSelectionModel *select = pTableWidget->selectionModel();

    if (select->hasSelection()) { //check if has selection
        QModelIndexList selection = pTableWidget->selectionModel()->selectedRows();

        // Multiple rows can be selected
        for(int i = 0; i < selection.count(); i++) {
            processGUI.checkBoxUtils.setStateCheckBox(pTableWidget, selection.at(i).row(), tableUtils.getLastIndexColumnTableWidget(pTableWidget), true);
        }
    }
}

void MainWindow::on_btn_select_all_clicked()
{
    QTableWidget *pTableWidget(ui->tableWidget);
    for (int iRow = 0; iRow < pTableWidget->rowCount(); iRow++) {
        processGUI.checkBoxUtils.setStateCheckBox(pTableWidget, iRow, tableUtils.getLastIndexColumnTableWidget(pTableWidget), true);
    }
}

void MainWindow::on_btn_unselect_all_clicked()
{
    QTableWidget *pTableWidget(ui->tableWidget);
    for (int iRow = 0; iRow < pTableWidget->rowCount(); iRow++) {
        processGUI.checkBoxUtils.setStateCheckBox(pTableWidget, iRow, tableUtils.getLastIndexColumnTableWidget(pTableWidget), false);
    }
}

void MainWindow::messageCantOpenFile() {
    messageUtils.createInformationMessage(this, messageError, messageCantOpen);
}

void MainWindow::on_btn_del_sel_clicked() {

}

void MainWindow::on_btn_add_row_clicked() {
    QTableWidget *pTableWidget = ui->tableWidget;
    int quantityAddRows = 1;
    int tmpQuantityAddRows = stringsUtils.convertStrToInt(ui->le_add_row->text());
    if (tmpQuantityAddRows > 1) {
        quantityAddRows = tmpQuantityAddRows;
    }
    int rowCount = tableUtils.getRowCountTableWidget(pTableWidget);
    rowCount += quantityAddRows;
    tableUtils.setRowsTableWidget(pTableWidget, rowCount);
}

void MainWindow::on_btn_add_process_clicked()
{
    if (isCorrectInputProcessPage()) {
        if (processPage->eventHeader.indexEventInLogWindow >= 0) {
            logWindow->editTask(*processPage, processPage->eventHeader.indexEventInLogWindow);
        } else {
            if (isEventChanged && isEventWillBeSelectedForEdit) {
                processPage->getNewGeneratedString();
            }
            logWindow->sendToControllerProcessPage(*processPage, isEventWillBeSelectedForEdit);
        }
    }
}

void MainWindow::on_btn_log_clicked()
{
    if (isCorrectInputProcessPage()) {
        logWindow->showTasks();
        logWindow->show();
    }
}

void MainWindow::on_le_add_row_textChanged(const QString &arg1)
{
    if (arg1 != "1") {
        QPalette palette = ui->le_add_row->palette();
        palette.setColor(QPalette::Text,Qt::black);
        ui->le_add_row->setPalette(palette);
    }
}

QString MainWindow::getStringCityProcessPageFromView() {
    QString currentText = ui->combo_city->currentText();
    processPage->eventHeader.stCity = currentText;
    processCommandsAfterChangesInGUI(false);
    return currentText;
}

int MainWindow::getIndexCityProcessPageFromView() {
    int currentIndex = ui->combo_city->currentIndex();
    processPage->eventHeader.city = currentIndex;
    processCommandsAfterChangesInGUI(false);
    return currentIndex;
}

void MainWindow::setFoundPlace(QString foundPlace)
{
    ui->le_location->setText(foundPlace);
    processPage->eventHeader.location = foundPlace;
    processCommandsAfterChangesInGUI(false);
}

void MainWindow::on_btn_place_clicked()
{
    Google *google = new Google(this);
    google->setDefaultLocation(getStringCityProcessPageFromView(), getIndexCityProcessPageFromView());
    google->show();
}

void MainWindow::on_le_title_textChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.title = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_le_desc_textChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.description = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_sports_currentIndexChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.stKindOfSport = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_sports_currentIndexChanged(int index) {
    if (!isSetEventToView) {
        processPage->eventHeader.kindOfSportForApp = index;
        processPage->eventHeader.kindOfSportForServer = processGUI.getIdKindOfSport(index);
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_city_currentIndexChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.stCity = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_city_currentIndexChanged(int index) {
    if (!isSetEventToView) {
        processPage->eventHeader.city = index;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_groupID_currentIndexChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.stGroupIDtoJoin = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_groupID_currentIndexChanged(int index) {
    if (!isSetEventToView) {
        processPage->eventHeader.groupIDtoJoin = index;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_days_repeat_currentIndexChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.stDaysRepeat = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_hours_repeat_currentIndexChanged(const QString &arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.stHoursRepeat = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

//void MainWindow::on_check_randomizeCaptain_stateChanged(int arg1) {

//}

void MainWindow::on_check_randomize_captain_stateChanged(int arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.randomizeCaptain = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_check_repeat_week_by_week_stateChanged(int arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.repeatWeekByWeek = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_check_days_repeat_stateChanged(int arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.daysRepeat = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_check_hours_repeat_stateChanged(int arg1) {
    if (!isSetEventToView) {
        processPage->eventHeader.hoursRepeat = arg1;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_days_repeat_currentIndexChanged(int index)
{
    if (!isSetEventToView) {
        processPage->eventHeader.valueDaysRepeat = index;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_combo_hours_repeat_currentIndexChanged(int index)
{
    if (!isSetEventToView) {
        processPage->eventHeader.valueHoursRepeat = index;
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::on_tableWidget_cellChanged(int row, int column) {
    if (isFormProcessPageInitialize && !isSetEventToView) {
        int lastColumnIndex = tableUtils.getLastIndexColumnTableWidget(pTableWidget);
        if (column == lastColumnIndex) {
            processPage->setCellValueInUserData(row, column, pTableWidget->item(row, column)->checkState());
        } else if (column == indexColumnEmail) {
            if (checkValidEmail(row, column)) {
                if (isHaveConnection) {
                    getProcessPageFromView(pTableWidget);
                    updateUserDataByUserId(processPage, row);
                }
            } else {
                messageUtils.createWarningMessage(this, messageTitleCheckCorrectInputTableData, MESSAGE_BODY_NOT_VALIDATE_EMAIL);
            }
        } else if (column != lastColumnIndex && column != indexColumnEmail){
            processPage->setCellValueInUserData(row, column, tableUtils.getTextFromTableWidgetCell(pTableWidget, row, column));
        }
        processCommandsAfterChangesInGUI(false);
    }
}

void MainWindow::setStateButtonTestConnection (QString stateButton) {
    ui->btn_test_connection->setStyleSheet(stateButton);
}

void MainWindow::on_btn_test_connection_clicked() {
    if (testConnecter->isRunning()) {
        testConnecter->requestInterruption();
        ui->btn_test_connection->setEnabled(true);
        ui->btn_test_connection->setText("Start Test");
        setStateButtonTestConnection(QString("background-color: rgb(%1, %2, %3);").arg(240).arg(240).arg(240));
    } else {
        startTestConnect();
    }
}

void MainWindow::startTestConnect() {
    testConnecter->start();
//    ui->btn_test_connection->setEnabled(false);
//    ui->btn_test_connection->setText("Stop Test");
}

void MainWindow::readyToUpdateStateTestConnect()
{
//    ui->btn_test_connection->setEnabled(true);
//    setStateButtonTestConnection(testConnecter->getColorStringValue());
    isHaveConnection = testConnecter->getConnectionStatus();
    if (!isHaveConnection) {
        messageUtils.createWarningMessage(this, messageTitleApplication, messageBodyNoExistsNetworkConnection);
    }
}

//------------------------------------------------------------------------------------------------------------
bool MainWindow::isCorrectInputProcessPage() {
    getProcessPageFromView(pTableWidget);

    int quantityOnEmptyRows = processPage->getQuantityNoEmptyRows();
    bool isCorrectInputEmail = true;
    for (int indexNoEmptyRow = 0; indexNoEmptyRow < quantityOnEmptyRows; indexNoEmptyRow++) {        
        if (!checkValidEmail(indexNoEmptyRow, indexColumnEmail)) {
            isCorrectInputEmail = false;
            tableUtils.clearCell(pTableWidget, indexNoEmptyRow, indexColumnEmail);
        }
    }
    if (!isCorrectInputEmail) {
        messageUtils.createWarningMessage(this, messageTitleCheckCorrectInputTableData, MESSAGE_BODY_REINPUT_NOT_VALIDATE_EMAIL);
        return false;
    }
    if (processPage->getQuantityFillRows() != quantityOnEmptyRows) {
        messageUtils.createWarningMessage(this, messageTitleCheckCorrectInputTableData, messageBodyCheckCorrectInputTableData);
        return false;
    } else {
        processPage->processIndexRowCaptainEvent();
    }
    return true;
}

bool MainWindow::isEmptyUserData(int indexRow) {
    UserData userData = processPage->listUserData.at(indexRow);
    if ((userData.id >= 0) && (userData.name != "") && (userData.email != "") &&
            (userData.password != "") && (userData.proxy != "")) {
        return processPage->isEmptyUserData(userData);
    }
    else {
        UserData emptyUserData;
        return processPage->isEmptyUserData(emptyUserData);
    }
}

QString MainWindow::isCorrectPairUserIdAndEmail(int userId, QString email) {
    if (isHaveConnection) {
        int getUserId = logWindow->checkUserIdByEmail(email);
        QString getEmail = logWindow->checkEmailByUserId(userId);

        if ((getUserId <= 0) && (email == "")) {
                return "NO";
        } else if (userId == getUserId) {
            if (email == getEmail) {
                return "OK";
            } else {
                return getEmail;
            }
        } else {
            if (getUserId > 0) {
                return QString("%1").arg(getUserId);
            } else {
                return getEmail;
            }
        }
    }
}

bool MainWindow::checkValidEmail(int row, int column) {
    QString originEmail = tableUtils.getTextFromTableWidgetCell(pTableWidget, row, column);
    QString email = stringsUtils.removeSpecSymbols(originEmail);
    if (tableUtils.validateEmail(email) && email != "") {
        if (originEmail != email) {
            tableUtils.setTextInTableWidgetCell(pTableWidget, row, column, email);
        }
        return true;
    } else {
        return false;
    }
}

