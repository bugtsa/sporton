#include "eventwindow.h"
#include "ui_eventwindow.h"

EventWindow::EventWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EventWindow)
{
    ui->setupUi(this);
}

EventWindow::~EventWindow()
{
    delete ui;
}

void EventWindow::setEventSpecification(const EventSpecification eventSpecification) {
    ui->lbl_creating_event->setText(eventSpecification.title);
    ui->lbl_connection->setText(eventSpecification.proxyConnection);

    ui->lbl_current_automation->setText(eventSpecification.dateTimeAutomation);
    ui->lbl_end_current_event->setText(eventSpecification.endTimeOfEvent);
    ui->lbl_start_current_event->setText(eventSpecification.startDateTimeOfEvent);

    ui->lbl_repeat_every->setText(eventSpecification.repeatEvery);
    ui->lbl_start_next_event->setText(eventSpecification.dateTimeNextEvent);
    ui->lbl_next_automation->setText(eventSpecification.dateTimeNextAutomation);

}
