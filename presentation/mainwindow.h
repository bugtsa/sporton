#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QTableWidget>
#include <QComboBox>
#include <QColor>
#include <QColorDialog>
#include <QThread>
#include <QApplication>
#include <QDebug>
#include <QByteArray>
#include <QEventLoop>
#include <QNetworkReply>
#include <QTextCodec>
#include <QUrl>
#include <QUrlQuery>
#include <QCloseEvent>

#include "logwindow.h"
#include "datetimedialog.h"
#include "domain/controllers/testconnecter.h"
#include "domain/controllers/maincontroller.h"
#include "google-place/google.h"
#include "utils/messageutils.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

    void readyToUpdateStateTestConnect();

    void setFoundPlace(QString foundPlace);

protected:
    virtual void closeEvent(QCloseEvent *event);

signals:
    void requestSetStateIsCloseApp();

private slots:
    void newPage();

    void open(QString sendFileName = 0);

    bool maybeSave();

    void autoSave(bool isOpenFile);

    bool save();

    void saveAs();

    void about();

    void enableEditCurrentEvent(QList<int> listSelectedRows);

    void on_btn_log_clicked();

    void on_btn_checked_select_clicked();

    void on_btn_select_all_clicked();

    void on_btn_unselect_all_clicked();

    void on_le_title_textChanged(const QString &arg1);

    void on_le_desc_textChanged(const QString &arg1);

    void on_combo_sports_currentIndexChanged(const QString &arg1);

    void on_btn_del_sel_clicked();

    void on_btn_add_row_clicked();

    void on_btn_add_process_clicked();

    void on_le_add_row_textChanged(const QString &arg1);

    void on_combo_city_currentIndexChanged(const QString &arg1);

    void on_combo_sports_currentIndexChanged(int index);

    void on_combo_city_currentIndexChanged(int index);

    void on_combo_groupID_currentIndexChanged(int index);

    void on_combo_groupID_currentIndexChanged(const QString &arg1);

    void on_combo_days_repeat_currentIndexChanged(const QString &arg1);

    void on_combo_hours_repeat_currentIndexChanged(const QString &arg1);

//    void on_check_randomizeCaptain_stateChanged(int arg1);
    void on_check_randomize_captain_stateChanged(int arg1);

    void on_check_repeat_week_by_week_stateChanged(int arg1);

    void on_check_days_repeat_stateChanged(int arg1);

    void on_check_hours_repeat_stateChanged(int arg1);

    void on_btn_place_clicked();

    void on_btn_start_of_automation_clicked();

    void on_btn_start_of_event_clicked();

    void on_combo_days_repeat_currentIndexChanged(int index);

    void on_combo_hours_repeat_currentIndexChanged(int index);

    void on_tableWidget_cellChanged(int row, int column);

    void on_btn_test_connection_clicked();

    void getRequestOutputProcessPage(ProcessPage _processPage);

    void getCurrentProcessPageToXmlFile(ProcessPage _processPage);

private:
    Ui::MainWindow *ui;
    LogWindow *logWindow;

    DateTimeDialog *dialogStartOfEvent;
    DateTimeDialog *dialogStartOfAutomation;

    bool isSavedCurrentStateFile;

    bool isSaveFileInCurrentSession;

    bool isEventChanged;

    bool isFormProcessPageInitialize;

    bool isEventWillBeSelectedForEdit;

    bool isSetEventToView;

    bool isHaveConnection;

    bool okConvert;

    QString openedFileName;

    QString titleFileName;

    QString autoSaveFileName;

    LogMessage *logMessage;

    MainController *mainController;

    ProcessPage *processPage;

    ProcessGUI processGUI;

    TableUtils tableUtils;

    MessageUtils messageUtils;

    StringsUtils stringsUtils;

    TestConnecter *testConnecter;

    QTableWidget *pTableWidget;

    QNetworkAccessManager *networkAccessManager;

    QSslSocket socket;

private:
    //---------------Actions--------------------------------------

    void recoveryPreviousSession();

    void createActions();

    void setActions();

    void setFileNameWindowTitle();

    void initTitleFileName();

    void setTitleFileName();

    QString getFileNameForSave();

    QString getOnlyFileName();

    void checkCurrentStateSaving();

    void processCommandsAfterChangesInGUI(bool isOpenFile);

    //---------------ProcessPage--------------------------------------
    void resetProcessPageToView();

    void setProcessPageToView();

    void setUserDataToView(QTableWidget *pTableWidget, const UserData userData, int indexRow);

    ProcessPage getProcessPageFromView(QTableWidget *pTableWidget);

    QString getStringCityProcessPageFromView();

    int getIndexCityProcessPageFromView();

    bool isEmptyUserData(int indexRow);

    void updateUserDataByUserId(ProcessPage *prccessPage, int indexRow);

    int getUserIdByUserData(UserData userData);



    //---------------Other--------------------------------------


    UserData getUserData();

    void setListUserDataFromQuery(int indexInsert, UserData userData);

    void requestForDateTimeButton(QPushButton *pDateTimeButton, bool isStartDateTime);

    void setDateToButton(QPushButton *dateButton,
                         DateTimeDialog *pDateTimeDialog,
                         QDate *dateDialog,
                         QTime *timeDialog,
                         QString *pStringDateAndTime);

    static void getDateAndTime(QString dateAndTime);

    void checkChangeDate(DateTimeDialog *pDateTimeDialog, QDate *dateDialog);

    void checkChangeTime(DateTimeDialog *pDateTimeDialog, QTime *timeDialog);

    void messageCantOpenFile();

    void initQMap();

    void startTestConnect();

    void setStateButtonTestConnection (QString stateButton);

    void TestTwoTasks();

    void swapProcessPage(ProcessPage _processPage);

    bool isCorrectInputProcessPage();

    QString isCorrectPairUserIdAndEmail(int userId, QString email);

    bool checkValidEmail(int row, int column);

};

#endif // MAINWINDOW_H
