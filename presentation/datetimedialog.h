#ifndef DATETIMEDIALOG_H
#define DATETIMEDIALOG_H

#include <QDialog>
#include <QDate>
#include <QKeyEvent>
#include <QObject>

#include "utils/constants.h"
#include "utils/calendarutils.h"

namespace Ui {
class DateTimeDialog;
}

class DateTimeDialog : public QDialog
{
    Q_OBJECT

public:
    QDateTime getDateTime();

    QString getDateTimeString();

    QString getDateString();

    QDate getDate();

    QTime getTime();

    void setDateAndTime(QDateTime dateTime);

    void setDateAndTime(QDate date, QTime time);

    explicit DateTimeDialog(QString windowTitle, QDate date, QTime time, QWidget *parent = 0);
    ~DateTimeDialog();

public slots:


private slots:
    void on_buttonBox_accepted();

    void btn1HourClicked();

    void btn2HourClicked();

    void btn3HourClicked();

    void btn4HourClicked();

    void btn5HourClicked();

    void btn6HourClicked();

    void btn7HourClicked();

    void btn8HourClicked();

    void btn9HourClicked();

    void btn10HourClicked();

    void btn11HourClicked();

    void btn12HourClicked();

    void btnMinute00Clicked();

    void btnMinute15Clicked();

    void btnMinute30Clicked();

    void btnMinute45Clicked();

    void changeCheckBoxMidday();

    void setDateAfterCalendarClicked(QDate currentDate);

private:
    QDateTime dateTime;

    QString dateTimeString;

    QString dateString;

    QDate date;

    QString timeString;

    QTime time;

    bool isAfterMidday;

    CalendarUtils calendarUtils;

    Ui::DateTimeDialog *ui;

private:
    void setTimeInTimeEdit(QTime time);

    void upHour();

    void downHour();

    void upMinute();

    void downMinute();

    void keyPressEvent(QKeyEvent *);

    void processDateTime();

    void setHourInTime(int quantityHour);

    void setMinuteInTime(int quantityMinute);

    int getHalfDayInHours(bool isAfterMidday);

    void changeButtonsText(bool isAfterMidday);

    void setTimeAfterChangeBoxMidday(bool isAfterMidday);

    bool checkHoursAfterMidday(QTime time);
};

#endif // DATETIMEDIALOG_H
