#include "logwindow.h"
#include "ui_logwindow.h"

LogWindow::LogWindow(LogMessage *logMessage, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LogWindow) {
    ui->setupUi(this);
    logMessage->writeLine("create LogWindow::LogWindow()");
    logController  = new LogController (logMessage, this);
    logController->start();
    connect(this, SIGNAL(initTableWidget(QTableWidget *)), logController , SLOT(initTableWidget(QTableWidget *)));

    connect(ui->tableWidget, SIGNAL(cellClicked(int, int)), this, SLOT(tableRowClicked(int, int)));
    connect(ui->tableWidget, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(tableRowDoubleClicked(int,int)));

    connect(this, SIGNAL(requestAddItemIntoMapSelectedTask(QTableWidget *, int, int)),
            logController , SLOT(getRequestAddItemIntoMapSelectedTask(QTableWidget *, int, int)));

    connect(logController , SIGNAL(replySetProcessPageOnUI(ProcessPage)),
            this, SLOT(getReplySetProcessPageOnUI(ProcessPage)));

    connect(this, SIGNAL(addTaskToServer (ProcessPage, bool)), logController , SLOT(addTask(ProcessPage, bool)));
    connect(this, SIGNAL(editTaskToServer(ProcessPage,int)), logController , SLOT(editTask(ProcessPage, int)));
    connect(this, SIGNAL(clearLogWindow()), logController , SLOT(clearLogWindow()));

    connect(this, SIGNAL(showTasksFromServer()), logController , SLOT(showTasks()));

    connect(this, SIGNAL(requestTransferToControllerProcessPage(ProcessPage, bool)),
            logController , SLOT(getRequestTransferToControllerProcessPage(ProcessPage, bool)));
    connect(logController , SIGNAL(saveCurrentProcessPageToXmlFile(ProcessPage)),
            this, SLOT(getCurrentProcessPageToXmlFile(ProcessPage)));

    connect(this, SIGNAL(startUploadTasksToServer()), logController , SLOT(startUploadTasksToServer()));
    connect(this, SIGNAL(changeSelectedTask()), logController , SLOT(changeSelectedTask()));
    connect(this, SIGNAL(stopUploadTasksToServer()), logController , SLOT(stopUploadTasksToServer()));
    connect(this, SIGNAL(cleanUploadedTasks()), logController , SLOT(cleanUploadedTasks()));
    connect(this, SIGNAL(requestBreakWaitStartTask()), logController, SLOT(getRequestBreakWaitStartTask()));

    connect(this, SIGNAL(requestEventSpecification(int)), logController, SLOT(getRequestEventSpecification(int)));

    connect(logController , SIGNAL(replyGetEventSpecification(EventSpecification)),
            this, SLOT(setToUiEventSpecification(EventSpecification)));
    emit (initTableWidget(ui->tableWidget));
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    eventWindow = new EventWindow(this);
}

LogWindow::~LogWindow() {
    delete ui;
}

void LogWindow::addTask(ProcessPage processPage, bool isEditCurrentEventForServer) {
    emit addTaskToServer (processPage, isEditCurrentEventForServer);
}

void LogWindow::editTask(ProcessPage processPage, int indexEditEvent) {
    emit editTaskToServer(processPage, indexEditEvent);
}

void LogWindow::showTasks() {
    emit showTasksFromServer();
}

void LogWindow::on_btn_start_clicked() {
    emit startUploadTasksToServer();
}

void LogWindow::on_btn_edit_clicked() {
    emit changeSelectedTask();
}

void LogWindow::on_btn_stop_clicked() {
    emit stopUploadTasksToServer();
}

void LogWindow::on_btn_clean_clicked() {
    emit cleanUploadedTasks();
}

int LogWindow::checkUserIdByEmail(QString requestEmail) {
    return logController->requestCheckUserIdByEmail(requestEmail);
}

QString LogWindow::registerUserData(UserData userData) {
    return logController->requestRegisterUserData(userData);
}

bool LogWindow::updateUser(UserData userData) {
    return logController->requestUpdateUser(userData);
}

QString LogWindow::checkEmailByUserId(int requestUserId) {
    return logController->requestCheckEmailByUserId(requestUserId);
}

void LogWindow::sendToControllerProcessPage(ProcessPage processPage, bool isEnableChangeSelectedEvent) {
    emit requestTransferToControllerProcessPage(processPage, isEnableChangeSelectedEvent);
}


void LogWindow::getReplySetProcessPageOnUI(ProcessPage _processPage) {
    this->close();
    emit requestOutputProcessPage(_processPage);
}

void LogWindow::getRequestSetStateIsCloseApp() {
    emit requestBreakWaitStartTask();
}

void LogWindow::getCurrentProcessPageToXmlFile(ProcessPage processPage) {
    emit saveCurrentProcessPageToXmlFile(processPage);
}

void LogWindow::setToUiEventSpecification(const EventSpecification eventSpecification) {
    eventWindow->setEventSpecification(eventSpecification);
    eventWindow->show();
}

void LogWindow::tableRowDoubleClicked(int row, int column) {
    if (!tableUtils.isEmptyClickedRow(ui->tableWidget, row)) {
        emit requestEventSpecification(row);
    }
}

void LogWindow::tableRowClicked(int row, int column) {
    emit requestAddItemIntoMapSelectedTask(ui->tableWidget, row, column);
}

void LogWindow::closeEvent(QCloseEvent *event) {
    if (eventWindow->isVisible()) {
        eventWindow->close();
    }
}
