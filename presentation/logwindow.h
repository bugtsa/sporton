#ifndef LOGWINDOW_H
#define LOGWINDOW_H

#include <QMainWindow>

#include "domain/controllers/logcontroller.h"
#include "eventwindow.h"

namespace Ui {
class LogWindow;
}

class LogWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LogWindow(LogMessage *logMessage, QWidget *parent = 0);
    ~LogWindow();

    void addTask(ProcessPage processPage, bool isEditCurrentEventForServer);

    void editTask(ProcessPage processPage, int indexEditEvent);

    void showTasks();

    void sendToControllerProcessPage(ProcessPage processPage, bool isEditCurrentEventForServer);

    int checkUserIdByEmail(QString requestEmail);

    QString checkEmailByUserId(int requestUserId);

    QString registerUserData(UserData userData);

    bool updateUser(UserData userData);

protected:
    virtual void closeEvent (QCloseEvent *event);

signals:

    void initTableWidget(QTableWidget *pTableWidget);

    void addTaskToServer (ProcessPage processPage, bool isEditCurrentEventForServer);

    void editTaskToServer(ProcessPage processPage, int indexEditEvent);

    void clearLogWindow();

    void showTasksFromServer();

    void readyReplyListTasks(QList<ProcessPage> listTasks);

    void enableEditSelectedEvent(int indexSelectedEvent, QList<int> listSelectedRows);

    void startUploadTasksToServer();

    void changeSelectedTask();

    void stopUploadTasksToServer();

    void cleanUploadedTasks();

    void requestTransferToControllerProcessPage(ProcessPage processPage, bool isEnableChangeSelectedEvent);

    void requestAddItemIntoMapSelectedTask(QTableWidget *pTableWidget, int rowIndex, int columnIndex);

    void requestOutputProcessPage(ProcessPage _processPage);

    void saveCurrentProcessPageToXmlFile(ProcessPage processPage);

    void requestEventSpecification(int indexTask);

    void requestBreakWaitStartTask();

public slots:

    void getReplySetProcessPageOnUI(ProcessPage processPage);

    void getRequestSetStateIsCloseApp();

private slots:
    void getCurrentProcessPageToXmlFile(ProcessPage processPage);

    void on_btn_edit_clicked();

    void on_btn_stop_clicked();

    void on_btn_start_clicked();

    void on_btn_clean_clicked();

    void setToUiEventSpecification(EventSpecification eventSpecification);

    void tableRowDoubleClicked(int row, int column);

    void tableRowClicked(int row, int column);



private:
    Ui::LogWindow *ui;

    EventWindow *eventWindow;

    LogController  *logController ;

    TableUtils tableUtils;
private:
};

#endif // LOGWINDOW_H
