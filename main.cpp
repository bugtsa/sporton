#include "presentation/mainwindow.h"
#include "domain/callbacks/cbconnecter.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    connecter::setPtr(&w);
    w.show();

    return a.exec();
}
