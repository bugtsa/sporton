#-------------------------------------------------
#
# Project created by QtCreator 2016-02-23T12:19:34
#
#-------------------------------------------------

QT       += core gui widgets xml sql network webkit

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webkitwidgets

TARGET = Sporton
TEMPLATE = app

INCLUDEPATH += ./../qjson/include

win32:RC_ICONS += ./images/ic_launcher.ico

LIBS += ./../qjson/build/lib/libqjson-qt5.dll

SOURCES += main.cpp\
    data/xmlworker.cpp \
    data/sportondatamanager.cpp \
    data/processpage.cpp \
    data/eventspecification.cpp \
    data/logmessage.cpp \
    domain/executors/mainexecutor.cpp \
    domain/callbacks/cbconnecter.cpp \
    domain/controllers/testconnecter.cpp \
    domain/executors/logexecutor.cpp \
    domain/controllers/logcontroller.cpp \
    domain/controllers/maincontroller.cpp \
    presentation/mainwindow.cpp \
    presentation/logwindow.cpp \
    presentation/datetimedialog.cpp \
    presentation/eventwindow.cpp \
    google-place/google.cpp \
    google-place/form.cpp \
    google-place/placedialog.cpp \
    google-place/placesdatamanager.cpp \
    google-place/placesjsmanager.cpp \
    google-place/datamanagerhelper.cpp \
    google-place/settingsdialog.cpp \
    google-place/variantlistmodel.cpp \
    google-place/placedetailsdialog.cpp \
    google-place/tools.cpp \
    google-place/eventdialog.cpp \
    utils/checkboxutils.cpp \
    utils/maputils.cpp \
    utils/scrollmessagebox.cpp \
    utils/calendarutils.cpp \
    utils/messageutils.cpp \
    utils/generationutils.cpp \
    utils/tableutils.cpp \
    utils/processgui.cpp \
    utils/stringsutils.cpp \
    utils/itemdelegate.cpp

HEADERS  += data/xmlworker.h \
    data/eventspecification.h \
    data/processpage.h \
    data/sportondatamanager.h \
    data/logmessage.h \
    domain/executors/mainexecutor.h \
    domain/callbacks/cbconnecter.h \
    domain/controllers/testconnecter.h \
    domain/executors/logexecutor.h \
    domain/controllers/logcontroller.h \
    domain/controllers/maincontroller.h \
    presentation/mainwindow.h \
    presentation/logwindow.h \
    presentation/datetimedialog.h \
    presentation/eventwindow.h \
    google-place/google.h \
    google-place/form.h \
    google-place/placedialog.h \
    google-place/placesdatamanager.h \
    google-place/placesjsmanager.h \
    google-place/datamanagerhelper.h \
    google-place/settingsdialog.h \
    google-place/variantlistmodel.h \
    google-place/placedetailsdialog.h \
    google-place/tools.h \
    google-place/eventdialog.h \
    utils/checkboxutils.h \
    utils/constants.h \
    utils/processgui.h \
    utils/stringsutils.h \
    utils/maputils.h \
    utils/scrollmessagebox.h \    
    utils/calendarutils.h \
    utils/messageutils.h \
    utils/generationutils.h \
    utils/tableutils.h \
    utils/itemdelegate.h

FORMS   += presentation/mainwindow.ui \
    presentation/logwindow.ui \
    presentation/datetimedialog.ui \
    presentation/eventwindow.ui \
    google-place/form.ui \
    google-place/settingsdialog.ui \
    google-place/placedetailsdialog.ui \
    google-place/placedialog.ui \
    google-place/eventdialog.ui \
    google-place/goolge.ui

RESOURCES += \
    Sporton.qrc

OTHER_FILES += \
    html_js/index.html \
    html_js/init.js \
    html_js/tools.js \
    html_js/supported_types.txt \
    html_js/supported_languages.txt \
    other_files/supported_types.txt \
    other_files/supported_languages.txt
