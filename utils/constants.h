#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <QString>


static QString messageError = "Error!";

static QString messageCantOpen = "Can`t Open File";

static QString messageVersion = "v 0.0.32 ";

static const QString messageSupport = "Support: <b>copypastestd@gmail.com</b>";

static const QString messageTitleAboutApplication = "About Application";

static const QString messageBodyAboutApplication = "This is <b>Sporton</b> ";

static const QString messageTitleCheckCorrectInputTableData = "No correct input data!";

static const QString messageBodyCheckCorrectInputTableData = "Fill all no empty users rows in Table";

static const QString messageBodyCheckUserIdByEmail = "This is input email doesn`t registered on SportOn server";

static const QString messageBodyCheckEmailByUserId = "This is input userId doesn`t registered on SportOn server";

static const QString MESSAGE_BODY_NOT_VALIDATE_EMAIL = "This is input email doesn`t valid";

static const QString MESSAGE_BODY_REINPUT_NOT_VALIDATE_EMAIL = "Re input doesn`t valid emails";

static const QString messageTitleApplication = "Application";

static const QString messageBodyMayBeSave = "The document has been modified.\nDo you want to save your changes?";

static const QString messageTitleWarningScrollMessageBox = "Warning";

static const QString messageBodyNoExistsNetworkConnection = "Check your network connection";

static const int minimumHeightWarningScrollMessageBox = 80;

static const int minimumWidthWarningScrollMessageBox = 260;

//-------------Names Cities--------------------------------------------
static const QString nameCityAtlanta = "Atlanta";

static const QString nameCityBoston = "Boston";

static const QString nameCityDallas = "Dallas";

static const QString nameCityMiami = "Miami";

static const QString nameCityMontreal = "Montreal";

static const QString nameCityPhiladelphia = "Philadelphia";

static const QString nameCityToronto = "Toronto";

static const QString nameCityDefault = "Default";

//-------------Title Windows--------------------------------------------
static const QString windowTitleDialogStartOfEvent = "Start Of Event";

static const QString windowTitleDialogStartOfAutomation = "Start Of Automation";

//-------------Indexs in Log Activity--------------------------------------------
static const int indexTitleEvent = 0;

static const int indexTitleLogActivity = 0;

static const int indexStartOfAutomationLogActivity = 1;

static const int indexStartOfEventLogActivity = 2;

static const int indexStatus = 3;

static const int indexAdded = 4;

static const QString stringAdded = "Correct API for get DateTime";

//-------------Indexs in Main Activity--------------------------------------------
static const int indexColumnNote = 0;

static const int indexColumnEmail = 2;

//-------------Methods SportON Server API--------------------------------------------
static QString urlCreateEvent = "http://54.69.110.214/index.php/mobile/create_event";

static QString urlUpdateEvent = "http://54.69.110.214/index.php/mobile/update_event";

static QString urlGetEvents = "http://54.69.110.214/index.php/mobile/get_events";

static QString urlAddEventParticipation = "http://54.69.110.214/index.php/mobile/add_event_participation";

static QString urlGetConnectionStatus = "http://54.69.110.214/index.php/";

static QString urlRegisterUser = "http://54.69.110.214/index.php/mobile/register";

static QString urlUpdateUser = "http://54.69.110.214/index.php/mobile/update_user";

static QString urlGetEmailByUserId = "http://54.69.110.214/index.php/mobile/get_email_by_user_id?id=";

static QString urlGetUserIdByEmail = "http://54.69.110.214/index.php/mobile/get_user_id_by_email?email=";

//-------------TAG reply--------------------------------------------
static const QString replyResponseCode = "responseCode";

static const QString replyUser = "user";

static const QString replyUserId = "user_id";

static const QString replyShortUserId = "id";

static const QString replyEmail = "email";

static const QString replyMessage = "message";

//-------------Google Place--------------------------------------------
static const int valueRadius = 50000;

static const QString placeAPIkey = "AIzaSyBvKoDPISDSHg4ekbo2_PT7A4QMFb0WUW8";

static const QString langageRequest = "en";


static const QString logActivityCreateGame = "Creating Game ";

static const QString logActivityUser = "User ";

static const QString logActivityJoinToGame = " Join to Game ";

//-------------TAG Strings--------------------------------------------
static const QString tagValue = "value";

static const QString tagTitle = "title";

static const QString tagDescription = "description";

static const QString tagSportID = "sportID";

static const QString tagKindOfSportForApp = "kindOfSportForApp";

static const QString tagKindOfSportForServer = "kindOfSportForServer";

static const QString tagCity = "city";

static const QString tagLocation = "location";

static const QString tagCalendar = "calendar";

static const QString tagDateTimeOfEvent = "DateTimeOfEvent";

static const QString tagStringTimeStartOfEvent = "StringTimeStartOfEvent";

static const QString tagDateStartOfEvent = "DateOfEvent";

static const QString tagTimeStartOfEvent = "TimeStartOfEvent";

static const QString tagDateTimeOfAutomation = "DateTimeOfAutomation";

static const QString tagStringOfAutomation = "StringOfAutomation";

static const QString tagDateOfAutomation = "DateOfAutomation";

static const QString tagTimeOfAutomation = "TimeOfAutomation";

static const QString tagTime = "time";

static const QString tagQuantityPlayersInEvent = "quantityPlayersInEvent";

static const QString tagGroupIDtoJoin = "groupIDtoJoin";

static const QString tagRandomizeCaptain = "randomizeCaptain";

static const QString tagRepeatWeekByWeek = "repeatWeekByWeek";

static const QString tagDaysRepeat = "daysRepeat";

static const QString tagHoursRepeat = "hoursRepeat";

static const int valueQuantityHoursInDay = 24;

static const int valueQuantityDaysInWeek = 7;

static const int valueQuantityMonthsInYear = 12;


static const QString tagMain = "sporton";

static const QString tagEventHeader = "mainHeader";

static const QString tagTable = "table";

static const QString tagRowCount = "rowCount";

static const QString tagUserData = "row";

static const QString tagIdInUserData = "id";

static const QString tagNameInUserData = "name";

static const QString tagEmailInUserData = "email";

static const QString tagPasswordInUserData = "password";

static const QString tagProxyInUserData = "proxy";

static const QString tagEnabledInUserData = "enabled";

static const QString tagNoteInUserData = "note";


//-------------Process File Strings--------------------------------------------
static const QString textActionSave = "Save ";

static const QString textActionSaveAs = " as";

static const QString textExtentionXml = ".xml";

static const QString textExtentionLog = ".log";

static const QString textNameFileAutoSave = "untitled";

static const QString textExtentionAutoSave = ".autosave";

static const QString textNoNameFile = "noName";

static const QString textNoSaveFile = "*";

static const QString xmlDefaultDirectory = "./Xml_files";

static const QString logDefaultDirectory = "./Logs";


static const QString formatOfTime = "hh:mm";

static const QString defaultTime = "08:00:00";

static const QString formatOfDate = "yyyy-MM-dd";

static const QString formatOfDateForServer = "yy-mm-dd";

static const QString formatDateTimeForLogMessageFile = "yyyy-MM-dd_hh-mm-ss";

static const QString formatDateTimeWithoutSeconds = "yyyy-MM-dd hh:mm";

static const QString formatDateTimeFullWithSeconds = "yyyy-MM-dd hh:mm:ss";

static const QString formatHourClockAnteMeridiem = "AM";

static const QString formatHourClockPostMeridiem = "PM";

static const int value12HourAM = 0;

static const int value12HourPM = 12;

static const int firstQuantityRowsProcessPage = 100;

static const int firstQuantityRowsActivityLog = 50;

static const int lengthEventInHours = 2;

static const int lengthIntervalAtUploadNewEvent = 3;

static const int lengthTimeForSetRatingEvent = 3;

static const int QUANTITY_REPEAT_THE_SAME_EVENT = 3;

static const QString STRING_PENDING = "PENDING";

static const QString STRING_OK = "OK";

static const QString STRING_ERROR = "ERROR";

static const QString STRING_STOP = "STOP";

static const int delaySendToRequestTestConnect = 5;

static const int timeWaitToDestroyTestConnect = delaySendToRequestTestConnect * 2000;

static const QString stringContentTypeHeader = "application/x-www-form-urlencoded";

//-------------GENERAL REQUESTS----------------------------------------
static const QString testFieldForRequest = "test";

static const QString userIdFieldForRequest = "user_id";

static const QString idFieldForRequest = "id";

static const QString emailFieldForRequest = "email";

static const QString requestTypeFieldForRequest = "request_type";

static const QString requestTimeFieldForRequest = "request_time";

//-------------UPDATE USER REQUESTS----------------------------------------
static const QString userNameFieldForRequest = "user_name";

static const QString userDobFieldForRequest = "user_dob";

static const QString userAboutFieldForRequest = "user_about";

static const QString userLocationFieldForRequest = "user_location";

static const QString userImageFieldForRequest = "image";

//-------------EVENT REQUESTS----------------------------------------
static const QString userEmailFieldForRequest = "user_email";

static const QString userPasswordFieldForRequest = "user_password";

static const QString userLoginNameFieldForRequst = "user_login_name";

//-------------EVENT REQUESTS----------------------------------------
static const QString eventCategoryIdFieldForRequest = "category_id";

static const QString eventTitleFieldForRequest = "event_title";

static const QString eventDescriptionFieldForRequest = "event_description";

static const QString eventLocationFieldForRequest = "event_location";

static const QString eventStartDateFieldForRequest = "event_start_date";

static const QString eventEndDateField = "event_end_date";

static const QString eventRatingDateFieldForRequest = "event_rating_date";

static const QString eventPrivacyFieldForRequest = "event_privacy";

static const QString eventGenderFieldForRequest = "event_gender";

static const QString eventMinimumSkillsFieldForRequest = "event_minimum_skills";

static const QString eventPlayersFieldForRequest = "event_players";

static const QString eventPriceFieldForRequest = "event_price";

static const QString eventSendInviteFieldForRequest = "send_invite";

static const QString eventTypeFieldForRequest = "event_type";

//-------------GET EVENTS REQUEST----------------------------------------
static const QString userLatitudeFieldForRequest = "user_latitude";

static const QString userLongitudeFieldForRequest = "user_longitude";

//-------------RESPONSE GET EVENTS----------------------------------------
static const QString responseGetEventsListEvents = "events";

static const QString responseGetEventsValueUserID = "event_user_id";


static const QString eventIdForRequestAndResponse = "event_id";

#endif // CONSTANTS_H
