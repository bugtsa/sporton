#ifndef GENERATIONUTILS_H
#define GENERATIONUTILS_H

#include <QTime>

class GenerationUtils
{
public:
    GenerationUtils();

    int getRandomNumberBetweenPosNegIndex(int index);

    int getRandomNumber(int minIndex, int maxIndex);

    QString getRandomString() const;
};

#endif // GENERATIONUTILS_H
