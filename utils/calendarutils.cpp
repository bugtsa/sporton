#include "calendarutils.h"

CalendarUtils::CalendarUtils()
{

}

QDateTime CalendarUtils::processDateTimeWithShiftOnOneMinute(QDateTime dateTime, bool isUpMinute) {
    QTime time = dateTime.time();
    int deltaMinute = 0;

    if (isUpMinute) {
        deltaMinute = 1;
    } else {
        deltaMinute = -1;
    }
    int hours = time.hour();
    int minutes = time.minute() + deltaMinute;
    if (minutes == 60) {
        minutes = 0;
        hours++;
    }
    if (minutes == -1) {
        minutes = 59;
        hours--;
    }
    return checkDateTimeOnCorrectHoursValue(dateTime, hours, minutes);
}

QDateTime CalendarUtils::checkDateTimeOnCorrectHoursValue(QDateTime dateTime, int _hours, int _minutes) {
    QDateTime currentDateTime;
    QDate currentDate = dateTime.date();
    QTime currentTime = dateTime.time();
    int hours = _hours;
    int minutes;
    if (_minutes >= 0) {
        minutes = _minutes;
    } else {
        minutes = currentTime.minute();
    }
    int seconds = currentTime.second();
    if (hours == 24) {
        currentDate = processDateWithShift(currentDate, 1);
        hours = 0;
    }
    if (hours == -1) {
        currentDate = processDateWithShift(currentDate, -1);
        hours = 23;
    }
    currentTime.setHMS(hours, minutes, seconds, 0);
    currentDateTime.setDate(currentDate);
    currentDateTime.setTime(currentTime);
    return currentDateTime;
}

QDateTime CalendarUtils::processDateTimeWithShiftOnOneHour(QDateTime dateTime, bool isUpHour) {
    QTime time = dateTime.time();
    int deltaHour = 0;

    if (isUpHour) {
        deltaHour = 1;
    } else {
        deltaHour = -1;
    }
    int hours = time.hour() + deltaHour;
    return checkDateTimeOnCorrectHoursValue(dateTime, hours, -1);
}

QDate CalendarUtils::processDateWithShift(QDate tmpDate, int valueShiftDay) {
    int day = tmpDate.day();
    int valueDaysInMonth = tmpDate.daysInMonth();
    int processShiftDay = day + valueShiftDay;
    int year = tmpDate.year();
    int month = tmpDate.month();
    int currentDay = 0;
    bool isPlusAtCurrentMonth = false;
    bool isMinusAtCurrentMonth = false;

    if (processShiftDay <= valueDaysInMonth) {
        if (processShiftDay > 0) {
            isPlusAtCurrentMonth = false;
        } else {
            isMinusAtCurrentMonth = true;
        }
    } else {        
        isPlusAtCurrentMonth = true;
    }
    if (isPlusAtCurrentMonth) {
        if (month < valueQuantityMonthsInYear) {
            month++;
        } else {
            year++;
            month = 1;
        }
        currentDay = processShiftDay - valueDaysInMonth;
    } else {
        currentDay = processShiftDay;
    }
    if (isMinusAtCurrentMonth) {
        if (month != 1) {
            month--;
        } else {
            month = valueQuantityMonthsInYear;
            year--;
        }
        QDate currentDate = QDate(year, month, 1);
        valueDaysInMonth = currentDate.daysInMonth();
        if (processShiftDay == 0) {
            currentDay = valueDaysInMonth;
        } else {
            currentDay = valueDaysInMonth + processShiftDay + 1;
        }
    }
    return QDate(year, month, currentDay);
}

QDateTime CalendarUtils::processDateTimeWithShift(QDate tmpDate, int _valueShiftDays,
                                                  QTime tmpTime, int _valueShiftHours, bool isShiftPlusWeek) {
    int day = tmpDate.day();
    int valueShiftDays = _valueShiftDays;
    if (isShiftPlusWeek) {
        valueShiftDays += valueQuantityDaysInWeek;
    }
    QDate processDate = processDateWithShift(tmpDate, valueShiftDays);
    QDate currentDate;

    int hour = tmpTime.hour();
    int minute = tmpTime.minute();
    int valueShiftHours = hour + _valueShiftHours;
    QTime currentTime;
    if (valueShiftHours < valueQuantityHoursInDay) {
        if (valueShiftHours >= 0) {
            currentTime = QTime(valueShiftHours, minute);
            currentDate = processDate;
        } else {
            currentDate = processDateWithShift(processDate, -1);
            int currentHour = valueQuantityHoursInDay + valueShiftHours;
            currentTime = QTime(currentHour, minute);
        }
    } else {
        currentDate = processDateWithShift(processDate, 1);
        int currentHour = valueShiftHours - valueQuantityHoursInDay;
        currentTime = QTime(currentHour, minute);
    }
    return QDateTime(currentDate, currentTime);
}
