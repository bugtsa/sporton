#ifndef MAPUTILS_H
#define MAPUTILS_H

#include <QObject>
#include <QString>
#include <QMap>

#include "constants.h"

class Coordinate{
public:

    Coordinate ();

    Coordinate(QString _latitude, QString _longitude);

    QString latitude;

    QString longitude;    

};

class MapUtils: public QObject
{
    Q_OBJECT
public:
    explicit MapUtils(QObject *parent = 0);

    QMap<QString, int> getMapNamesCities();

    QString getCoordinatesByIntegerNameCity(int idInMapNameCity);

    QString getCoordinatesByStringNameCity(QString nameCity);

    QString getLatitudeCoordinateByStringNameCity(QString nameCity);

    QString getLongitudeCoordinateByStringNameCity(QString nameCity);

private:
    QMap<QString, int> mapNamesCities;

    QMap<QString, Coordinate> mapCitiesCoordinates;

private:
    QString getStringFromObjectClassCoordinate(const Coordinate coordinate);

    QString getStringLatitudeFromObjectClassCoordinate(const Coordinate coordinate);

    QString getStringLongitudeFromObjectClassCoordinate(const Coordinate coordinate);
};

#endif // MAPUTILS_H
