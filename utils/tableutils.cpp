#include "tableutils.h"

TableUtils::TableUtils()
{

}

void TableUtils::initActivityLogTableWidget(QTableWidget *pTableWidget) {
    setRowsTableWidget(pTableWidget, firstQuantityRowsActivityLog);
}

void TableUtils::setRowsTableWidget(QTableWidget *pTableWidget, int firstQuantityRows) {
    pTableWidget->setRowCount(firstQuantityRows);
}

void TableUtils::prepareTableWidget(QTableWidget *pTableWidget) {
    //set may be before resetProcessPage
    setRowsTableWidget(pTableWidget, firstQuantityRowsProcessPage);

    int lastIndexColumnTableWidget = getLastIndexColumnTableWidget(pTableWidget);
    for (int iRow = 0; iRow < pTableWidget->rowCount(); iRow++){
        pTableWidget->setItem(iRow, lastIndexColumnTableWidget, checkBoxUtils.getNewCheckBoxInItem(false));
    }
    clearAllCells(pTableWidget);
}

bool TableUtils::validateEmail(QString email) {
    bool isValidate = true;

    QRegularExpression regex("^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$");

    if(!regex.match(email).hasMatch()) {
        isValidate = false;
    }
    return isValidate;
}

QString TableUtils::getTextFromTableWidgetCell(QTableWidget *pTableWidget, int row, int column) {
    QTableWidgetItem* item = pTableWidget->item(row, column);
    if (!item || item->text().isEmpty()) {
        return "";
    } else {
        return pTableWidget->item(row, column)->text();
    }
}

int TableUtils::getIntFromTableWidgetCell(QTableWidget *pTableWidget, int row, int column) {
    int intFromStr = stringsUtils.convertStrToInt(pTableWidget->item(row, column)->text());
    if (intFromStr == 0) {
        intFromStr = -1;
    }
    return intFromStr;
}

int TableUtils::getLastIndexColumnTableWidget(QTableWidget *pTableWidget) {
    return pTableWidget->columnCount() - 1;
}

int TableUtils::getRowCountTableWidget(QTableWidget *pTableWidget) {
    return pTableWidget->rowCount();
}

void TableUtils::clearAllCells(QTableWidget *pTableWidget) {
    int lastIndexColumnTableWidget = getLastIndexColumnTableWidget(pTableWidget);
    for (int iRow = 0; iRow < pTableWidget->rowCount(); iRow++){
        for (int iCol = 0; iCol < lastIndexColumnTableWidget; iCol++){
            setTextInTableWidgetCell(pTableWidget, iRow, iCol, "");
        }
        checkBoxUtils.setStateCheckBox(pTableWidget, iRow, getLastIndexColumnTableWidget(pTableWidget), 0);
    }
    setOnTopLeftCell(pTableWidget);
}

void TableUtils::clearRow(QTableWidget *pTableWidget, int row) {
    for (int indColumn = 0; indColumn <= getLastIndexColumnTableWidget(pTableWidget); indColumn++) {
        setTextInTableWidgetCell(pTableWidget, row, indColumn, "");
    }
}

void TableUtils::clearCell(QTableWidget *pTableWidget, int row, int column) {
    setTextInTableWidgetCell(pTableWidget, row, column, "");
}

void TableUtils::setOnTopLeftCell(QTableWidget *pTableWidget) {
    pTableWidget->setCurrentCell(0, 0);
    pTableWidget->clearSelection();
}

void TableUtils::setTextInTableWidgetCell(QTableWidget *pTableWidget, int row, int column, QString textCell) {
    QTableWidgetItem *item = new QTableWidgetItem;
    item->setText(textCell);
    item->setTextAlignment(Qt::AlignCenter);
    pTableWidget->setCurrentCell(row, column);
    pTableWidget->setItem(row, column, item);
}

UserData TableUtils::getUserDataFromTableWidget(QTableWidget *pTableWidget, int indexRow) {
    UserData userData;
    userData.note = getTextFromTableWidgetCell(pTableWidget, indexRow, 0);
    userData.name = getTextFromTableWidgetCell(pTableWidget, indexRow, 1);
    userData.email = getTextFromTableWidgetCell(pTableWidget, indexRow, 2);
    userData.password = getTextFromTableWidgetCell(pTableWidget, indexRow, 3);
    userData.proxy = getTextFromTableWidgetCell(pTableWidget, indexRow, 4);
    userData.enabled = checkBoxUtils.getStateCheckBox(pTableWidget, indexRow, getLastIndexColumnTableWidget(pTableWidget));
    return userData;
}

int TableUtils::getQuantityPlayersInEvent(QTableWidget *pTableWidget, ProcessPage *processPage) {
    int lastIndexColumnTableWidget = getLastIndexColumnTableWidget(pTableWidget);
    int quantityPlayers = 0;
    if (lastIndexColumnTableWidget <= 0) {
        //error message
    }
    for (int indexRow = 0; indexRow < processPage->rowCount; indexRow++) {
        if (checkBoxUtils.getStateCheckBox(pTableWidget, indexRow, lastIndexColumnTableWidget)) {
            quantityPlayers++;
        }
    }
    return quantityPlayers;
}

//void TableUtils::changeUserIdInCurrentRow(QTableWidget *pTableWidget, int indexRow, int userId) {
//    if (getTextFromTableWidgetCell(pTableWidget, indexRow, indexColumnUserId).toInt() != userId) {
//        setTextInTableWidgetCell(pTableWidget, indexRow, indexColumnUserId, QString("%1").arg(userId));
//    }
//}

void TableUtils::changeEmailByUserId(QTableWidget *pTableWidget, int indexRow, QString email) {
    if (getTextFromTableWidgetCell(pTableWidget, indexRow, indexColumnEmail) != email) {
        setTextInTableWidgetCell(pTableWidget, indexRow, indexColumnEmail, email);
    }
}

bool TableUtils::isEmptyClickedRow(QTableWidget *pTableWidget, int indexRow) {
    bool isEmptyRow = true;

    for (int indexColumn = 0; indexColumn < getLastIndexColumnTableWidget(pTableWidget); indexColumn++) {
        if (getTextFromTableWidgetCell(pTableWidget, indexRow, indexColumn) != "") {
            isEmptyRow = false;
            break;
        }
    }
    return isEmptyRow;
}
