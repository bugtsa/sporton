#ifndef TABLEUTILS_H
#define TABLEUTILS_H

#include <QString>
#include <QTableWidget>
#include <QRegularExpression>

#include "data/processpage.h"
#include "utils/checkboxutils.h"
#include "utils/stringsutils.h"

class TableUtils
{
public:
    TableUtils();

    //---------------Table Widget Utils--------------------------------------
    void initActivityLogTableWidget(QTableWidget *pTableWidget);

    void changeEmailByUserId(QTableWidget *pTableWidget, int indexRow, QString email);

    void setRowsTableWidget(QTableWidget *pTableWidget, int firstQuantityRows);

    void setOnTopLeftCell(QTableWidget *pTableWidget);

    void prepareTableWidget(QTableWidget *pTableWidget);

    int getRowCountTableWidget(QTableWidget *pTableWidget);

    int getLastIndexColumnTableWidget(QTableWidget *pTableWidget);

    void setTextInTableWidgetCell(QTableWidget *pTableWidget, int row, int column, QString textCell);

    QString getTextFromTableWidgetCell(QTableWidget *pTableWidget, int row, int column);

    int getIntFromTableWidgetCell(QTableWidget *pTableWidget, int row, int column);

    UserData getUserDataFromTableWidget(QTableWidget *pTableWidget, int indexRow);

    int getQuantityPlayersInEvent(QTableWidget *pTableWidget, ProcessPage *processPage);

    bool isEmptyClickedRow(QTableWidget *pTableWidget, int indexRow);

    void clearAllCells(QTableWidget *pTableWidget);

    void clearRow(QTableWidget *pTableWidget, int row);

    void clearCell(QTableWidget *pTableWidget, int row, int column);

    bool validateEmail(QString email);

private:

    CheckBoxUtils checkBoxUtils;

    StringsUtils stringsUtils;

};

#endif // TABLEUTILS_H
