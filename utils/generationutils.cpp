#include "generationutils.h"

GenerationUtils::GenerationUtils()
{

}

int GenerationUtils::getRandomNumberBetweenPosNegIndex(int index) {
   QTime time = QTime::currentTime();
   qsrand((uint)time.msec());

   int randomIndex = qrand() % (2* index + 1) - index;
   return randomIndex;
}

int GenerationUtils::getRandomNumber(int minIndex, int maxIndex) {
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    if (minIndex > maxIndex) {
        int temp = maxIndex;
        maxIndex = minIndex;
        minIndex = temp;
    }
    int randomIndex = qrand() % ((maxIndex + 1) - minIndex) + minIndex;
    return randomIndex;
}

QString GenerationUtils::getRandomString() const
{
   const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
   const int randomStringLength = 12; // assuming you want random strings of 12 characters
   QTime midnight(0,0,0);
   qsrand(midnight.secsTo(QTime::currentTime()));

   QString randomString;
   for(int i=0; i<randomStringLength; ++i)
   {
       int index = qrand() % possibleCharacters.length();
       QChar nextChar = possibleCharacters.at(index);
       randomString.append(nextChar);
   }
   return randomString;
}

