#include "maputils.h"

Coordinate::Coordinate() {

}

Coordinate::Coordinate(QString _latitude, QString _longitude) {
    latitude = _latitude;
    longitude = _longitude;
}

MapUtils::MapUtils(QObject *parent) : QObject(parent) {
    mapNamesCities[nameCityAtlanta] = 0;
    mapNamesCities[nameCityBoston] = 1;
    mapNamesCities[nameCityDallas] = 2;
    mapNamesCities[nameCityMiami] = 3;
    mapNamesCities[nameCityMontreal] = 4;
    mapNamesCities[nameCityPhiladelphia] = 5;
    mapNamesCities[nameCityToronto] = 6;

    mapCitiesCoordinates.insert("Atlanta", Coordinate("33.75694", "-84.39028"));
    mapCitiesCoordinates.insert("Boston", Coordinate("42.35778", "-71.06167"));
    mapCitiesCoordinates.insert("Dallas", Coordinate("32.78250", "-96.79750"));
    mapCitiesCoordinates.insert("Miami", Coordinate("25.78333", "-80.21667"));
    mapCitiesCoordinates.insert("Montreal", Coordinate("45.508888888", "-73.554166666"));
    mapCitiesCoordinates.insert("Philadelphia", Coordinate("39.95306", "-75.16333"));
    mapCitiesCoordinates.insert("Toronto", Coordinate("43.648536", "-79.385372"));
    mapCitiesCoordinates.insert("Default", Coordinate("-34.397", "150.644"));
}

QMap<QString, int> MapUtils::getMapNamesCities() {
    return mapNamesCities;
}

QString MapUtils::getStringFromObjectClassCoordinate(const Coordinate coordinate) {
    return coordinate.latitude + ", " + coordinate.longitude;
}

QString MapUtils::getStringLatitudeFromObjectClassCoordinate(const Coordinate coordinate) {
    return coordinate.latitude;
}

QString MapUtils::getStringLongitudeFromObjectClassCoordinate(const Coordinate coordinate) {
    return coordinate.longitude;
}

QString MapUtils::getCoordinatesByStringNameCity(QString nameCity) {
    return getStringFromObjectClassCoordinate(mapCitiesCoordinates.value(nameCity));
}

QString MapUtils::getLatitudeCoordinateByStringNameCity(QString nameCity) {
    return getStringLatitudeFromObjectClassCoordinate(mapCitiesCoordinates.value(nameCity));
}

QString MapUtils::getLongitudeCoordinateByStringNameCity(QString nameCity) {
    return getStringLongitudeFromObjectClassCoordinate(mapCitiesCoordinates.value(nameCity));
}

QString MapUtils::getCoordinatesByIntegerNameCity(int idInMapNameCity) {
    switch(idInMapNameCity){
    case 0:
        return "33.75694,-84.39028";
    case 1:
        return "42.35778,-71.06167";
    case 2:
        return "32.78250,-96.79750";
    case 3:
        return "25.78333,-80.21667";
    case 4:
        return "45.508888888,-73.554166666";
    case 5:
        return "39.95306,-75.16333";
    case 6:
        return "43.648536,-79.385372";
    default:
        return "-34.397, 150.644";
    }
}

