#ifndef STRINGSUTILS_H
#define STRINGSUTILS_H

#include <QString>
#include <QTime>

class StringsUtils
{
public:
    StringsUtils();

    int convertStrToInt(QString text);

    QString convertIntToStr(int value);

    bool convertStrToBool(QString text);    

    QString checkIdInTableWidget(int id);

    QString getNameHost(QString stringProxy);

    QString getPortHost(QString stringProxy);

    QString removeSpecSymbols(const QString email);

private:

    bool okConvert;

    bool isItemEmpty(QString textItem);
};

#endif // STRINGSUTILS_H
