#ifndef CHECKBOXUTILS_H
#define CHECKBOXUTILS_H

#include <QWidget>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QTableWidget>

class CheckBoxUtils
{
public:
    CheckBoxUtils();
    
public:
    QCheckBox* getNewCheckBox();

    QCheckBox* getNewCheckBox(bool checked);

    QTableWidgetItem* getNewCheckBoxInItem(bool checked);

    void setStateCheckBox(QTableWidget *pTableWidget, int row, int column, bool checked);

    bool getStateCheckBox(QTableWidget *pTableWidget, int row, int column);

private:
    void setStateCheckBoxWithoutCenterAlignmentInItem(QTableWidget *pTableWidget, int row, int column, bool checked);

    bool getStateCheckBoxWithoutCenterAlignmentInItem(QTableWidget *pTableWidget, int row, int column);

    QWidget* createCheckBoxWithCenterAlignment(bool checked);

    void setStateCheckBoxWithCenterAlignment(QTableWidget *pTableWidget, int row, int colunm, bool checked);

    bool getStateCheckBoxWithCenterAlignment(QTableWidget *pTableWidget, int row, int column);

    QCheckBox* createCheckBoxWithoutCenterAlignment(bool checked);

    void setStateCheckBoxWithoutCenterAlignment(QTableWidget *pTableWidget, int row, int column, bool checked);

    bool getStateCheckBoxWithoutCenterAlignment(QTableWidget *pTableWidget, int row, int column);
};

#endif // CHECKBOXUTILS_H
