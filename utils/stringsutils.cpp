#include "stringsutils.h"

StringsUtils::StringsUtils()
{

}

int StringsUtils::convertStrToInt(QString text) {
    if (isItemEmpty(text)) {
        return 0;
    }
    else {
        return text.toInt(&okConvert, 0);
    }
}

QString StringsUtils::convertIntToStr(int value) {
    return QString("%1").arg(value);
}

bool StringsUtils::isItemEmpty(QString textItem) {
    if (textItem.isEmpty() || textItem.isNull() || textItem == "")
        return true;
    else
        return false;
}

bool StringsUtils::convertStrToBool(QString text) {
    if (isItemEmpty(text)) {
        return false;
    }
    else {
        return text.toInt(&okConvert, 0);
    }
}

QString StringsUtils::checkIdInTableWidget(int id) {
    if (id == -1) {
        return "";
    }
    else {
        return QString("%1").arg(id);
    }
}

QString StringsUtils::getNameHost(QString stringProxy) {
    return stringProxy.left(stringProxy.indexOf(":"));
}

QString StringsUtils::getPortHost(QString stringProxy) {
    int indexSymbol = stringProxy.indexOf(":");
    if (indexSymbol > 0) {
        indexSymbol++;
        return stringProxy.right(stringProxy.length() - indexSymbol);
    } else {
        return "";
    }
}

QString StringsUtils::removeSpecSymbols(const QString _email) {
    QString email = _email;
    email = email.simplified();
    email = email.remove(QRegExp("\""));
    email = email.remove(QRegExp(" "));
    return email;
}
