#include "processgui.h"

ProcessGUI::ProcessGUI()
{

}

bool ProcessGUI::existsCurrentProcessPageInListTasks(ProcessPage *processPageFromView, QList<ProcessPage> listTasks) {
    for (int indList = 0; indList < listTasks.size(); indList++) {
        if (isProcessPagesMatch(processPageFromView, listTasks.at(indList))) {
            return true;
        } else {
            continue;
        }
    }
    return false;
}

bool ProcessGUI::isProcessPagesMatch(ProcessPage *processPageFromView, const ProcessPage processPageFromListTasks) {
    if (processPageFromView->generatedString == processPageFromListTasks.generatedString) {
        return true;
    } else {
        return false;
    }
}

void ProcessGUI::initComboBoxCity(QComboBox *comboBoxCity) {
    QMap <QString, int> mapNamesCities = mapUtils.getMapNamesCities();
    comboBoxCity->setItemText(0, mapNamesCities.key(0, "Balashikha"));
    comboBoxCity->setItemText(1, mapNamesCities.key(1, "Balashikha"));
    comboBoxCity->setItemText(2, mapNamesCities.key(2, "Balashikha"));
    comboBoxCity->setItemText(3, mapNamesCities.key(3, "Balashikha"));
    comboBoxCity->setItemText(4, mapNamesCities.key(4, "Balashikha"));
    comboBoxCity->setItemText(5, mapNamesCities.key(5, "Balashikha"));
    comboBoxCity->setItemText(6, mapNamesCities.key(6, "Balashikha"));
}

int ProcessGUI::getIdKindOfSport(int indexComboBoxToKindOfSport) {
    switch(indexComboBoxToKindOfSport) {
        //Hockey
        case 0:
            return 16;
            break;
        //Golf
        case 1:
            return 17;
            break;
        //Football
        case 2:
            return 18;
            break;
        //Soccer
        case 3:
            return 19;
            break;
        //Tennis
        case 4:
            return 20;
            break;
        //Baseball
        case 5:
            return 21;
            break;
        //Ski
        case 6:
            return 22;
            break;
        //Cricket
        case 7:
            return 23;
            break;
        //VolleyBall
        case 8:
            return 24;
            break;
        //Basketball
        case 9:
            return 25;
            break;
        //Ping Pong
        case 10:
            return 26;
            break;
        //Badminton
        case 11:
            return 27;
            break;
        //Running
        case 12:
            return 28;
            break;
        //Yoga
        case 13:
            return 29;
            break;
        //Boxing
        case 14:
            return 30;
            break;
        //Martial Arts
        case 15:
            return 31;
            break;
        //Gym
        case 16:
            return 32;
            break;
        //Rollerblade
        case 17:
            return 33;
            break;
        //Cycling
        case 18:
            return 34;
            break;
        //Watersports
        case 19:
            return 35;
            break;
        //Climbing
        case 20:
            return 36;
            break;
        //Other
        case 21:
            return 37;
            break;
        default:
            return 16;
    }
}
