#ifndef CALENDARUTILS_H
#define CALENDARUTILS_H

#include <QDate>

#include "utils/constants.h"

class CalendarUtils
{
public:
    CalendarUtils();

public:    
    QDateTime processDateTimeWithShiftOnOneMinute(QDateTime dateTime, bool isUpMinute);

    QDateTime processDateTimeWithShiftOnOneHour(QDateTime dateTime, bool isUpHour);

    QDate processDateWithShift(QDate tmpDate, int valueShiftDays);

    QDateTime processDateTimeWithShift(QDate tmpDate, int _valueShiftDays,
                                                      QTime tmpTime, int _valueShiftHours, bool isShiftPlusWeek);

private:
    QDateTime checkDateTimeOnCorrectHoursValue(QDateTime dateTime, int _hours, int _minutes);
};

#endif // CALENDARUTILS_H
