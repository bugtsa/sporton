#include "checkboxutils.h"

CheckBoxUtils::CheckBoxUtils()
{

}

QCheckBox* CheckBoxUtils::getNewCheckBox() {
    return new QCheckBox();
}

QWidget* CheckBoxUtils::createCheckBoxWithCenterAlignment(bool checked){
    QWidget *pWidget = new QWidget();
    QCheckBox *pCheckBox = new QCheckBox();
    QHBoxLayout *pLayout = new QHBoxLayout(pWidget);
    pCheckBox->setChecked(checked);
    pLayout->addWidget(pCheckBox);
    pLayout->setAlignment(Qt::AlignCenter);
    pLayout->setContentsMargins(0,0,0,0);
    pWidget->setLayout(pLayout);
    return pWidget;
}

QCheckBox* CheckBoxUtils::createCheckBoxWithoutCenterAlignment(bool checked) {
    QCheckBox *checkBoxTemp = new QCheckBox();
    checkBoxTemp->setChecked(checked);
    return checkBoxTemp;
}

QCheckBox* CheckBoxUtils::getNewCheckBox(bool checked) {
    createCheckBoxWithoutCenterAlignment(checked);
}

QTableWidgetItem* CheckBoxUtils::getNewCheckBoxInItem(bool checked) {
    QTableWidgetItem *tempItem = new QTableWidgetItem;
    if (checked) {
        tempItem->setCheckState(Qt::Checked);
    } else if (!checked) {
        tempItem->setCheckState(Qt::Unchecked);
    }
    return tempItem;
}

void CheckBoxUtils::setStateCheckBoxWithCenterAlignment(QTableWidget *pTableWidget, int row, int column, bool checked) {
    try {
        QWidget *mainWidget = qobject_cast<QWidget *>(pTableWidget->cellWidget(row, column));
        QHBoxLayout *hBoxLayout = qobject_cast<QHBoxLayout *>(mainWidget->layout());
        QLayoutItem *item = hBoxLayout->layout()->takeAt(0);
        QWidget* widget = item->widget();
        QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget);
        checkBox->setChecked(checked);
        hBoxLayout->addWidget(checkBox);
        hBoxLayout->setAlignment(Qt::AlignCenter);
        hBoxLayout->setContentsMargins(0, 0, 0, 0);
        mainWidget->setLayout(hBoxLayout);
        pTableWidget->setCellWidget(row, column, mainWidget);
    } catch (...) {
    }
}

void CheckBoxUtils::setStateCheckBoxWithoutCenterAlignment(QTableWidget *pTableWidget, int row, int column, bool checked) {
    QCheckBox *checkBox = qobject_cast<QCheckBox *>(pTableWidget->cellWidget(row, column));
    if (checkBox) {
        checkBox->setChecked(checked);
        pTableWidget->setCellWidget(row, column, qobject_cast<QWidget *>(checkBox));
    }
}

void CheckBoxUtils::setStateCheckBoxWithoutCenterAlignmentInItem(QTableWidget *pTableWidget, int row, int column, bool checked) {
    QTableWidgetItem * itemCheckBox = pTableWidget->item(row, column);
    if (itemCheckBox) {
        if (checked) {
            pTableWidget->item(row, column)->setCheckState(Qt::Checked);
        } else if (!checked) {
            pTableWidget->item(row, column)->setCheckState(Qt::Unchecked);
        }
    }
}

void CheckBoxUtils::setStateCheckBox(QTableWidget *pTableWidget, int row, int column, bool checked) {
    setStateCheckBoxWithoutCenterAlignmentInItem(pTableWidget, row, column, checked);
}

bool CheckBoxUtils::getStateCheckBoxWithCenterAlignment(QTableWidget *pTableWidget, int row, int column) {
    try {
        QWidget *mainWidget = qobject_cast<QWidget *>(pTableWidget->cellWidget(row, column));
        QHBoxLayout *hBoxLayout = qobject_cast<QHBoxLayout *>(mainWidget->layout());
        QLayoutItem *item = hBoxLayout->layout()->takeAt(0);
        QWidget* widget = item->widget();
        QCheckBox *chechBox = qobject_cast<QCheckBox *>(widget);
        return chechBox->isChecked();
    } catch (...) {
        return false;
    }
}

bool CheckBoxUtils::getStateCheckBoxWithoutCenterAlignment(QTableWidget *pTableWidget, int row, int column) {
    return qobject_cast<QCheckBox *>(pTableWidget->cellWidget(row, column))->isChecked();
}

bool CheckBoxUtils::getStateCheckBoxWithoutCenterAlignmentInItem(QTableWidget *pTableWidget, int row, int column) {
    if (pTableWidget->item(row, column)->checkState() == Qt::Checked) {
        return true;
    } else if (pTableWidget->item(row, column)->checkState() == Qt::Unchecked) {
        return false;
    }
}
bool CheckBoxUtils::getStateCheckBox(QTableWidget *pTableWidget, int row, int column) {
    return getStateCheckBoxWithoutCenterAlignmentInItem(pTableWidget, row, column);
}
