#ifndef MESSAGEUTILS_H
#define MESSAGEUTILS_H

#include <QMessageBox>
#include <QWidget>
#include <QString>

class MessageUtils
{
public:
    MessageUtils();

    void createWarningMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage);

    void createAboutMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage);

    void createInformationMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage);

    QMessageBox::StandardButton createWarningMessageWithStandartButtons(QWidget *pWidget, QString titleMessage, QString bodyMessage);
};

#endif // MESSAGEUTILS_H
