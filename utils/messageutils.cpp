#include "messageutils.h"

MessageUtils::MessageUtils()
{

}

//void MessageUtils::createErrorMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage) {
//    QMessageBox::
//}

void MessageUtils::createWarningMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage) {
    QMessageBox::warning(pWidget, titleMessage, bodyMessage);
}

void MessageUtils::createAboutMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage) {
    QMessageBox::about(pWidget, titleMessage, bodyMessage);
}

void MessageUtils::createInformationMessage(QWidget *pWidget, QString titleMessage, QString bodyMessage) {
    QMessageBox::information(pWidget, titleMessage, bodyMessage, QMessageBox::Ok);
}

QMessageBox::StandardButton MessageUtils::createWarningMessageWithStandartButtons(QWidget *pWidget, QString titleMessage, QString bodyMessage) {
    return QMessageBox::warning(pWidget, titleMessage, bodyMessage,
                     QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
}
