#ifndef PROCESSGUI_H
#define PROCESSGUI_H

#include <QString>
#include <QTableWidget>
#include <QComboBox>

#include "checkboxutils.h"
#include "stringsutils.h"
#include "maputils.h"
#include "data/processpage.h"

class ProcessGUI
{
public:
    ProcessGUI();

    bool okConvert;

    CheckBoxUtils checkBoxUtils;

    StringsUtils stringsUtils;

    MapUtils mapUtils;

public:

    //---------------Process Page Utils--------------------------------------
    bool existsCurrentProcessPageInListTasks(ProcessPage *processPageFromView, QList<ProcessPage> listTasks);

    bool isProcessPagesMatch(ProcessPage *processPageFromView, const ProcessPage processPageFromListTasks);

    int getIdKindOfSport(int indexComboBoxToKindOfSport);

    //---------------Map Utils--------------------------------------
    void initComboBoxCity(QComboBox *comboBoxCity);
};

#endif // PROCESSGUI_H
