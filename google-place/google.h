#ifndef GOOGLE_H
#define GOOGLE_H

#include <QMainWindow>

namespace Ui {
class Google;
}

class Google : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Google(QWidget *parent = 0);
    ~Google();

    void setDefaultLocation(QString nameCity, int indexCity);

protected:
void closeEvent( QCloseEvent *);

private:
    Ui::Google * ui;
    class Form * m_pForm;
};

#endif // GOOGLE_H
