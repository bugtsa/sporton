#include "placesjsmanager.h"

#include <QWebFrame>
#include <QMessageBox>

#include "./../qjson/include/serializer.h"

PlacesJsManager::
PlacesJsManager(QObject *parent) :
    QObject(parent)
{
}

void PlacesJsManager::setFrame(QWebFrame * frame) {
    m_pFrame = frame;
}

QString PlacesJsManager::
getCurrentPointOfView() const
{
    return eval( "getCurrentPointOfView()" ).toString();
}

QVariant PlacesJsManager::
eval(const QString & script) const
{
    return m_pFrame->evaluateJavaScript( script );
}

void PlacesJsManager::
removeMarkers()
{
    eval( "removeMarkers()" );
}

QString PlacesJsManager::getReference(int indexPlace) {
    return listReferences.at(indexPlace);
}

void PlacesJsManager::
createMarkers(const QVariantList & list)
{
    QJson::Serializer serializer;
    QString json;

    listReferences.clear();
    for (int indexList = 0; indexList < list.size(); indexList++) {
        QVariantMap mapItemListMarkers = list.at(indexList).toMap();
        QString reference = mapItemListMarkers["reference"].toString();
        listReferences.push_back(reference);
    }
    foreach(const QVariant & el, list)
    {
        json = serializer.serialize( el );
        eval( QString("appendMarker(%1)").arg( json ) );
    }
}

void PlacesJsManager::
recreateSearchCircle(const QString & location, const QString & radius)
{
    eval( QString("recreateSearchCircle(%1, %2)").arg(location, radius) );
}

void PlacesJsManager::
gotoPlace(const QVariant & place, int zoom)
{
    QJson::Serializer serializer;
    QString json = serializer.serialize( place );

    eval( QString("gotoPlace(%1, %2)").arg( json ).arg(zoom) );
}

void PlacesJsManager::
gotoLocation( const QString & location, int zoom )
{
    eval( QString("gotoLocation(%1, %2)").arg( location ).arg(zoom) );
}
