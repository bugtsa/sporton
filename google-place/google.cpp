#include "google.h"
#include "ui_goolge.h"

#include "form.h"

Google::Google(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Google)
{
    ui->setupUi(this);
    m_pForm = new Form(this);
    setCentralWidget(m_pForm);

    connect(ui->actionSettings, SIGNAL(triggered()), m_pForm, SLOT(editSettings()));
    connect(ui->actionAdd_place, SIGNAL(triggered()), m_pForm, SLOT(addPlace()));
}

Google::~Google()
{
    delete ui;
}

void Google::setDefaultLocation(QString nameCity, int indexCity) {
    m_pForm->setDefaultLocation(nameCity, indexCity);
}

void Google::closeEvent( QCloseEvent * ev )
{

}
