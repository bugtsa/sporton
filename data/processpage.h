#ifndef PROCESSPAGE_H
#define PROCESSPAGE_H
#include <QString>
#include <QVariant>
#include <QList>
#include <QTime>
#include <QDate>

#include "utils/constants.h"
#include "utils/stringsutils.h"
#include "utils/calendarutils.h"
#include "utils/generationutils.h"

//event header
class EventHeader{
public:
    QString filePathToOpenFile;

    QString title;
    QString description;

    int kindOfSportForApp;
    int kindOfSportForServer;
    int city;
    int quantityPlayersInEvent;
    int groupIDtoJoin;

    QString stKindOfSport;
    QString stCity;
    QString stGroupIDtoJoin;

    QString location;

    QDate dateStartOfEvent;
    QTime timeStartOfEvent;
    QString stringDateTimeStartOfEvent;

    QDate dateEndOfEvent;
    QTime timeEndOfEvent;
    QString stringDateTimeEndOfEvent;

    QString ratingDateOfEvent;

    QDate dateOfAutomation;
    QTime timeOfAutomation;
    QString stringDateTimeOfAutomation;

    QDateTime dateTimeOfNextEvent;
    QDateTime dateTimeOfNextAutomation;

    bool randomizeCaptain;
    bool repeatWeekByWeek;
    bool daysRepeat;
    int valueDaysRepeat;
    QString stDaysRepeat;
    bool hoursRepeat;
    int valueHoursRepeat;
    QString stHoursRepeat;
    QString status;
    int indexRowCaptainEvent;
    //
    int indexEventInLogWindow;
};

//user data information
class UserData{
public:
    UserData();

public:
    int id;
    QString name;
    QString email;
    QString password;
    QString proxy;
    bool enabled;
    QString note;
    QString status;
};

//Class for information at Events
class ProcessPage
{
public:
    ProcessPage();
    EventHeader eventHeader;
    QList<UserData> listUserData;
    int rowCount;
    QString status;
    QString generatedString;

private:
    StringsUtils stringsUtils;

    GenerationUtils generationUtils;

    CalendarUtils calendarUtils;

public:
    bool isEmptyProcessPage();

    bool isEmptyUserData(UserData userData);

    bool isStatusUploadToServer();

    QString getStatusStringUserData (int indListUserData);

    void setDateTimeStartOfEvent(QDateTime tmpDateTime);

    QDateTime getDateTimeStartOfEvent();

    QString getStringDateTimeStartOfEvent();

    QDateTime getDateTimeEndOfEvent();

    QDate getDateEndOfEvent();

    QTime getTimeEndOfEvent();

    void setDateTimeEndOfEvent();

    QTime getTimeEndOfEvent(QTime timeStartOfEvent);

    void setDateTimeOfAutomation(QDateTime tmpDateTime);

    QDateTime getDateTimeOfAutomation();

    QString getStringDateTimeOfAutomation();

    QDateTime getShiftToStartEvent(int lengthShiftInHours);

    QString getStringCity();

    int getQuantityNoEmptyRows();

    void initializeListFillRows();

    int getQuantityFillRows();

    QList<int> getListFillRows();

    void initializeListEnableUsers();

    int getQuantityEnableUsers();

    QList<int> getListEnableUsers();

    void getNewGeneratedString();

    void refreshItemUserData(int indexInsert, UserData userData);

    void reset(int rowCount);

    void swapProcessPage(ProcessPage _processPage);

    void setCellValueInUserData(int row, int column, QVariant cellValue);

    void processIndexRowCaptainEvent();

private:
    int quantityFillRows;

    QList <int> listFillRows;

    int quantityEnableUsers;

    QList <int> listEnableUsers;

};

#endif // PROCESSPAGE_H
