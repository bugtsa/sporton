#include "xmlworker.h"

XmlWorker::XmlWorker()
{

}

void XmlWorker::CreateEmptyXmlFile(QFile *file, int rowCount)
{
    if(file->open(QIODevice::WriteOnly)) {
        QDomDocument tmpDoc("xmlSporton");
        QDomElement mainElement = tmpDoc.createElement(tagMain);
        tmpDoc.appendChild(mainElement);
        QDomElement  eventHeaderElement = tmpDoc.createElement(tagEventHeader);
        mainElement.appendChild(eventHeaderElement);

        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagTitle, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagDescription, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagSportID,
                                                  tagKindOfSportForApp, "",
                                                  tagKindOfSportForServer, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagCity, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagLocation, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagDateTimeOfEvent,
                                                  tagStringTimeStartOfEvent, "",
                                                  tagDateStartOfEvent, "",
                                                  tagTimeStartOfEvent, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagDateTimeOfAutomation,
                                                  tagStringOfAutomation, "",
                                                  tagDateOfAutomation, "",
                                                  tagTimeOfAutomation, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagQuantityPlayersInEvent, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagGroupIDtoJoin, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagRandomizeCaptain, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagRepeatWeekByWeek, tagValue, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagDaysRepeat, tagValue, "", tagDaysRepeat, ""));
        eventHeaderElement.appendChild(fieldHeader(tmpDoc, tagHoursRepeat, tagValue, "", tagHoursRepeat, ""));

        QDomElement tableElement = makeElement(tmpDoc, tagTable, tagRowCount, QString("%1").arg(rowCount));
        mainElement.appendChild(tableElement);
        for(int row = 0; row < rowCount; row++){
            QDomElement rowElement = makeRowElement(tmpDoc, tagUserData);
            tableElement.appendChild(rowElement);
        }

        QTextStream(file) << tmpDoc.toString();
        file->close();
    }
}

bool XmlWorker::ReadXmlWithDomTree(QFile *file, ProcessPage *processPage)
{
    QDomElement root, dataElem;
    QDomNode dataNode;
    bool flReadFile = false;

    if(file->open(QIODevice::ReadOnly))
    {
        if(domDoc.setContent(file))
        {
            processPage->reset(processPage->rowCount);
            root = domDoc.documentElement();    //возвращает корневой элемент документа

            dataNode = root.firstChild();

            if (dataNode.toElement().tagName() == tagEventHeader)
            {
                flReadFile = true;
                processPage->eventHeader.filePathToOpenFile = file->fileName();
                dataElem = dataNode.firstChildElement(tagTitle);
                processPage->eventHeader.title = dataElem.attribute(tagValue, "");
                dataElem = dataElem.nextSiblingElement(tagDescription);
                processPage->eventHeader.description = dataElem.attribute(tagValue, "");
                dataElem = dataNode.firstChildElement(tagSportID);
                processPage->eventHeader.kindOfSportForApp = dataElem.attribute(tagKindOfSportForApp, "").toInt(&okForConvertStrToInt,0);
                processPage->eventHeader.kindOfSportForServer = dataElem.attribute(tagKindOfSportForServer, "").toInt(&okForConvertStrToInt,0);
                dataElem = dataNode.firstChildElement(tagCity);
                processPage->eventHeader.city = dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt,0);
                dataElem = dataNode.firstChildElement(tagLocation);
                processPage->eventHeader.location = dataElem.attribute(tagValue, "");
                dataElem = dataNode.firstChildElement(tagDateTimeOfEvent);
                processPage->eventHeader.stringDateTimeStartOfEvent = dataElem.attribute(tagStringTimeStartOfEvent, "");
                processPage->eventHeader.dateStartOfEvent = QDate().fromString(dataElem.attribute(tagDateStartOfEvent, ""), formatOfDate);
                processPage->eventHeader.timeStartOfEvent = QTime().fromString(dataElem.attribute(tagTimeStartOfEvent, ""), formatOfTime);

                processPage->setDateTimeEndOfEvent();
                processPage->eventHeader.ratingDateOfEvent = processPage->getShiftToStartEvent(lengthTimeForSetRatingEvent).toString(formatDateTimeFullWithSeconds);

                dataElem = dataNode.firstChildElement(tagDateTimeOfAutomation);
                processPage->eventHeader.stringDateTimeOfAutomation = dataElem.attribute(tagStringOfAutomation, "");
                processPage->eventHeader.dateOfAutomation = QDate().fromString(dataElem.attribute(tagDateOfAutomation, ""), formatOfDate);
                processPage->eventHeader.timeOfAutomation = QTime().fromString(dataElem.attribute(tagTimeOfAutomation, ""), formatOfTime);

                dataElem = dataNode.firstChildElement(tagQuantityPlayersInEvent);
                processPage->eventHeader.quantityPlayersInEvent = dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt,0);
                dataElem = dataNode.firstChildElement(tagGroupIDtoJoin);
                processPage->eventHeader.groupIDtoJoin = dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt,0);
                dataElem = dataNode.firstChildElement(tagRandomizeCaptain);
                processPage->eventHeader.randomizeCaptain = convertIntToBoolean(dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt, 0));
                dataElem = dataNode.firstChildElement(tagRepeatWeekByWeek);
                processPage->eventHeader.repeatWeekByWeek = dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt,0);
                dataElem = dataNode.firstChildElement(tagDaysRepeat);
                processPage->eventHeader.daysRepeat = convertIntToBoolean(dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt, 0));
                processPage->eventHeader.valueDaysRepeat = dataElem.attribute(tagDaysRepeat, "").toInt(&okForConvertStrToInt, 0);
                dataElem = dataNode.firstChildElement(tagHoursRepeat);
                processPage->eventHeader.hoursRepeat = dataElem.attribute(tagValue, "").toInt(&okForConvertStrToInt,0);
                processPage->eventHeader.valueHoursRepeat = dataElem.attribute(tagHoursRepeat, "").toInt(&okForConvertStrToInt, 0);

                dataNode = dataNode.nextSiblingElement(tagTable);
                processPage->rowCount = dataNode.toElement().attribute(tagRowCount, "").toInt(&okForConvertStrToInt, 0);
                if (!dataNode.isNull()) {
                    flReadFile = true;
                    processPage->listUserData.clear();
                    dataElem = dataNode.firstChildElement(tagUserData);
                    for (int ind = 0; ind < processPage->rowCount; ind++) {
                        if (!dataElem.isNull()) {
                            flReadFile = true;
                            UserData userData;
                            userData.id = dataElem.attribute(tagIdInUserData, "").toInt(&okForConvertStrToInt,0);
                            userData.name = dataElem.attribute(tagNameInUserData, "");
                            userData.email = dataElem.attribute(tagEmailInUserData, "");
                            userData.password = dataElem.attribute(tagPasswordInUserData, "");
                            userData.proxy = dataElem.attribute(tagProxyInUserData, "");
                            userData.enabled = convertStrToBoolean(dataElem.attribute(tagEnabledInUserData, ""));
                            userData.note = dataElem.attribute(tagNoteInUserData, "");
                            processPage->listUserData.push_back(userData);
                            dataElem = dataElem.nextSiblingElement(tagUserData);
                        }
                        dataNode = dataNode.nextSiblingElement(tagUserData);
                    }
                }
                if (processPage->rowCount == processPage->listUserData.size()) {
                    flReadFile = true;
                }
            }
        }
    }        
    file->close();
    return flReadFile;
}

bool XmlWorker::SetDataToDomTree(QFile *file, ProcessPage *processPage)
{
    QDomElement root, dataElem;
    QDomNode dataNode;
    if(file->open(QIODevice::ReadOnly)) {
        if(domDoc.setContent(file))
        {
            root = domDoc.documentElement();    //возвращает корневой элемент документа

            dataNode = root.firstChild();

            if (dataNode.toElement().tagName() == tagEventHeader)
            {
                dataElem = dataNode.firstChildElement(tagTitle);
                dataElem.setAttribute(tagValue, processPage->eventHeader.title);
                dataElem = dataNode.firstChildElement(tagDescription);
                dataElem.setAttribute(tagValue, processPage->eventHeader.description);
                dataElem = dataNode.firstChildElement(tagSportID);
                dataElem.setAttribute(tagKindOfSportForApp, QString("%1").arg(processPage->eventHeader.kindOfSportForApp));
                dataElem.setAttribute(tagKindOfSportForServer, QString("%1").arg(processPage->eventHeader.kindOfSportForServer));
                dataElem = dataNode.firstChildElement(tagCity);
                dataElem.setAttribute(tagValue, QString("%1").arg(processPage->eventHeader.city));
                dataElem = dataNode.firstChildElement(tagLocation);
                dataElem.setAttribute(tagValue, processPage->eventHeader.location);

                dataElem = dataNode.firstChildElement(tagDateTimeOfEvent);
                dataElem.setAttribute(tagStringTimeStartOfEvent, processPage->eventHeader.stringDateTimeStartOfEvent);
                dataElem.setAttribute(tagDateStartOfEvent, processPage->eventHeader.dateStartOfEvent.toString(formatOfDate));
                dataElem.setAttribute(tagTimeStartOfEvent, processPage->eventHeader.timeStartOfEvent.toString(formatOfTime));

                dataElem = dataNode.firstChildElement(tagDateTimeOfAutomation);
                dataElem.setAttribute(tagStringOfAutomation, processPage->eventHeader.stringDateTimeOfAutomation);
                dataElem.setAttribute(tagDateOfAutomation, processPage->eventHeader.dateOfAutomation.toString(formatOfDate));
                dataElem.setAttribute(tagTimeOfAutomation, processPage->eventHeader.timeOfAutomation.toString(formatOfTime));
                dataElem = dataNode.firstChildElement(tagQuantityPlayersInEvent);
                dataElem.setAttribute(tagValue, QString("%1").arg(processPage->eventHeader.quantityPlayersInEvent));
                dataElem = dataNode.firstChildElement(tagGroupIDtoJoin);
                dataElem.setAttribute(tagValue, QString("%1").arg(processPage->eventHeader.groupIDtoJoin));
                dataElem = dataNode.firstChildElement(tagRandomizeCaptain);
                dataElem.setAttribute(tagValue, processPage->eventHeader.randomizeCaptain);
                dataElem = dataNode.firstChildElement(tagRepeatWeekByWeek);
                dataElem.setAttribute(tagValue, processPage->eventHeader.repeatWeekByWeek);
                dataElem = dataNode.firstChildElement(tagDaysRepeat);
                dataElem.setAttribute(tagValue, processPage->eventHeader.daysRepeat);
                dataElem.setAttribute(tagDaysRepeat, QString("%1").arg(processPage->eventHeader.valueDaysRepeat));
                dataElem = dataNode.firstChildElement(tagHoursRepeat);
                dataElem.setAttribute(tagValue, processPage->eventHeader.hoursRepeat);
                dataElem.setAttribute(tagHoursRepeat, processPage->eventHeader.valueHoursRepeat);

                dataNode = dataNode.nextSiblingElement(tagTable);
                dataNode.toElement().setAttribute(tagRowCount, processPage->rowCount);
                if (!dataNode.isNull()) {
                    dataElem = dataNode.firstChildElement(tagUserData);
                    for (int row = 0; row < processPage->rowCount; row++) {
                        if (!dataElem.isNull()) {
                            UserData userData = processPage->listUserData.at(row);
                            dataElem.setAttribute(tagIdInUserData, userData.id );
                            dataElem.setAttribute(tagNameInUserData, QString("%1").arg(userData.name));
                            dataElem.setAttribute(tagEmailInUserData, QString("%1").arg(userData.email));
                            dataElem.setAttribute(tagPasswordInUserData, userData.password);
                            dataElem.setAttribute(tagProxyInUserData, userData.proxy);
                            dataElem.setAttribute(tagEnabledInUserData, userData.enabled);
                            dataElem.setAttribute(tagNoteInUserData, userData.note);
                            dataElem = dataElem.nextSiblingElement(tagUserData);
                        }
                        dataNode = dataNode.nextSiblingElement(tagUserData);
                    }
                }
            }
        }
//        if(file->open(QIODevice::WriteOnly)) {
//            QTextStream(file) << domDoc.toString();
//            file->close();
//        }
    }
    file->close();
    return WriteDomTreeToXml(file);
}

bool XmlWorker::WriteDomTreeToXml(QFile *file){
    const int Indent = 4;
    if(file->open(QIODevice::WriteOnly))
    {
        QTextStream out(file);
        domDoc.save(out, Indent);
    }
    file->close();
    return file->exists();
}

QDomElement XmlWorker::makeElement(      QDomDocument& domDoc,
                        const QString&      tagNameElement,
                        const QString&      tagValue,
                        const QString&      value
                       )
{
    QDomElement domElement = domDoc.createElement(tagNameElement);

    if (!tagValue.isEmpty()) {
        QDomAttr domAttr = domDoc.createAttribute(tagValue);
        domAttr.setValue(value);
        domElement.setAttributeNode(domAttr);
    }

    return domElement;
}

QDomElement XmlWorker::makeElement(      QDomDocument& domDoc,
                        const QString&      tagNameElement,
                        const QString& nameTagValueFirstAttr, const QString& valueFirstAttr,
                        const QString& nameTagValueSecondAttr, const QString& valueSecondAttr
                       )
{
    QDomElement domElement = domDoc.createElement(tagNameElement);

    if (!tagValue.isEmpty()) {
        QDomAttr domFirstAttr = domDoc.createAttribute(nameTagValueFirstAttr);
        domFirstAttr.setValue(valueFirstAttr);
        domElement.setAttributeNode(domFirstAttr);
        QDomAttr domSecondAttr = domDoc.createAttribute(nameTagValueSecondAttr);
        domSecondAttr.setValue(valueSecondAttr);
        domElement.setAttributeNode(domSecondAttr);
    }

    if ((!valueFirstAttr.isEmpty()) && (!valueSecondAttr.isEmpty())) {
        QDomText domText = domDoc.createTextNode(tagNameElement);
        domElement.appendChild(domText);
    }
    return domElement;
}

QDomElement XmlWorker::makeElement(      QDomDocument& domDoc,
                        const QString&      tagNameElement,
                        const QString& nameTagValueFirstAttr, const QString& valueFirstAttr,
                        const QString& nameTagValueSecondAttr, const QString& valueSecondAttr,
                        const QString& nameTagValueThirdAttr, const QString& valueThirdAttr
                       )
{
    QDomElement domElement = domDoc.createElement(tagNameElement);

    if (!tagValue.isEmpty()) {
        QDomAttr domFirstAttr = domDoc.createAttribute(nameTagValueFirstAttr);
        domFirstAttr.setValue(valueFirstAttr);
        domElement.setAttributeNode(domFirstAttr);
        QDomAttr domSecondAttr = domDoc.createAttribute(nameTagValueSecondAttr);
        domSecondAttr.setValue(valueSecondAttr);
        domElement.setAttributeNode(domSecondAttr);
        QDomAttr domThirdAttr = domDoc.createAttribute(nameTagValueThirdAttr);
        domThirdAttr.setValue(valueThirdAttr);
        domElement.setAttributeNode(domThirdAttr);
    }

    if ((!valueFirstAttr.isEmpty()) && (!valueSecondAttr.isEmpty())) {
        QDomText domText = domDoc.createTextNode(tagNameElement);
        domElement.appendChild(domText);
    }
    return domElement;
}

void XmlWorker::makeRowAttribute(QDomDocument& domDoc, QDomElement *domElement, QString nameAttr, QString valueAttr) {
    QDomAttr domAttr = domDoc.createAttribute(nameAttr);
    domAttr.setValue(valueAttr);
    domElement->setAttributeNode(domAttr);
}

QDomElement XmlWorker::makeRowElement(QDomDocument& domDoc, const QString tagNameRow) {
    QDomElement domElement;
    if (!tagNameRow.isEmpty()) {
        domElement = domDoc.createElement(tagNameRow);
        makeRowAttribute(domDoc, &domElement, tagIdInUserData, "");
        makeRowAttribute(domDoc, &domElement, tagNameInUserData, "");
        makeRowAttribute(domDoc, &domElement, tagEmailInUserData, "");
        makeRowAttribute(domDoc, &domElement, tagPasswordInUserData, "");
        makeRowAttribute(domDoc, &domElement, tagProxyInUserData, "");
        makeRowAttribute(domDoc, &domElement, tagEnabledInUserData, "");
        makeRowAttribute(domDoc, &domElement, tagNoteInUserData, "");
    }
    return domElement;
}

QDomElement XmlWorker::fieldHeader(QDomDocument& domDoc, QString tagNameElement, QString nameTagValue, QString value)
{
    QDomElement domElement = makeElement(domDoc, tagNameElement, nameTagValue, value);
    return domElement;
}

QDomElement XmlWorker::fieldHeader(QDomDocument& domDoc, QString tagNameElement,
                                   QString nameTagValueFirstAttr, QString valueFirstAttr,
                                   QString nameTagValueSecondAttr, QString valueSecondAttr)
{
    QDomElement domElement = makeElement(domDoc, tagNameElement,
                                         nameTagValueFirstAttr, valueFirstAttr,
                                         nameTagValueSecondAttr, valueSecondAttr);
    return domElement;
}

QDomElement XmlWorker::fieldHeader(QDomDocument& domDoc, QString tagNameElement,
                                   QString nameTagValueFirstAttr, QString valueFirstAttr,
                                   QString nameTagValueSecondAttr, QString valueSecondAttr,
                                   QString nameTagValueThirdAttr, QString valueThirdAttr)
{
    QDomElement domElement = makeElement(domDoc, tagNameElement,
                                         nameTagValueFirstAttr, valueFirstAttr,
                                         nameTagValueSecondAttr, valueSecondAttr,
                                         nameTagValueThirdAttr, valueThirdAttr);
    return domElement;
}

bool XmlWorker::convertStrToBoolean(QString valString) {
    if (valString.isEmpty()|| valString.isNull() || valString == "") {
        return convertIntToBoolean(0);
    }
    else {
        int valInt = valString.toInt(&okForConvertStrToInt, 0);
        if (valInt == 0) {
            return convertIntToBoolean(0);
        }
        else {
            return convertIntToBoolean(valInt);
        }
    }
}

bool XmlWorker::convertIntToBoolean(int valBoolean){
    if(valBoolean == 0) {
        return false;
    }
    else if (valBoolean == 1) {
        return true;
    }
    else {
        return false;
    }
}
