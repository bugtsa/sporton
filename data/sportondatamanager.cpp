#include "sportondatamanager.h"

//SportonDataManager provide REST API at SportOn Server
SportonDataManager::SportonDataManager(QObject *parent) : QObject(parent)
{    // Call the webservice
    m_NetworkAccessManager = new QNetworkAccessManager(this);

    connect(m_NetworkAccessManager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), this, SLOT(onSslError(QNetworkReply*, QList<QSslError>)));
}

//test proxy connection
QNetworkProxy SportonDataManager::getProxy(const UserData userData) {
    QString name = stringsUtils.getNameHost(userData.proxy);
    quint16 port = stringsUtils.getPortHost(userData.proxy).toShort();
    QString user = userData.name;
    QString pass = userData.password;
    QNetworkProxy proxy;
    proxy.setHostName(name);
    proxy.setPort(port);
    proxy.setUser(user);
    proxy.setPassword(pass);
    return proxy;
}

//register user with user data(email, password, name
QString SportonDataManager::registerUserData(UserData userData) {
    QUrl serviceUrl = QUrl(urlRegisterUser);
    QByteArray postData;
    QUrl params;
    QUrlQuery query;

    query.addQueryItem(testFieldForRequest, "test");
    query.addQueryItem(userEmailFieldForRequest, userData.email);
    query.addQueryItem(userPasswordFieldForRequest, userData.password);
    query.addQueryItem(userLoginNameFieldForRequst, userData.name);

    params.setQuery(query);

    postData = params.toEncoded(QUrl::RemoveFragment);

    QNetworkRequest networkRequest(serviceUrl);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QNetworkReply *replyPost = m_NetworkAccessManager->post(networkRequest, postData);
    connect(replyPost, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    return replyFinishedAfterRegisterUser(replyPost);
}

//update user with id and name
bool SportonDataManager::updateUser(UserData userData) {
    QUrl serviceUrl = QUrl(urlUpdateUser);
    QByteArray postData;
    QUrl params;
    QUrlQuery query;

    query.addQueryItem(testFieldForRequest, "test");
    query.addQueryItem(userIdFieldForRequest, QString("%1").arg(userData.id));
    query.addQueryItem(userNameFieldForRequest, userData.name);
//    query.addQueryItem(userDobFieldForRequest, "1999");
    query.addQueryItem(userAboutFieldForRequest, "");
    query.addQueryItem(userLocationFieldForRequest, "");

    params.setQuery(query);
    postData = params.toEncoded(QUrl::RemoveFragment);

    QNetworkRequest networkRequest(serviceUrl);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QNetworkReply *reply = m_NetworkAccessManager->post(networkRequest, postData);
    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    return replyAfterUpdateUser(reply);
}

//create event
QString SportonDataManager::createEvent(ProcessPage *processPage, QString eventTypeForRequestGetEvents, QString userID) {
    QUrl serviceUrl = QUrl(urlCreateEvent);
    QByteArray postData;
    QUrl params;
    QUrlQuery query;
    EventHeader *pEventHeader = &processPage->eventHeader;

    query.addQueryItem(testFieldForRequest, "test");
    query.addQueryItem(userIdFieldForRequest, userID);
    query.addQueryItem(eventCategoryIdFieldForRequest, QString("%1").arg(pEventHeader->kindOfSportForServer));
    query.addQueryItem(eventTitleFieldForRequest, pEventHeader->title);
    query.addQueryItem(eventDescriptionFieldForRequest, pEventHeader->description);
    query.addQueryItem(eventLocationFieldForRequest, pEventHeader->location);
    query.addQueryItem(eventStartDateFieldForRequest, pEventHeader->stringDateTimeStartOfEvent);
    query.addQueryItem(eventEndDateField, pEventHeader->stringDateTimeEndOfEvent);
    query.addQueryItem(eventRatingDateFieldForRequest, pEventHeader->ratingDateOfEvent);
    query.addQueryItem(eventPrivacyFieldForRequest, "Public");
    query.addQueryItem(eventGenderFieldForRequest, "3");
    query.addQueryItem(eventMinimumSkillsFieldForRequest, "");
    query.addQueryItem(eventPlayersFieldForRequest, QString("%1").arg(pEventHeader->quantityPlayersInEvent));
    query.addQueryItem(eventPriceFieldForRequest, "");
    query.addQueryItem(requestTimeFieldForRequest, pEventHeader->stringDateTimeOfAutomation);
    query.addQueryItem(eventSendInviteFieldForRequest, "");

    params.setQuery(query);

    postData = params.toEncoded(QUrl::RemoveFragment);

    QNetworkRequest networkRequest(serviceUrl);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    m_NetworkAccessManager->setProxy(getProxy(processPage->listUserData.at(0)));
    QEventLoop eventLoop;
    QNetworkReply *replyPost = m_NetworkAccessManager->post(networkRequest, postData);
    connect (replyPost, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    QString addedUpcomingEventID  = getUpcomingEventsByUserId(eventTypeForRequestGetEvents, userID, pEventHeader);
    if ((replyFinishedAfterCreateEvent(replyPost)) && (!addedUpcomingEventID.isEmpty())) {
        return addedUpcomingEventID;
    } else {
        return "";
    }
}

//update event
bool SportonDataManager::updateEvent(ProcessPage *processPage, QString eventID, QString userID) {
    QUrl serviceUrl = QUrl(urlUpdateEvent);
    QByteArray postData;
    QUrl params;
    QUrlQuery query;
    EventHeader *pEventHeader = &processPage->eventHeader;

    query.addQueryItem(testFieldForRequest, "test");
    query.addQueryItem(eventIdForRequestAndResponse, eventID);
    query.addQueryItem(userIdFieldForRequest, userID);
    query.addQueryItem(eventCategoryIdFieldForRequest, QString("%1").arg(pEventHeader->kindOfSportForServer));
    query.addQueryItem(eventTitleFieldForRequest, pEventHeader->title);
    query.addQueryItem(eventDescriptionFieldForRequest, pEventHeader->description);
    query.addQueryItem(eventLocationFieldForRequest, pEventHeader->location + ", " + pEventHeader->stCity);
    query.addQueryItem(eventStartDateFieldForRequest, pEventHeader->stringDateTimeStartOfEvent);
    query.addQueryItem(eventEndDateField, pEventHeader->stringDateTimeEndOfEvent);
    query.addQueryItem(eventRatingDateFieldForRequest, pEventHeader->ratingDateOfEvent);
    query.addQueryItem(eventPrivacyFieldForRequest, "Public");
    query.addQueryItem(eventGenderFieldForRequest, "3");
    query.addQueryItem(eventMinimumSkillsFieldForRequest, "");
    query.addQueryItem(eventPlayersFieldForRequest, QString("%1").arg(pEventHeader->quantityPlayersInEvent));
    query.addQueryItem(eventPriceFieldForRequest, "");
    query.addQueryItem(requestTimeFieldForRequest, pEventHeader->stringDateTimeOfAutomation);
    query.addQueryItem(eventSendInviteFieldForRequest, "");

    params.setQuery(query);

    postData = params.toEncoded(QUrl::RemoveFragment);

    QNetworkRequest networkRequest(serviceUrl);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QNetworkReply *replyPost = m_NetworkAccessManager->post(networkRequest, postData);
    connect (replyPost, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    return replyFinishedAfterUpdateEvent(replyPost);
}

//get upcomings events by user id
//not used
QString SportonDataManager::getUpcomingEventsByUserId(QString eventType, QString userID, EventHeader *pEventHeader) {
    QUrl serviceUrlForGetEvents = QUrl(urlGetEvents);
    QByteArray postData;
    QUrl params;
    QUrlQuery query;

    query.addQueryItem(testFieldForRequest, "test");
    query.addQueryItem(eventTypeFieldForRequest, eventType);
    query.addQueryItem(userIdFieldForRequest, userID);
    query.addQueryItem(userLatitudeFieldForRequest, mapUtils.getLatitudeCoordinateByStringNameCity(pEventHeader->stCity));
    query.addQueryItem(userLongitudeFieldForRequest, mapUtils.getLongitudeCoordinateByStringNameCity(pEventHeader->stCity));
    query.addQueryItem(requestTimeFieldForRequest, pEventHeader->stringDateTimeOfAutomation);
    params.setQuery(query);
    postData = params.toEncoded(QUrl::RemoveFragment);
    QNetworkRequest networkRequest(serviceUrlForGetEvents);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QNetworkReply *replyPostGetEvents = m_NetworkAccessManager->post(networkRequest, postData);
    connect(replyPostGetEvents, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    QMap<QString, QVariant> mapUpcomingEventsID = replyFinishedAfterGetUpcomingEvents(replyPostGetEvents);
    if (mapUpcomingEventsID.isEmpty()) {
        return "";
    } else {
        QList<QString> listKeysAtUserID = mapUpcomingEventsID.keys(QString("%1").arg(userID));
        if (!listKeysAtUserID.isEmpty()) {
            return listKeysAtUserID.last();
        } else {
            return "";
        }
    }
}

//get status connection
bool SportonDataManager::getConnectionStatus() {
    QUrl serviceUrlGetStatus = QUrl(urlGetConnectionStatus);
    QNetworkRequest networkRequest(serviceUrlGetStatus);

    QTimer timer;
    timer.setSingleShot(true);

    QEventLoop loop;

    QNetworkReply* reply = m_NetworkAccessManager->get(networkRequest);

    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    timer.start(10000);   // 30 secs. timeout
    loop.exec();

    if(timer.isActive()) {
        timer.stop();
        if(reply->error() > 0) {
            return false;
        }
        else {
          int v = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

          if (v >= 200 && v < 300) {  // Success
              return true;
          }
        }
    } else {
       // timeout
       disconnect(reply, SIGNAL(finished()), &loop, SLOT(quit()));

       reply->abort();
    }
    QByteArray answer = reply->readAll();
    if (answer.isEmpty()) {
        return false;
    } else {
        return true;
    }
}

//add event participation
bool SportonDataManager::addEventParticipation(QString userID, QString eventID, QString requestType, QString requestTime) {
    QUrl serviceUrlAddEventParticipation = QUrl(urlAddEventParticipation);
    QByteArray postDataAddEventParticipation;
    QUrl params;
    QUrlQuery query;
    query.addQueryItem(testFieldForRequest, "test");
    query.addQueryItem(userIdFieldForRequest, userID);
    query.addQueryItem(eventIdForRequestAndResponse, eventID);
    query.addQueryItem(requestTypeFieldForRequest, requestType);
    query.addQueryItem(requestTimeFieldForRequest, requestTime);
    params.setQuery(query);
    postDataAddEventParticipation = params.toEncoded(QUrl::RemoveFragment);
    QNetworkRequest networkRequest(serviceUrlAddEventParticipation);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QNetworkReply *replyAddEventParticipation = m_NetworkAccessManager->post(networkRequest, postDataAddEventParticipation);
    connect(replyAddEventParticipation, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    return replyFinishedAfterAddEventParticipation(replyAddEventParticipation);
}

int SportonDataManager::getUserIdByEmail(QString requestEmail) {
    QUrl serviceUrlGetUserIdByEmail = QUrl(urlGetUserIdByEmail + requestEmail);
    QNetworkRequest networkRequest(serviceUrlGetUserIdByEmail);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QNetworkReply *replyGetUserIdByEmail = m_NetworkAccessManager->get(networkRequest);
    connect(replyGetUserIdByEmail, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    return replyFinishedAfterGetUserIdByEmail(replyGetUserIdByEmail);
}

QString SportonDataManager::getEmailByUserId(int requestUserId) {
    QUrl serviceUrlGetEmailByUserId = QUrl(urlGetEmailByUserId + QString("%1").arg(requestUserId));
    QNetworkRequest networkRequest(serviceUrlGetEmailByUserId);
    networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, stringContentTypeHeader);
    QEventLoop eventLoop;
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QNetworkReply *replyGetEmailByUserId = m_NetworkAccessManager->get(networkRequest);
    connect(replyGetEmailByUserId, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    return replyFinishedAfterGetEmailByUserId(replyGetEmailByUserId);
}

QString SportonDataManager::replyFinishedAfterGetEmailByUserId(QNetworkReply *reply) {
    QString data = reply->readAll();
    if (existsReplyError(reply)) {
        return 0;
    }
    QObject *objectOriginal = reply->request().originatingObject();
    if (!objectOriginal) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();
#else
        QVariant result = parser.parse(data.toLatin1(), &ok);
#endif
        if(!ok) {
            emit errorOccured("at GetEmailByUserId", QString("Cannot convert to QJson object: %1").arg(data));
            return "";
        } else {
            if (result[replyResponseCode].toBool()) {
                return result[replyEmail].toString();
            } else {
                return "";
            }
        }
    } else {
        return "";
    }
}

//function reply get user id by email
int SportonDataManager::replyFinishedAfterGetUserIdByEmail(QNetworkReply *reply) {
    QString data = reply->readAll();
    if (existsReplyError(reply)) {
        return 0;
    }
    QObject *originalObject = reply->request().originatingObject();
    if(!originalObject) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();
#else
        QVariant result = parser.parse(data.toLatin1(), &ok);
#endif
        if(!ok) {
            emit errorOccured("at Get UserIdByEmail", QString("Cannot convert to QJson object: %1").arg(data));
            return 0;
        } else {
            if (result[replyResponseCode].toBool()) {
                return result[replyShortUserId].toInt();
            } else {
                return 0;
            }
        }
    } else {
        return 0;
    }
}

//function reply register user
QString SportonDataManager::replyFinishedAfterRegisterUser(QNetworkReply *reply) {
    QString data = reply->readAll();

    if (existsReplyError(reply)) {
        return "-1";
    }
    QObject *originalObject = reply->request().originatingObject();
    if(!originalObject) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();
#else
        QVariant result = parser.parse(data.toLatin1(), &ok);
#endif
        if(!ok) {
            emit errorOccured("at Register User", QString("Cannot convert to QJson object: %1").arg(data));
            return "-1";
        } else {
            if (result[replyResponseCode].toBool()) {
                QVariantMap userMap = result[replyUser].toMap();
                return userMap[replyUserId].toString();
            } else {
                return "0 - " + result[replyMessage].toString();
            }
        }
    } else {
        return "-1";
    }
}

//function reply update user
bool SportonDataManager::replyAfterUpdateUser(QNetworkReply *reply) {
    QString data = reply->readAll();

    if (existsReplyError(reply)) {
        return false;
    }
    QObject *originalObject = reply->request().originatingObject();
    if (!originalObject) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();
#else
        QVariant result = parser.parse(data.toLatin1(), &ok);
#endif
        if (!ok) {
            emit errorOccured("at Update User", QString("Cannot convert to QJson object: %1").arg(data));
            return false;
        } else {
            if (result[replyResponseCode].toBool()) {
                return true;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}

//function reply update event
bool SportonDataManager::replyFinishedAfterUpdateEvent(QNetworkReply *reply) {
    QString data = reply->readAll();

    if (existsReplyError(reply)) {
        return false;
    }
    QObject *originalObject = reply->request().originatingObject();
    if (!originalObject) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();
#else
        QVariant result = parser.parse(data.toLatin1(), &ok);
#endif
        if (!ok) {
            emit errorOccured("at UpdateEvent", QString("Cannot convert to QJson object: %1").arg(data));
            return false;
        } else {
            return result[replyResponseCode].toBool();
        }
    } else {
        return false;
    }
}

//function reply create event
bool SportonDataManager::replyFinishedAfterCreateEvent(QNetworkReply *replyPost) {
    QString data = replyPost->readAll();

    // check for error
    if ( replyPost->error() )
    {
        int step = 50;
        int length = data.size();
        QString dataTmp;
        if (length > 0) {
            while (length < step){
                step /=2;
            }
            for(int ind = 0; ind < length; ind) {
                dataTmp = data.mid(ind, step);
                ind += step;
                if (ind > length) {
                    ind = length;
                }
                dataTmp.clear();
            }
        }
    }

    if( existsReplyError(replyPost) ) {
        return false;
    }
    QObject * origObject = replyPost->request().originatingObject();

    //it's ok, just simple usage of QNetworkAccessManager, without DataManagerHelper
    if( ! origObject ) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        // Qt5 code
        QVariantMap result = parser.parse (data.toUtf8(), &ok).toMap();
#else
        // Qt4 code
        QVariant result = parser.parse (data.toLatin1(), &ok);
#endif
        if(!ok)
        {
            errorOccured("at CreateEvent", QString("Cannot convert to QJson object: %1").arg(data));
            return false;
        }
        return result[replyResponseCode].toBool();
    } else {
        return false;
    }
}

//function reply get upcoming events
QMap<QString, QVariant> SportonDataManager::replyFinishedAfterGetUpcomingEvents(QNetworkReply *replyPostGetEvents) {
    QMap<QString, QVariant> mapEventsID;
    QString replyData = replyPostGetEvents->readAll();

    if (existsReplyError(replyPostGetEvents)) {
        return mapEventsID;
    }
    QObject *replyObject = replyPostGetEvents->request().originatingObject();

    if (!replyObject) {
        QJson::Parser parser;
        bool ok;

#if QT_VERSION >= 0x050000
        QVariantMap result = parser.parse(replyData.toUtf8(), &ok).toMap();
#else
        // Qt4 code
        QVariant result = parser.parse (data.toLatin1(), &ok);
#endif
        if (!ok) {
            return mapEventsID;
        } else {
            QVariant events = result.take(responseGetEventsListEvents);
            if(events.canConvert(QVariant::List)) {
                QList<QVariant> listEvents = events.toList();
                for(int ind = 0; ind < listEvents.count(); ind++) {
                    QMap<QString, QVariant> oneEvent = listEvents.at(ind).toMap();
                    mapEventsID.insert(oneEvent.value(eventIdForRequestAndResponse).toString(), oneEvent.value(responseGetEventsValueUserID));
                }
            }
        }
    }
    return mapEventsID;
}

//function reply add event participation
bool SportonDataManager::replyFinishedAfterAddEventParticipation(QNetworkReply * replyAfterAddEventParticipation) {
    QApplication::restoreOverrideCursor();

    QString data = replyAfterAddEventParticipation->readAll();

    if( existsReplyError(replyAfterAddEventParticipation) ) {
        return false;
    }
    QObject * origObject = replyAfterAddEventParticipation->request().originatingObject();

    //it's ok, just simple usage of QNetworkAccessManager, without DataManagerHelper
    if( ! origObject ) {
        QJson::Parser parser;
        bool ok;
#if QT_VERSION >= 0x050000
        // Qt5 code
        QVariantMap result = parser.parse (data.toUtf8(), &ok).toMap();
#else
        // Qt4 code
        QVariant result = parser.parse (data.toLatin1(), &ok);
#endif
        if(!ok)
        {
            emit errorOccured("at Add Event Participation", QString("Cannot convert to QJson object: %1").arg(data));
            return false;
        }
        return result[replyResponseCode].toBool();
    } else {
        return false;
    }
}

//exit after error in QNetworkReply
bool SportonDataManager::existsReplyError(const QNetworkReply * reply) const
{
    QApplication::restoreOverrideCursor();
    // Client Error 4xx
    // Server Error 5xx

    if( 399 < reply->error() )
    {
        errorOccured("at Check Exists Errors",  reply->errorString() );
        return true;
    }

    return false;
}

//show error scroll message box
void SportonDataManager::errorOccured(const QString title, const QString & error) const
{
    ScrollMessageBox scrollMessageBox(QMessageBox::Critical, "Error " + title, error, QDialogButtonBox::Ok);
    scrollMessageBox.setMinimumHeight(600);
    scrollMessageBox.setMinimumWidth(800);
    int ret = scrollMessageBox.exec();
    switch (ret) {
    case QMessageBox::Save:
         break;
      case QMessageBox::Ok:
         break;
      default:
         break;
    }
    return;
}

void SportonDataManager::onSslError(QNetworkReply* r, QList<QSslError> l) {
    r->ignoreSslErrors();
}
