#include "logmessage.h"

LogMessage::LogMessage()
{
    QDir logDefaultDir(logDefaultDirectory);
    if (!logDefaultDir.exists()) {
        QDir().mkdir(logDefaultDirectory);
    }
    fileName = logDefaultDirectory + "/" + QDateTime::currentDateTime().toString(formatDateTimeForLogMessageFile) + textExtentionLog;

    currentLogFile.setFileName(fileName);
}

void LogMessage::writeLine(QString stringLine) {
    if (!currentLogFile.open(QFile::Append | QFile::Text)) {
        return;
    } else {
        if (currentLogFile.exists()) {
            QTextStream out(&currentLogFile);
            out << QDateTime::currentDateTime().toString(formatDateTimeFullWithSeconds) << " | " << stringLine << "\n";
            currentLogFile.close();
        }
    }
}
