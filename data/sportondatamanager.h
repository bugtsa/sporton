#ifndef SPORTONDATAMANAGER_H
#define SPORTONDATAMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QVariantMap>
#include <QUrlQuery>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QApplication>
#include <QMessageBox>
#include <QSslError>
#include <QEventLoop>
#include <QTimer>

#include "utils/constants.h"
#include "utils/maputils.h"
#include "utils/stringsutils.h"
#include "utils/scrollmessagebox.h"
#include "./../qjson/include/serializer.h"
#include "./../qjson/include/parser.h"
#include "google-place/datamanagerhelper.h"
#include "data/processpage.h"
#include "data/logmessage.h"

class SportonDataManager : public QObject
{
    Q_OBJECT
public:
    explicit SportonDataManager(QObject *parent = 0);

    QString createEvent(ProcessPage *processPage, QString eventTypeForRequestGetEvents, QString userID);

    bool updateEvent(ProcessPage *processPage, QString eventID, QString userID);

    bool addEventParticipation(QString userID, QString eventID, QString requestType, QString requestTime);

    bool getConnectionStatus();

    int getUserIdByEmail(QString requestEmail);

    QString getEmailByUserId(int requestUserId);

    QString registerUserData(UserData userData);

    bool updateUser(UserData userData);

signals:


private slots:
    void onSslError(QNetworkReply* r, QList<QSslError> l);

private:
    QNetworkAccessManager *m_NetworkAccessManager;

    MapUtils mapUtils;

    StringsUtils stringsUtils;

private:
    enum RequestType { Get, Post, Put, Delete };
    void sendRequest(
            QNetworkRequest request,
            RequestType type = Get, const QByteArray & data = QByteArray()
    );
    bool existsReplyError(const QNetworkReply * reply) const;

    int replyFinishedAfterGetUserIdByEmail(QNetworkReply *reply);

    QString replyFinishedAfterGetEmailByUserId(QNetworkReply *reply);

    QString replyFinishedAfterRegisterUser(QNetworkReply *replyPost);

    bool replyAfterUpdateUser(QNetworkReply *reply);

    bool replyFinishedAfterCreateEvent(QNetworkReply *replyPost);

    bool replyFinishedAfterUpdateEvent(QNetworkReply *reply);

    QString getUpcomingEventsByUserId(QString eventType, QString userID, EventHeader *pEventHeader);

    QMap<QString, QVariant> replyFinishedAfterGetUpcomingEvents(QNetworkReply *replyPostGetEvents);

    bool replyFinishedAfterAddEventParticipation(QNetworkReply * replyAfterAddEventParticipation);

    void errorOccured(const QString title, const QString & error) const;

    QNetworkProxy getProxy(const UserData userData);

};

#endif // SPORTONDATAMANAGER_H
