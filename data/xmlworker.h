#ifndef XMLWORKER_H
#define XMLWORKER_H
#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QFileDialog>
#include <QtXml>

#include "processpage.h"



class XmlWorker
{
public:
    XmlWorker();

    //объект DOM дерева
    QDomDocument domDoc;
    //для преобразования строки в число
    bool okForConvertStrToInt;

    bool convertIntToBoolean(int valBoolean);

    bool convertStrToBoolean(QString valString);

    bool ReadXmlWithDomTree(QFile *file, ProcessPage *processPage);

    bool SetDataToDomTree(QFile *file, ProcessPage *processPage);

    QDomElement makeElement(      QDomDocument& domDoc,
                            const QString&      tagNameElement,
                            const QString&      tagValue,
                            const QString&      value
                           );

    QDomElement makeElement(      QDomDocument& domDoc,
                            const QString&      tagNameElement,
                            const QString& nameTagValueFirstAttr, const QString& valueFirstAttr,
                            const QString& nameTagValueSecondAttr, const QString& valueSecondAttr
                           );

    QDomElement makeElement(      QDomDocument& domDoc,
                            const QString&      tagNameElement,
                            const QString& nameTagValueFirstAttr, const QString& valueFirstAttr,
                            const QString& nameTagValueSecondAttr, const QString& valueSecondAttr,
                            const QString& nameTagValueThirdAttr, const QString& valueThirdAttr
                           );

    void makeRowAttribute(QDomDocument& domDoc, QDomElement *domElement, QString nameAttr, QString valueAttr);

    QDomElement makeRowElement(QDomDocument& domDoc, const QString tagNameRow);

    QDomElement fieldHeader(QDomDocument& domDoc, QString tagNameElement,
                            QString tagValue, QString value);

    QDomElement fieldHeader(QDomDocument& domDoc, QString tagNameElement,
                            QString nameTagValueFirstAttr, QString valueFirstAttr,
                            QString nameTagValueSecondAttr, QString valueSecondAttr);

    QDomElement fieldHeader(QDomDocument& domDoc, QString tagNameElement,
                                       QString nameTagValueFirstAttr, QString valueFirstAttr,
                                       QString nameTagValueSecondAttr, QString valueSecondAttr,
                                       QString nameTagValueThirdAttr, QString valueThirdAttr);

    void CreateEmptyXmlFile(QFile *file, int rowCount);

    bool WriteDomTreeToXml(QFile *file);
};

#endif // XMLWORKER_H
