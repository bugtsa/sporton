#include "processpage.h"

UserData::UserData() {
    this->email = "";
    this->enabled = false;
    this->id = 0;
    this->name = "";
    this->note = "";
    this->password = "";
    this->proxy = "";
    this->status = "";
}

ProcessPage::ProcessPage()
{

}

bool ProcessPage::isEmptyProcessPage() {
    return false;
}

bool ProcessPage::isStatusUploadToServer() {
    bool isStatusUpload = false;
    if (this->eventHeader.status == STRING_OK) {
        isStatusUpload = true;
    }
    for(int indRow = 0; indRow < this->listEnableUsers.size(); indRow++) {
//        if (!isEmptyUserData(this->listUserData.at(indRow))) {
        if (getStatusStringUserData(indRow) == STRING_OK) {
            isStatusUpload = true;
            continue;
        } else {
            isStatusUpload = false;
            break;
        }
//        }
    }
    return isStatusUpload;
}

QString ProcessPage::getStatusStringUserData(int indListUserData) {
    UserData userData = this->listUserData.at(indListUserData);
    return userData.status;
}

void ProcessPage::reset(int rowCount) {

    EventHeader pEventHeader = this->eventHeader;

    pEventHeader.filePathToOpenFile = "";
    pEventHeader.title = "Title";
    pEventHeader.description = "Description";
    pEventHeader.kindOfSportForApp = 0;
    pEventHeader.kindOfSportForServer = 16;
    pEventHeader.city = 0;
    pEventHeader.stCity = "";
    pEventHeader.location = "Place";
    pEventHeader.quantityPlayersInEvent = 0;
    pEventHeader.groupIDtoJoin = 0;
    pEventHeader.dateStartOfEvent = QDate::currentDate();
    pEventHeader.timeStartOfEvent = QTime::fromString(defaultTime, formatOfTime);
    pEventHeader.stringDateTimeStartOfEvent = pEventHeader.dateStartOfEvent.toString(formatOfDate) + " " + pEventHeader.timeStartOfEvent.toString(formatOfTime);

    pEventHeader.dateEndOfEvent = getDateEndOfEvent();
    pEventHeader.timeEndOfEvent = getTimeEndOfEvent();
    pEventHeader.stringDateTimeEndOfEvent = pEventHeader.dateEndOfEvent.toString(formatOfDate) + " " + pEventHeader.timeEndOfEvent.toString(formatOfTime);

    pEventHeader.ratingDateOfEvent = pEventHeader.stringDateTimeStartOfEvent;
    pEventHeader.dateOfAutomation = QDate::currentDate();
    pEventHeader.timeOfAutomation = QTime::fromString(defaultTime, formatOfTime);
    pEventHeader.stringDateTimeOfAutomation = pEventHeader.dateOfAutomation.toString(formatOfDate) + " " + pEventHeader.timeOfAutomation.toString(formatOfTime);
    pEventHeader.randomizeCaptain = 1;
    pEventHeader.repeatWeekByWeek = 0;
    pEventHeader.daysRepeat = 0;
    pEventHeader.valueDaysRepeat = 0;
    pEventHeader.hoursRepeat = 0;
    pEventHeader.valueHoursRepeat = 0;
    pEventHeader.indexRowCaptainEvent = 0;
    pEventHeader.indexEventInLogWindow = -1;
    this->eventHeader = pEventHeader;

    this->rowCount = rowCount;
    this->status = "";
    this->generatedString = generationUtils.getRandomString();
    this->listUserData.clear();
    UserData userData;
    userData.id = -1;
    userData.name = "";
    userData.email = "";
    userData.password = "";
    userData.proxy = "";
    userData.enabled = false;
    userData.note = "";
    userData.status = "";
    for (int iRow = 0; iRow < rowCount; iRow++){
        this->listUserData.push_back(userData);
    }
}

void ProcessPage::swapProcessPage(ProcessPage _processPage) {
    EventHeader thisEventHeader = this->eventHeader;
    EventHeader copyEventHeader = _processPage.eventHeader;

    thisEventHeader.title = copyEventHeader.title;
    thisEventHeader.description = copyEventHeader.description;
    thisEventHeader.kindOfSportForApp = copyEventHeader.kindOfSportForApp;
    thisEventHeader.kindOfSportForServer = copyEventHeader.kindOfSportForServer;
    thisEventHeader.city = copyEventHeader.city;
    thisEventHeader.stCity = copyEventHeader.stCity;
    thisEventHeader.location = copyEventHeader.location;
    thisEventHeader.quantityPlayersInEvent = copyEventHeader.quantityPlayersInEvent;
    thisEventHeader.groupIDtoJoin = copyEventHeader.groupIDtoJoin;
    thisEventHeader.dateStartOfEvent = copyEventHeader.dateStartOfEvent;
    thisEventHeader.timeStartOfEvent = copyEventHeader.timeStartOfEvent;
    thisEventHeader.stringDateTimeStartOfEvent = copyEventHeader.stringDateTimeStartOfEvent;
    thisEventHeader.timeEndOfEvent = copyEventHeader.timeEndOfEvent;
    thisEventHeader.stringDateTimeEndOfEvent = copyEventHeader.stringDateTimeEndOfEvent;
    thisEventHeader.ratingDateOfEvent = copyEventHeader.stringDateTimeStartOfEvent;
    thisEventHeader.dateOfAutomation = copyEventHeader.dateOfAutomation;
    thisEventHeader.timeOfAutomation = copyEventHeader.timeOfAutomation;
    thisEventHeader.stringDateTimeOfAutomation = copyEventHeader.stringDateTimeOfAutomation;
    thisEventHeader.randomizeCaptain = copyEventHeader.randomizeCaptain;
    thisEventHeader.repeatWeekByWeek = copyEventHeader.repeatWeekByWeek;
    thisEventHeader.daysRepeat = copyEventHeader.daysRepeat;
    thisEventHeader.valueDaysRepeat = copyEventHeader.valueDaysRepeat;
    thisEventHeader.hoursRepeat = copyEventHeader.hoursRepeat;
    thisEventHeader.valueHoursRepeat = copyEventHeader.valueHoursRepeat;
    thisEventHeader.indexRowCaptainEvent = copyEventHeader.indexRowCaptainEvent;
    thisEventHeader.indexEventInLogWindow = copyEventHeader.indexEventInLogWindow;
    this->eventHeader = thisEventHeader;

    this->rowCount = _processPage.rowCount;
    this->status = _processPage.status;
    if (this->status == STRING_OK) {
        this->status = "";
    }
    this->generatedString = _processPage.generatedString;
    this->listUserData.clear();
    for (int iRow = 0; iRow < rowCount; iRow++){
        UserData userData = _processPage.listUserData.at(iRow);
        this->listUserData.push_back(userData);
    }
}

void ProcessPage::setDateTimeStartOfEvent(QDateTime tmpDateTime) {
    this->eventHeader.dateStartOfEvent = tmpDateTime.date();
    this->eventHeader.timeStartOfEvent = tmpDateTime.time();
    this->eventHeader.stringDateTimeStartOfEvent = tmpDateTime.toString(formatDateTimeWithoutSeconds);
}

void ProcessPage::setDateTimeEndOfEvent() {
    this->eventHeader.dateEndOfEvent = this->getDateEndOfEvent();
    this->eventHeader.timeEndOfEvent = this->getTimeEndOfEvent();
    this->eventHeader.stringDateTimeEndOfEvent = this->eventHeader.dateEndOfEvent.toString(formatOfDate) + " " + this->eventHeader.timeEndOfEvent.toString(formatOfTime);
}

QDateTime ProcessPage::getDateTimeStartOfEvent() {
    return  QDateTime(this->eventHeader.dateStartOfEvent, this->eventHeader.timeStartOfEvent, Qt::LocalTime);
}

void ProcessPage::setDateTimeOfAutomation(QDateTime tmpDateTime) {
    this->eventHeader.dateOfAutomation = tmpDateTime.date();
    this->eventHeader.timeOfAutomation = tmpDateTime.time();
    this->eventHeader.stringDateTimeOfAutomation = tmpDateTime.toString(formatDateTimeWithoutSeconds);
}

QDateTime ProcessPage::getDateTimeOfAutomation() {
    return QDateTime(this->eventHeader.dateOfAutomation, this->eventHeader.timeOfAutomation, Qt::LocalTime);
}

QString ProcessPage::getStringDateTimeStartOfEvent() {
    return this->eventHeader.stringDateTimeStartOfEvent;
}

QString ProcessPage::getStringDateTimeOfAutomation() {
    return this->eventHeader.stringDateTimeOfAutomation;
}

QDate ProcessPage::getDateEndOfEvent() {
    QDateTime dateTimeEndOfEvent = calendarUtils.processDateTimeWithShift(this->eventHeader.dateStartOfEvent, 0,
                                                                          this->eventHeader.timeStartOfEvent, lengthEventInHours,
                                                                          false);
    return dateTimeEndOfEvent.date();
}

QTime ProcessPage::getTimeEndOfEvent() {
    QDateTime dateTimeEndOfEvent = calendarUtils.processDateTimeWithShift(this->eventHeader.dateStartOfEvent, 0,
                                                                          this->eventHeader.timeStartOfEvent, lengthEventInHours,
                                                                          false);
    return dateTimeEndOfEvent.time();
}

QTime ProcessPage::getTimeEndOfEvent(QTime timeStartOfEvent) {
    QDateTime dateTimeEndOfEvent = calendarUtils.processDateTimeWithShift(this->eventHeader.dateStartOfEvent, 0,
                                                                          timeStartOfEvent, lengthEventInHours,
                                                                          false);
    return dateTimeEndOfEvent.time();
}

QDateTime ProcessPage::getDateTimeEndOfEvent() {
    QTime timeEndOfEvent = this->eventHeader.timeStartOfEvent;
    int hours = timeEndOfEvent.hour() + lengthEventInHours;
    int minutes = timeEndOfEvent.minute();
    int seconds = timeEndOfEvent.second();
    timeEndOfEvent.setHMS(hours, minutes, seconds, 0);
    return QDateTime(this->eventHeader.dateStartOfEvent, timeEndOfEvent, Qt::LocalTime);
}

QDateTime ProcessPage::getShiftToStartEvent(int lengthShiftInHours) {
    QTime timeShiftOfEvent = this->eventHeader.timeStartOfEvent;
    int hours = timeShiftOfEvent.hour() + lengthShiftInHours;
    int minutes = timeShiftOfEvent.minute();
    int seconds = timeShiftOfEvent.second();
    timeShiftOfEvent.setHMS(hours, minutes, seconds, 0);
    return QDateTime(this->eventHeader.dateStartOfEvent, timeShiftOfEvent, Qt::LocalTime);
}

QString ProcessPage::getStringCity() {
    return this->eventHeader.stCity;
}

void ProcessPage::initializeListFillRows() {
    for (int index = 0; index < this->rowCount; index++) {
        UserData userData = this->listUserData.at(index);
        if((userData.email != "") && (userData.name != "") &&
                (userData.password != "") && (userData.id > 0)) {
            this->listFillRows.push_back(index);
        }
    }
    quantityFillRows = listFillRows.size();
}

int ProcessPage::getQuantityFillRows() {
    if (!listFillRows.isEmpty()) {
        listFillRows.clear();
    }
    initializeListFillRows();
    return quantityFillRows;
}

QList<int> ProcessPage::getListFillRows() {
    if (listFillRows.isEmpty()) {
        initializeListFillRows();
    }
    return listFillRows;
}

int ProcessPage::getQuantityEnableUsers() {
    if (listEnableUsers.isEmpty()) {
        initializeListEnableUsers();
    }
    return quantityEnableUsers;
}

void ProcessPage::initializeListEnableUsers() {
    if (listFillRows.isEmpty()) {
        initializeListFillRows();
    }
    for (int indexFillRow = 0; indexFillRow < quantityFillRows; indexFillRow++) {
        UserData userData = this->listUserData.at(indexFillRow);
        if (userData.enabled) {
            this->listEnableUsers.push_back(indexFillRow);
        }
    }
    quantityEnableUsers = this->listEnableUsers.size();
}

QList<int> ProcessPage::getListEnableUsers() {
    if (listEnableUsers.isEmpty()) {
        initializeListEnableUsers();
    }
    return this->listEnableUsers;
}

int ProcessPage::getQuantityNoEmptyRows() {
    int quantityNoEmptyRows = 0;
    for (int index = 0; index < this->rowCount; index++) {
        UserData userData = this->listUserData.at(index);
        if((userData.email != "") || (userData.name != "") ||
                (userData.password != "") || (userData.id > 0)) {
            quantityNoEmptyRows++;
        }
    }
    return quantityNoEmptyRows;
}

bool ProcessPage::isEmptyUserData(UserData userData) {
    if ((userData.name != "") && (userData.email != "") &&
            (userData.password != ""))
    return false;
    else {
        return true;
    }
}

void ProcessPage::processIndexRowCaptainEvent() {
    if (this->eventHeader.randomizeCaptain) {
        int enableUsers = 0;
        getListEnableUsers();
        if (!listEnableUsers.isEmpty()) {
            enableUsers = listEnableUsers.size() - 1;
        }
        if (enableUsers > 0) {
            this->eventHeader.indexRowCaptainEvent = generationUtils.getRandomNumber(0, enableUsers);
        } else {
            this->eventHeader.indexRowCaptainEvent = 0;
        }
    }
}

void ProcessPage::getNewGeneratedString() {
    this->generatedString = generationUtils.getRandomString();
}

void ProcessPage::refreshItemUserData(int indexInsert, UserData userData) {
    if (this->listUserData.isEmpty()) {
        this->listUserData.push_back(userData);
    }
    else {
        this->listUserData.removeAt(indexInsert);
        this->listUserData.insert(indexInsert, userData);
    }
}

void ProcessPage::setCellValueInUserData(int row, int column, QVariant cellValue) {
    UserData userData = this->listUserData.at(row);
    switch(column) {
        case 0:
            userData.note = cellValue.toString();
        break;
        case 1:
            userData.name = cellValue.toString();
        break;
        case 2:
            userData.email = cellValue.toString();
        break;
        case 3:
            userData.password = cellValue.toString();
        break;
        case 4:
            userData.proxy = cellValue.toString();
        break;
        case 5:
            userData.enabled = cellValue.toBool();
        break;
    }
    this->listUserData.removeAt(row);
    this->listUserData.insert(row, userData);
}
