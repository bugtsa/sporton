#ifndef LOGMESSAGE_H
#define LOGMESSAGE_H

#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QTextStream>
#include <QString>

#include "utils/constants.h"

//class for output Message Windows
class LogMessage
{
public:
    LogMessage();

    void writeLine(QString stringLine);

private:
    QFile currentLogFile;

    QString fileName;
};

#endif // LOGMESSAGE_H
