#ifndef EVENTSPECIFICATION_H
#define EVENTSPECIFICATION_H

#include <QString>

//class for spacification Events
class EventSpecification
{
public:
    EventSpecification();

    QString title;

    QString proxyConnection;

    QString startDateTimeOfEvent;

    QString endTimeOfEvent;

    QString dateTimeAutomation;

    QString repeatEvery;

    QString dateTimeNextEvent;

    QString dateTimeNextAutomation;
};

#endif // EVENTSPECIFICATION_H
