#ifndef MainExecutor_H
#define MainExecutor_H

#include <QObject>

#include "utils/processgui.h"
#include "data/xmlworker.h"
#include "data/sportondatamanager.h"

class MainExecutor : public QObject
{
    Q_OBJECT
public:
    explicit MainExecutor(LogMessage *_logMessage, QObject *parent = 0);

    ~MainExecutor();

    QString openFile(QString fileName, ProcessPage *processPage);

    bool saveFile(QString fileName, ProcessPage *processPage, int rowCount);

    void resetProcessPage(ProcessPage *processPage, int rowCount);

    void swapProcessPage(ProcessPage *processPage, ProcessPage _processPage);

    QDate getDate(ProcessPage *processPage);

    QString getDateTimeString(ProcessPage *processPage, bool isDateTimeStartOfEvent);

    bool getConnectionStatus();

private:
    LogMessage *logMessage;

    XmlWorker *xmlWorker;

    SportonDataManager *sportonDataManager;

    QFile xmlBuffer;
private:


};

#endif // MainExecutor_H
