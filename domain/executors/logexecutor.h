#ifndef LogExecutor_H
#define LogExecutor_H

#include <QTableWidget>
#include <QMainWindow>
#include <QApplication>
#include <QFile>

#include "data/processpage.h"
#include "data/eventspecification.h"
#include "data/sportondatamanager.h"
#include "utils/processgui.h"
#include "utils/tableutils.h"
#include "utils/scrollmessagebox.h"
#include "utils/calendarutils.h"

class LogExecutor : public QMainWindow
{
    Q_OBJECT
public:
    LogExecutor(LogMessage *_logMessage, QWidget *parent = 0);

    ~LogExecutor();

public:    
    int getRequestCheckUserIdByEmail(QString requestEmail);

    QString getRequestCheckEmailByUserId(int requestUserId);

    QString getRequestRegisterUserData(UserData userData);

    bool getRequestUpdateUser(UserData userData);

    void setPointToTableWidget(QTableWidget *pTableWidget);

    void initActivityLogTableWidget();

    void addTask(ProcessPage processPage, bool isEditCurrentEventForServer);

    void editTask(ProcessPage processPage, int indexEditEvent);

    void clearLogWindow();

    void showTasks();

    void requestStartTasks();

    void stopUploadTasksToServer();

    void breakWaitStartTask();

    QList<ProcessPage> getListTasks(bool isEditCurrentEventForServer);

    ProcessPage getItemInListTasks(QString indexInListTasks);

    void cleanUploadedTasks();

    void cleanListTasks();

    bool isEmptyClickedRow(QTableWidget *pTableWidget, int indexRow);    

    const EventSpecification getEventSpecification(const ProcessPage processPage);

    const ProcessPage getProcessPage(int indexListTasks);

signals:
    void saveCurrentProcessPageToXmlFile(ProcessPage processPage);

    void requestSetProcessPageOnUI(ProcessPage processPage);

public slots:
    void getAddItemIntoMapSelectedTask(QTableWidget *pTableWidget, int rowIndex, int columnIndex);

    void addEventAtStartUploadingOnServer(ProcessPage processPage, bool isEnableChangeSelectedEvent);

    void getRequestChangeEventOnUI();

private:

    bool isStart;

    int quantityNoEmptyRows;

    int quantityNoEmptyStatusCell;

    QString requestType;

    QString userID;

    bool okConverter;

    ProcessGUI processGUI;

    TableUtils tableUtils;

    StringsUtils stringsUtils;

    GenerationUtils generationUtils;

    CalendarUtils calendarUtils;

    QTableWidget *pTableWidget;

    QString nameDatabase;    

    SportonDataManager *sportonDataManager;

    LogMessage *logMessage;

    QList<QString> listEventsID;

    QMap<int, QString> mapEventsStartOfAutomation;

    QList<int> listIndexUploadedTasks;

    QList<ProcessPage> listTasks;

    QMap<QString, QString> mapSelectedRow;

    QMap<QString, QString> mapTasksForEdit;

    int quantityRepeatTheSameEvent;

private:

    bool isStatusReady(QString stStatus);

    QString getStringCreateGame(QString title);

    QString getStringUserToJoinGame(int userID, QString title);

    int getIndexNoEmptyStatusCells();

    void setStatus();

    void setStatus(bool status, int indRow);

    QString getStatus(int intRow);

    void refreshEventStatusAndAdded(int indexCurrentTask);

    void setAdded(QString dateTime, int indRow);

    void setStatusAndAdded(QString dateTime, int indRow);

    void setStatusAndAdded(bool responseCode, int indRow);

    bool isSelectedRowAsCreateEvent();

    void clearOneRow(int indexOutputRow, QTableWidget *pTableWidget);

    void showOneRow(int indexOutputRow, ProcessPage processPage);

    void waitStartTask();

    void startUploadTasksToServer(int indexCurrentTask);

    void calculationRepeatEventWeekByWeek(ProcessPage processPage);

    void prccessEventWeekByWeek(int indexEvent);

    bool addEvent(ProcessPage *processPage);

    void addUsers(ProcessPage *processPage, int indexEvent);

    void processNextEventByRepeatAtWeekByWeek(ProcessPage *processPage);

    void setDateTimeOfNextEventAndAutomation(ProcessPage *processPage);

    void swapDateTimeFromNextEventInCurrent(ProcessPage *processPage);
};


#endif // LogExecutor_H
