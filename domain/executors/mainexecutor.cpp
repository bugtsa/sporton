#include "MainExecutor.h"

MainExecutor::MainExecutor(LogMessage *_logMessage, QObject *parent) :
    QObject(parent) {
    xmlWorker = new XmlWorker();
    sportonDataManager = new SportonDataManager(this);
    logMessage = _logMessage;
    logMessage->writeLine("create MainExecutor::MainExecutor()");
}

MainExecutor::~MainExecutor() {
    delete xmlWorker;
}

QString MainExecutor::openFile(QString fileName, ProcessPage *processPage) {
    xmlBuffer.setFileName(fileName);
    if (!xmlBuffer.exists()) {
        return "";
    }
    else {
        if (!xmlWorker->ReadXmlWithDomTree(&xmlBuffer, processPage)) {
            return "";
        }
    }
    return fileName;
}

bool MainExecutor::saveFile(QString fileName, ProcessPage *processPage, int rowCount) {
    xmlBuffer.setFileName(fileName);
    xmlWorker->CreateEmptyXmlFile(&xmlBuffer, rowCount);
    return xmlWorker->SetDataToDomTree(&xmlBuffer, processPage);
}

void MainExecutor::resetProcessPage(ProcessPage *processPage, int rowCount) {
    processPage->reset(rowCount);
}

void MainExecutor::swapProcessPage(ProcessPage *processPage, ProcessPage _processPage) {
    processPage->swapProcessPage(_processPage);
}

QString MainExecutor::getDateTimeString(ProcessPage *processPage, bool isDateTimeStartOfEvent) {
    if (isDateTimeStartOfEvent) {
        return processPage->getStringDateTimeStartOfEvent();
    } else {
        return processPage->getStringDateTimeOfAutomation();
    }
}

bool MainExecutor::getConnectionStatus() {
    return sportonDataManager->getConnectionStatus();
}
