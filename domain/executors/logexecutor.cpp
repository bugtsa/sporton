#include "logexecutor.h"

LogExecutor::LogExecutor(LogMessage *_logMessage, QWidget *parent)
{
    nameDatabase = "LogExecutor";
    quantityNoEmptyRows = quantityNoEmptyStatusCell = 0;
    quantityRepeatTheSameEvent = 0;
    isStart = false;

    sportonDataManager = new SportonDataManager(this);
    logMessage = _logMessage;
    logMessage->writeLine("create LogExecutor::LogExecutor()");
}

LogExecutor::~LogExecutor() {
}

void LogExecutor::setPointToTableWidget(QTableWidget *_pTableWidget) {
    pTableWidget = _pTableWidget;
}

bool LogExecutor::isStatusReady(QString stStatus) {
    if (stStatus == STRING_OK) {
        return false;
    } else {
        return true;
    }
}

QString LogExecutor::getStringCreateGame(QString title) {
    return logActivityCreateGame + title;
}

QString LogExecutor::getStringUserToJoinGame(int userID, QString title) {
    return logActivityUser + stringsUtils.convertIntToStr(userID) + logActivityJoinToGame + title;
}

void LogExecutor::initActivityLogTableWidget() {
    tableUtils.initActivityLogTableWidget(pTableWidget);
}

void LogExecutor::cleanUploadedTasks() {
    logMessage->writeLine("start LogExecutor::cleanUploadedTasks()");
    int indOutputRow = 0;

    if (!listTasks.isEmpty()) {
        for (int indGame = 0; indGame < listTasks.size(); indGame++) {
            ProcessPage processPage = listTasks.at(indGame);
            if (!isStatusReady(processPage.eventHeader.status)) {
                tableUtils.clearRow(pTableWidget, indOutputRow);
            }
            indOutputRow++;
            for (int indUser = 0; indUser < processPage.listUserData.size(); indUser++) {
                UserData userData = processPage.listUserData.at(indUser);
                if (!processPage.isEmptyUserData(userData)) {
                    if (!isStatusReady(userData.status)) {
                        tableUtils.clearRow(pTableWidget, indOutputRow);
                    }
                    indOutputRow++;
                }
            }
        }
        tableUtils.setOnTopLeftCell(pTableWidget);
        for(int indList = 0; indList < listTasks.size(); indList++) {
            ProcessPage processPage = listTasks.at(indList);
            if (processPage.isStatusUploadToServer()) {
                listTasks.removeAt(indList);
            }
        }
    }
}

void LogExecutor::cleanListTasks() {
    listTasks.clear();
}

void LogExecutor::setStatusAndAdded(QString dateTime, int indRow) {
    bool status = false;
    if (dateTime.contains("Error")) {
        status = false;
    } else {
        status = true;
    }
    setStatus(status, indRow);
    setAdded(dateTime, indRow);
}

void LogExecutor::setStatusAndAdded(bool responseCode, int indRow) {
    setStatus(responseCode, indRow);
    setAdded(stringAdded, indRow);
}

void LogExecutor::setStatus(bool status, int indRow) {
    QString stStatus = "";
    if(status) {
        stStatus = STRING_OK;
    }
    else {
        stStatus = STRING_ERROR;
    }
    tableUtils.setTextInTableWidgetCell(pTableWidget, indRow, indexStatus, stStatus);
}

QString LogExecutor::getStatus(int indRow) {
    return tableUtils.getTextFromTableWidgetCell(pTableWidget, indRow, indexStatus);
}

void LogExecutor::refreshEventStatusAndAdded(int indexCurrentTask) {
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexCurrentTask,
                                        indexStatus, listTasks.at(indexCurrentTask).status);
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexCurrentTask,
                                        indexAdded, stringAdded);
}

void LogExecutor::setAdded(QString stDateTime, int indRow) {
   tableUtils.setTextInTableWidgetCell(pTableWidget, indRow, indexAdded, stDateTime);
}

int LogExecutor::getIndexNoEmptyStatusCells() {
    if (quantityNoEmptyRows == 0) {
        return 0;
    } else {
        return quantityNoEmptyStatusCell;
    }
}

void LogExecutor::setStatus() {
    int indexCurrentRow = getIndexNoEmptyStatusCells();
    logMessage->writeLine("start LogExecutor::setStatus() with indexCurrentRow = quantityNoEmptyStatusCell: "
                          + QString("%1").arg(quantityNoEmptyRows));
    for (int indexEvent = indexCurrentRow; indexEvent < listTasks.size(); indexEvent++) {
        ProcessPage processPage = listTasks.at(indexEvent);
        QString stringStatus = "";
        if (processPage.status == "" || processPage.status == STRING_STOP) {
            stringStatus = STRING_PENDING;
        } else if (processPage.status == STRING_PENDING && !isStart) {
            stringStatus = STRING_STOP;
        }
        processPage.status = stringStatus;
        editTask(processPage, indexEvent);
        tableUtils.setTextInTableWidgetCell(pTableWidget, indexCurrentRow++, indexStatus, stringStatus);
    }
}

void LogExecutor::showTasks() {
    logMessage->writeLine("start LogExecutor::showTasks()");
    if (!listTasks.isEmpty()) {

        int indexCurrentRow;
        if (quantityNoEmptyRows == 0) {
            indexCurrentRow = 0;
        } else {
            indexCurrentRow = quantityNoEmptyRows;
        }
        for (int indGame = indexCurrentRow; indGame < listTasks.size(); indGame++) {
            ProcessPage processPage = listTasks.at(indGame);
            showOneRow(indexCurrentRow, processPage);
            indexCurrentRow++;
        }
        quantityNoEmptyRows = indexCurrentRow;
        tableUtils.setOnTopLeftCell(pTableWidget);
    }
}

void LogExecutor::showOneRow(int indexOutputRow, ProcessPage processPage) {
    QString title = "";
    QString startOfAutomation = "";
    QString startOfEvent = "";
    title = processPage.eventHeader.title;
    startOfAutomation = processPage.getDateTimeOfAutomation().toString(formatDateTimeWithoutSeconds);
    startOfEvent = processPage.getDateTimeStartOfEvent().toString(formatDateTimeWithoutSeconds);
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexOutputRow, indexTitleEvent, getStringCreateGame(processPage.eventHeader.title));
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexOutputRow, indexStartOfAutomationLogActivity,
                                        processPage.getDateTimeOfAutomation().toString(formatDateTimeWithoutSeconds));
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexOutputRow, indexStartOfEventLogActivity,
                                        processPage.getDateTimeStartOfEvent().toString(formatDateTimeWithoutSeconds));
    tableUtils.setTextInTableWidgetCell(pTableWidget, indexOutputRow, indexStatus, processPage.status);
}

void LogExecutor::clearOneRow(int indexOutputRow, QTableWidget *pTableWidget) {
    QString cellText = "";
    for (int index = 0; index < tableUtils.getLastIndexColumnTableWidget(pTableWidget); index++) {
        tableUtils.setTextInTableWidgetCell(pTableWidget, indexOutputRow, 0, cellText);
    }
}

bool LogExecutor::isEmptyClickedRow(QTableWidget *pTableWidget, int indexRow) {
    return tableUtils.isEmptyClickedRow(pTableWidget, indexRow);
}

void LogExecutor::clearLogWindow() {
    logMessage->writeLine("start LogExecutor::clearLogWindow()");
    if (!listTasks.isEmpty()) {

        int indOutputRow = 0;

        for (int indGame = 0; indGame < listTasks.size(); indGame++) {
            ProcessPage processPage = listTasks.at(indGame);
            clearOneRow(indOutputRow, pTableWidget);
            indOutputRow++;
            for (int indUser = 0; indUser < processPage.listUserData.size(); indUser++) {
                clearOneRow(indOutputRow, pTableWidget);
                indOutputRow++;
            }
        }
        tableUtils.setOnTopLeftCell(pTableWidget);
    }
}

void LogExecutor::stopUploadTasksToServer() {
    logMessage->writeLine("start LogExecutor::stopUploadTasksToServer()");
    isStart = false;
    setStatus();
    showTasks();
}

void LogExecutor::requestStartTasks() {
    logMessage->writeLine("start LogExecutor::requestStartTasks()");
    if (!listTasks.isEmpty()) {
        for (int indexTask = 0; indexTask < listTasks.size(); indexTask++) {
            ProcessPage processPage = listTasks.at(indexTask);
            if (isStatusReady(processPage.eventHeader.status)) {
                mapEventsStartOfAutomation.insert(indexTask, processPage.eventHeader.stringDateTimeOfAutomation);
            }
        }
        isStart = true;
        waitStartTask();
    }
}

void LogExecutor::setDateTimeOfNextEventAndAutomation(ProcessPage *processPage) {
    bool isRandomizeDays = processPage->eventHeader.daysRepeat;
    bool isRandomizeHours = processPage->eventHeader.hoursRepeat;
    int valueRandomizeHours = 0;
    int valueRandomizeDays = 0;

    if (isRandomizeDays) {
        int valueDays = processPage->eventHeader.valueDaysRepeat + 1;
        valueRandomizeDays = generationUtils.getRandomNumberBetweenPosNegIndex(valueDays);
    }
    if (isRandomizeHours) {
        int valueHours = processPage->eventHeader.valueHoursRepeat + 1;
        valueRandomizeHours = generationUtils.getRandomNumberBetweenPosNegIndex(valueHours);
    }
    processPage->eventHeader.dateTimeOfNextEvent = calendarUtils.processDateTimeWithShift(processPage->eventHeader.dateStartOfEvent, valueRandomizeDays,
                                                                         processPage->eventHeader.timeStartOfEvent, valueRandomizeHours, true);
    processPage->eventHeader.dateTimeOfNextAutomation = calendarUtils.processDateTimeWithShift(processPage->eventHeader.dateEndOfEvent, 0,
                                                                                          processPage->eventHeader.timeEndOfEvent, lengthIntervalAtUploadNewEvent, false);
    calculationRepeatEventWeekByWeek(*processPage);
}

void LogExecutor::processNextEventByRepeatAtWeekByWeek(ProcessPage *processPage) {
    swapDateTimeFromNextEventInCurrent(processPage);
    setDateTimeOfNextEventAndAutomation(processPage);
    processPage->status = STRING_PENDING;
    processPage->eventHeader.status = "";
    mapEventsStartOfAutomation.insert(listTasks.size(), processPage->eventHeader.stringDateTimeOfAutomation);
    processPage->eventHeader.indexEventInLogWindow = listTasks.size();
    listTasks.push_back(*processPage);
}

void LogExecutor::swapDateTimeFromNextEventInCurrent(ProcessPage *processPage) {
    processPage->setDateTimeStartOfEvent(processPage->eventHeader.dateTimeOfNextEvent);
    processPage->setDateTimeEndOfEvent();
    processPage->setDateTimeOfAutomation(processPage->eventHeader.dateTimeOfNextAutomation);
}

int LogExecutor::getRequestCheckUserIdByEmail(QString requestEmail) {
    return sportonDataManager->getUserIdByEmail(requestEmail);
}

QString LogExecutor::getRequestCheckEmailByUserId(int requestUserId) {
    return sportonDataManager->getEmailByUserId(requestUserId);
}

QString LogExecutor::getRequestRegisterUserData(UserData email) {
    return sportonDataManager->registerUserData(email);
}

bool LogExecutor::getRequestUpdateUser(UserData userRow) {
    return sportonDataManager->updateUser(userRow);
}

void LogExecutor::waitStartTask() {
    QString currentDateTime;
    bool isAlreadyUploadCurrentTask;

    setStatus();
    while (isStart) {
        currentDateTime = QDateTime::currentDateTime().toString(formatDateTimeWithoutSeconds);
        QCoreApplication::processEvents( QEventLoop::AllEvents, 1);
        for (int indexCurrentTask = 0; indexCurrentTask < mapEventsStartOfAutomation.size(); indexCurrentTask++) {
            if (currentDateTime == mapEventsStartOfAutomation.value(indexCurrentTask)) {
                isAlreadyUploadCurrentTask = false;
                for(int indexUploadedTask = 0; indexUploadedTask < listIndexUploadedTasks.size(); indexUploadedTask++) {
                    if (indexCurrentTask == listIndexUploadedTasks.at(indexUploadedTask)) {
                        isAlreadyUploadCurrentTask = true;
                    }
                }
                if (!isAlreadyUploadCurrentTask) {
                    listIndexUploadedTasks.push_back(indexCurrentTask);
                    startUploadTasksToServer(indexCurrentTask);
                    break;
                }
            }
        }
    }
}

void LogExecutor::breakWaitStartTask() {
    isStart = false;
}

void LogExecutor::startUploadTasksToServer(int indexCurrentTask) {
    logMessage->writeLine("start LogExecutor::startUploadTasksToServer()");
    if (!listTasks.isEmpty()) {
        requestType = "1";
        userID = "0";
        logMessage->writeLine(QString("size listTasks: %1 and indexCurrentTask: %2").arg(listTasks.size()).arg(indexCurrentTask));
        ProcessPage processPage = listTasks.at(indexCurrentTask);
        if (!addEvent(&processPage)) {
            return;
        }
        addUsers(&processPage, indexCurrentTask);
        if (processPage.isStatusUploadToServer()) {
            processPage.status = STRING_OK;
        } else {
            processPage.status = STRING_PENDING;
        }
        editTask(processPage, indexCurrentTask);
        refreshEventStatusAndAdded(indexCurrentTask);
        if (quantityRepeatTheSameEvent < QUANTITY_REPEAT_THE_SAME_EVENT) {
            if (processPage.eventHeader.repeatWeekByWeek) {
                processNextEventByRepeatAtWeekByWeek(&processPage);
                showTasks();
                quantityRepeatTheSameEvent++;
            }
        }
    }
    tableUtils.setOnTopLeftCell(pTableWidget);
}

bool LogExecutor::addEvent(ProcessPage *processPage) {
    if (isStatusReady(processPage->eventHeader.status)) {
        bool isCreateEvent;
        QString stringStatus = "";

        if (!processPage->listUserData.isEmpty()) {
            QList<int> listIndexEnabledUsers = processPage->getListEnableUsers();
            int indexUserCaptainOfEvent = 0;
            if (processPage->eventHeader.randomizeCaptain) {
                indexUserCaptainOfEvent = listIndexEnabledUsers.at(generationUtils.getRandomNumber(0, processPage->getQuantityEnableUsers() - 1));
            } else {
                indexUserCaptainOfEvent = listIndexEnabledUsers.first();
            }
            UserData UserData = processPage->listUserData.at(indexUserCaptainOfEvent);
            logMessage->writeLine(QString("start sportonDataManager->registerUserData() with id: %1, email: %2").arg(UserData.id).arg(UserData.email));
            userID = sportonDataManager->registerUserData(UserData);
            if (userID == "-1") {
                return false;
            } else if ((userID != "-1") && (!userID.contains("0"))) {
                UserData.id = userID.toInt(&okConverter, 0);
                processPage->refreshItemUserData(indexUserCaptainOfEvent, UserData);
            } else if (userID.contains("0")) {
                userID = QString("%1").arg(UserData.id);
            }
        }
        logMessage->writeLine(QString("reply user_id: %1").arg(userID));
//        if (listEventsID.size() <= indexEvent) {
            logMessage->writeLine("start sportonDataManager->createEvent()");
            listEventsID.push_back(sportonDataManager->createEvent(processPage, requestType, userID));
//        } else {
//            logMessage->writeLine("start sportonDataManager->updateEvent()");
//            sportonDataManager->updateEvent(processPage, listEventsID.at(indexEvent), userID);
//        }
        if (listEventsID.isEmpty()) {
            isCreateEvent = false;
        } else if (!listEventsID.isEmpty()) {
            if (listEventsID.last() == "") {
                isCreateEvent = false;
            } else if (listEventsID.last() != ""){
                isCreateEvent = true;
            }
        }
        if (isCreateEvent) {
            stringStatus = STRING_OK;
        } else {
            stringStatus = STRING_ERROR;
        }
        logMessage->writeLine("get Event ID: " + listEventsID.last()
                              + QString(" statusCreateEvent is %1").arg(isCreateEvent));
//        setStatusAndAdded(statusCreateEvent, quantityNoEmptyStatusCell);
        processPage->eventHeader.status = stringStatus;
        quantityNoEmptyStatusCell++;
    }
    return true;
}

void LogExecutor::addUsers(ProcessPage *processPage, int indexEvent) {
    for (int indexUser = 0; indexUser < processPage->listUserData.size(); indexUser++) {
        QCoreApplication::processEvents();
        if (!isStart) {
            break;
        }
        UserData UserData = processPage->listUserData.at(indexUser);
        QString addedUserID = "0";
        if (isStatusReady(UserData.status)) {
            if (!processPage->isEmptyUserData(UserData) && UserData.enabled) {
                if (UserData.id <= 0) {
                    logMessage->writeLine(QString("start sportonDataManager->registerUserData() with id: %1, email: %2").arg(UserData.id).arg(UserData.email));
                    addedUserID = sportonDataManager->registerUserData(UserData);
                }
                if (addedUserID == "-1") {
                    logMessage->writeLine(QString("addedUserID is: %1 with continue").arg(addedUserID));
                    continue;
                } else if (addedUserID.contains("0")) {
                    addedUserID = QString("%1").arg(UserData.id);
                    UserData.status = STRING_OK;
                    logMessage->writeLine(QString("addedUserID is: %1 with contains 0").arg(addedUserID));
                }
                else if ((addedUserID != "-1") && (!addedUserID.contains("0"))) {
                    logMessage->writeLine(QString("start processPage.refreshItemUserData with indexUser: %1, userEmail %1").arg(indexUser).arg(UserData.email));
                    UserData.id = addedUserID.toInt(&okConverter, 0);
                    processPage->refreshItemUserData(indexUser, UserData);
                }
                sportonDataManager->addEventParticipation(addedUserID,
                                                            listEventsID.at(indexEvent),
                                                            requestType,
                                                            processPage->eventHeader.stringDateTimeOfAutomation);
                logMessage->writeLine("start sportonDataManager->addEventParticipation()");
                //обновляем UserData, чтобы сохранилось значение status
                processPage->listUserData.removeAt(indexUser);
                processPage->listUserData.insert(indexUser, UserData);
            }
        }
    }
}

const ProcessPage LogExecutor::getProcessPage(int indexListTasks) {
    return listTasks.at(indexListTasks);
}

const EventSpecification LogExecutor::getEventSpecification(const ProcessPage processPage) {
    EventSpecification eventSpecification;
    const EventHeader *pEventHeader = &processPage.eventHeader;
    QString dateTimeNextEvent = "Non";
    QString dateTimeNextAutomation = "Non";

    eventSpecification.title = pEventHeader->title;
    eventSpecification.proxyConnection = processPage.listUserData.at(pEventHeader->indexRowCaptainEvent).proxy;
    eventSpecification.startDateTimeOfEvent = pEventHeader->stringDateTimeStartOfEvent;
    eventSpecification.endTimeOfEvent = pEventHeader->stringDateTimeEndOfEvent;
    eventSpecification.dateTimeAutomation = pEventHeader->stringDateTimeOfAutomation;
    eventSpecification.repeatEvery = QString("%1").arg(valueQuantityDaysInWeek);
    if (!pEventHeader->dateTimeOfNextEvent.isNull()) {
        dateTimeNextEvent = pEventHeader->dateTimeOfNextEvent.toString(formatDateTimeWithoutSeconds);
    }
    if (!pEventHeader->dateTimeOfNextAutomation.isNull()) {
        dateTimeNextAutomation = pEventHeader->dateTimeOfNextAutomation.toString(formatDateTimeWithoutSeconds);
    }
    eventSpecification.dateTimeNextEvent = dateTimeNextEvent;
    eventSpecification.dateTimeNextAutomation = dateTimeNextAutomation;
    return eventSpecification;
}

void LogExecutor::addTask(ProcessPage processPage, bool isEditCurrentEventForServer) {
    logMessage->writeLine("start LogExecutor::addTask()");
    if (isEditCurrentEventForServer) {
        if (!listTasks.isEmpty()) {
            if (listTasks.last().generatedString == processPage.generatedString) {
                listTasks.pop_back();
            }
        }
    }
    bool isWeekByWeek = processPage.eventHeader.repeatWeekByWeek;
    if (isWeekByWeek) {
        setDateTimeOfNextEventAndAutomation(&processPage);
    }
    processPage.eventHeader.indexEventInLogWindow = listTasks.size();
    listTasks.push_back(processPage);
}

void LogExecutor::editTask(ProcessPage processPage, int indexEditEvent) {
    logMessage->writeLine("start LogExecutor::editTask()");
    if (listTasks.size() > indexEditEvent) {
        if (processPage.eventHeader.dateTimeOfNextAutomation.isNull() &&
                processPage.eventHeader.dateTimeOfNextEvent.isNull()) {
            setDateTimeOfNextEventAndAutomation(&processPage);
        }
        listTasks.replace(indexEditEvent, processPage);
        mapEventsStartOfAutomation.insert(indexEditEvent, processPage.eventHeader.stringDateTimeOfAutomation);
        clearOneRow(indexEditEvent, pTableWidget);
        showOneRow(indexEditEvent, processPage);
    } else {
        processPage.eventHeader.indexEventInLogWindow = listTasks.size();
        listTasks.push_back(processPage);
    }
}

void LogExecutor::addEventAtStartUploadingOnServer(ProcessPage processPage, bool isEnableChangeSelectedEvent) {
//    if (isStart) {
//        mapEventsStartOfAutomation.push_back(processPage.eventHeader.stringDateTimeOfAutomation);
//    }
    if (!processGUI.existsCurrentProcessPageInListTasks(&processPage, listTasks) || isEnableChangeSelectedEvent)
    {
        if (!mapSelectedRow.isEmpty()) {
            //mapSelectedRow.firstKey() = first Value in String
            int indexSelectedEventForChange = mapSelectedRow.firstKey().toInt(&okConverter, 0);
            if (!mapTasksForEdit.isEmpty()) {
                listEventsID.push_back(mapTasksForEdit.first());
                editTask(processPage, indexSelectedEventForChange);
            } else {
                addTask(processPage, isEnableChangeSelectedEvent);
            }
        } else {
            addTask(processPage, isEnableChangeSelectedEvent);
        }
    }
}

void LogExecutor::getRequestChangeEventOnUI() {
    QString indexInListTasks = mapSelectedRow.firstKey();
    ProcessPage processPage = getItemInListTasks(indexInListTasks);
    if (isStart) {
        processPage.eventHeader.indexEventInLogWindow = -1;
    }
    emit requestSetProcessPageOnUI(processPage);
}

bool LogExecutor::isSelectedRowAsCreateEvent() {
    if (!mapSelectedRow.isEmpty()) {
        return true;
    } else {
        return false;
    }
}

QList<ProcessPage> LogExecutor::getListTasks(bool isEditCurrentEventForServer) {
    return listTasks;
}

ProcessPage LogExecutor::getItemInListTasks(QString indexInListTasks) {
    if (!listTasks.isEmpty()) {
        int index = indexInListTasks.toInt();
        return listTasks.at(index);
    } else {
        ProcessPage tempProcessPage;
        return tempProcessPage;
    }
}

void LogExecutor::getAddItemIntoMapSelectedTask(QTableWidget *pTableWidget, int rowIndex, int columnIndex) {
    QString stringValue = tableUtils.getTextFromTableWidgetCell(pTableWidget, rowIndex, indexTitleLogActivity) + "|" +
            tableUtils.getTextFromTableWidgetCell(pTableWidget, rowIndex, indexStartOfAutomationLogActivity) + "|" +
            tableUtils.getTextFromTableWidgetCell(pTableWidget, rowIndex, indexStartOfEventLogActivity);
    mapSelectedRow.clear();
    mapSelectedRow.insert(QString("%1").arg(rowIndex), stringValue);
}

void LogExecutor::calculationRepeatEventWeekByWeek(ProcessPage processPage) {
    processPage.eventHeader.stringDateTimeOfAutomation = processPage.getStringDateTimeOfAutomation();
}
