#include "testconnecter.h"

TestConnecter::TestConnecter(LogMessage *logMessage, QObject *parent) : QThread(parent) {
    nameDatabase = "TestConnecter";
}

TestConnecter::~TestConnecter(){
}

void TestConnecter::run(){
    forever {
        returnValue = checkConnectionWithServer();
        if (isInterruptionRequested()) {
            break;
        }
        emit updateStateTestConnection();
        sleep(delaySendToRequestTestConnect);
    }
}

QString TestConnecter::checkConnectionWithServer() {
    int red = 0;
    int green = 0;
    int blue = 0;
//    if(mainExecutor.getConnectionStatus()) {
//        green = 255;
//        isHaveConnection = true;
//    } else {
//        red = 255;
//        isHaveConnection = false;
//    }
    return QString("background-color: rgb(%1, %2, %3);").arg(red).arg(green).arg(blue);
}

QString TestConnecter::getColorStringValue() {
    return returnValue;
}

bool TestConnecter::getConnectionStatus() {
    return isHaveConnection;
}
