#ifndef LogController _H
#define LogController _H

#include <QThread>
#include <QTableWidget>
#include <QList>

#include "domain/executors/logexecutor.h"

class LogController  : public QThread
{
    Q_OBJECT
public:
    explicit LogController (LogMessage *logMessage, QObject *parent = 0);
    ~LogController ();

    int requestCheckUserIdByEmail(QString requestEmail);

    QString requestCheckEmailByUserId(int requestUserId);

    QString requestRegisterUserData(UserData userData);

    bool requestUpdateUser(UserData userData);

    bool isEmptyClickedRow(QTableWidget *pTableWidget, int indexRow);

protected:
    void run();

public slots:

    void initTableWidget(QTableWidget *pTableWidget);

    void addTask(ProcessPage processPage, bool isEditCurrentEventForServer);

    void editTask(ProcessPage processPage, int indexEditEvent);

    void clearLogWindow();

    void showTasks();

    void startUploadTasksToServer();

    void changeSelectedTask();

    void stopUploadTasksToServer();

    void cleanUploadedTasks();

    void getCurrentProcessPageToXmlFile(ProcessPage processPage);

    void getRequestTransferToControllerProcessPage(ProcessPage processPage, bool isEditCurrentEventForServer);

    void getRequestAddItemIntoMapSelectedTask(QTableWidget *pTableWidget, int rowIndex, int columnIndex);

    void getRequestsetProcessPageOnUI(ProcessPage _processPage);

    void getRequestEventSpecification(int indexTask);

    void getRequestBreakWaitStartTask();

signals:
    void requestUploadEventOnServer(ProcessPage processPage, bool isEnableChangeSelectedEvent);

    void requestToExecutorAddItemIntoMapSelectedTask(QTableWidget *pTableWidget, int rowIndex, int columnIndex);

    void replySetProcessPageOnUI(ProcessPage processPage);

    void saveCurrentProcessPageToXmlFile(ProcessPage processPage);

    void replyGetEventSpecification(EventSpecification);

private:

    LogExecutor *logExecutor;

    bool okConvert;
};


#endif // LogController _H
