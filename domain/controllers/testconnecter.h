#ifndef TESTCONNECTER_H
#define TESTCONNECTER_H

#include <QThread>

#include "domain/executors/mainexecutor.h"

class TestConnecter : public QThread
{
    Q_OBJECT
public:
    explicit TestConnecter(LogMessage *logMessage, QObject *parent = 0);
    ~TestConnecter();

    QString getColorStringValue();

    bool getConnectionStatus();
protected:
    void run();
signals:
    void updateStateTestConnection();
private:
    MainExecutor *mainExecutor;

    QString returnValue;

    bool isHaveConnection;

    QString nameDatabase;

private:
    QString checkConnectionWithServer();
};

#endif // TESTCONNECTER_H
