#ifndef MAINCONTROLLER
#define MAINCONTROLLER

#include "domain/executors/mainexecutor.h"

class MainController
{
public:
    MainController(LogMessage *logMessage, QObject *parent = 0);

    QString openFile(QString fileName, ProcessPage *processPage);

    bool saveFile(QString fileName, ProcessPage *processPage, int rowCount);

    void resetProcessPage(ProcessPage *processPage, int rowCount);

    void swapProcessPage(ProcessPage *processPage, ProcessPage _processPage);

    QString getDateTimeString(ProcessPage *processPage, bool isDateTimeStartOfEvent);

    bool getConnectionStatus();

public:
    MainExecutor *mainExecutor;
};

#endif // MAINCONTROLLER
