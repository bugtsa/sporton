#include "logcontroller.h"

LogController ::LogController (LogMessage *logMessage, QObject *parent) : QThread(parent) {
    logMessage->writeLine("create LogController ::LogController ()");
    logExecutor = new LogExecutor(logMessage, qobject_cast<QWidget*>(parent));
    connect(this, SIGNAL(requestToExecutorAddItemIntoMapSelectedTask(QTableWidget *, int, int )),
            logExecutor, SLOT(getAddItemIntoMapSelectedTask(QTableWidget *, int, int )));
    connect(this, SIGNAL(requestUploadEventOnServer(ProcessPage, bool)),
            logExecutor, SLOT(addEventAtStartUploadingOnServer(ProcessPage, bool)));

    connect(logExecutor, SIGNAL(requestSetProcessPageOnUI(ProcessPage)),
            this, SLOT(getRequestsetProcessPageOnUI(ProcessPage)));
    connect(logExecutor, SIGNAL(saveCurrentProcessPageToXmlFile(ProcessPage)),
            this, SLOT(getCurrentProcessPageToXmlFile(ProcessPage)));
 }

LogController ::~LogController () {

}

void LogController ::run() {

}

void LogController ::initTableWidget(QTableWidget *pTableWidget) {
    logExecutor->setPointToTableWidget(pTableWidget);
    logExecutor->initActivityLogTableWidget();
}

void LogController ::addTask(ProcessPage processPage, bool isEnableChangeSelectedEvent) {
    logExecutor->addTask(processPage, isEnableChangeSelectedEvent);
}

void LogController ::editTask(ProcessPage processPage, int indexEditEvent) {
    logExecutor->editTask(processPage, indexEditEvent);
}

void LogController ::clearLogWindow() {
    logExecutor->clearLogWindow();
}

void LogController ::showTasks() {
    logExecutor->showTasks();
}

void LogController ::getRequestTransferToControllerProcessPage(ProcessPage processPage, bool isEnableChangeSelectedEvent) {
    emit requestUploadEventOnServer(processPage, isEnableChangeSelectedEvent);
}

void LogController ::getRequestsetProcessPageOnUI(ProcessPage _processPage) {
    emit replySetProcessPageOnUI(_processPage);
}

void LogController ::startUploadTasksToServer() {
    logExecutor->requestStartTasks();
}

void LogController ::changeSelectedTask() {
    logExecutor->getRequestChangeEventOnUI();
}

void LogController ::stopUploadTasksToServer() {
    logExecutor->stopUploadTasksToServer();
}

void LogController ::cleanUploadedTasks() {
    logExecutor->cleanUploadedTasks();
}

int LogController ::requestCheckUserIdByEmail(QString requestEmail) {
    return logExecutor->getRequestCheckUserIdByEmail(requestEmail);
}

QString LogController ::requestRegisterUserData(UserData userData) {
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QString getUserId = logExecutor->getRequestRegisterUserData(userData);
    int responseUserId = getUserId.toInt(&okConvert, 0);
    if (responseUserId > 0) {
        userData.id = responseUserId;
        bool isUpdateUser = logExecutor->getRequestUpdateUser(userData);
        if (!isUpdateUser) {
//            messageUtils.createWarningMessage(this, messageTitleCheckCorrectInputTableData, "Cannot Update User with this data");
        }
    }
    QApplication::restoreOverrideCursor();
    return getUserId;
}

bool LogController::requestUpdateUser(UserData userData) {
    return logExecutor->getRequestUpdateUser(userData);
}

QString LogController::requestCheckEmailByUserId(int requestUserId) {
    return logExecutor->getRequestCheckEmailByUserId(requestUserId);
}

void LogController::getRequestAddItemIntoMapSelectedTask(QTableWidget *pTableWidget, int rowIndex, int columnIndex) {
    emit requestToExecutorAddItemIntoMapSelectedTask(pTableWidget, rowIndex, columnIndex);
}

void LogController::getCurrentProcessPageToXmlFile(ProcessPage processPage) {
    emit saveCurrentProcessPageToXmlFile(processPage);
}

bool LogController::isEmptyClickedRow(QTableWidget *pTableWidget, int indexRow) {
    return logExecutor->isEmptyClickedRow(pTableWidget, indexRow);
}

void LogController::getRequestEventSpecification(int indexTask) {
    EventSpecification eventSpecification = logExecutor->getEventSpecification(logExecutor->getProcessPage(indexTask));
    emit replyGetEventSpecification(eventSpecification);
}

void LogController::getRequestBreakWaitStartTask() {
    logExecutor->breakWaitStartTask();
}
