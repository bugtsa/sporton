#include "maincontroller.h"

MainController::MainController(LogMessage *logMessage, QObject *parent)
{
    mainExecutor = new MainExecutor(logMessage, qobject_cast<QWidget*>(parent));
}

QString MainController::openFile(QString fileName, ProcessPage *processPage) {
    return mainExecutor->openFile(fileName, processPage);
}

bool MainController::saveFile(QString fileName, ProcessPage *processPage, int rowCount) {
    return mainExecutor->saveFile(fileName, processPage, rowCount);
}

void MainController::resetProcessPage(ProcessPage *processPage, int rowCount) {
    mainExecutor->resetProcessPage(processPage, rowCount);
}

void MainController::swapProcessPage(ProcessPage *processPage, ProcessPage _processPage) {
    mainExecutor->swapProcessPage(processPage, _processPage);
}

QString MainController::getDateTimeString(ProcessPage *processPage, bool isDateTimeStartOfEvent) {
    return mainExecutor->getDateTimeString(processPage, isDateTimeStartOfEvent);
}

bool MainController::getConnectionStatus() {
    return mainExecutor->getConnectionStatus();
}
