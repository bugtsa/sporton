#ifndef CBCONNECTER_H
#define CBCONNECTER_H

#include "presentation/mainwindow.h"

class connecter{
public:
    static void setPtr(MainWindow *ptr);

    static void setFoundPlace(QString foundPlace);

private:

    static MainWindow *m_MainWindowPtr;
};

namespace Ui {
class CbConnecter;
}

class CbConnecter
{
public:
    CbConnecter();


};

#endif // CBCONNECTER_H
